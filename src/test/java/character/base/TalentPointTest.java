package character.base;

import character.DDCharacter;
import character.components.Clazz;
import character.components.Race;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p>
 * Test class for character talent points.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.0.0
 */
public class TalentPointTest {
    private DDCharacter factory(int level, Race race) {
        return new DDCharacter(level, race, Clazz.FIGHTER, new int[]{ 10, 10, 10, 10, 10, 10, });
    }

    @Test
    public void testHumanTalentPointsAtLevel1() {
        assertEquals(2, factory(1, Race.HUMAN).getTalentPoints());
    }

    @Test
    public void testHumanTalentPointsAtLevel3() {
        assertEquals(3, factory(3, Race.HUMAN).getTalentPoints());
    }

    @Test
    public void testHumanTalentPointsAtLevel6() {
        assertEquals(4, factory(6, Race.HUMAN).getTalentPoints());
    }

    @Test
    public void testHumanTalentPointsAtLevel9() {
        assertEquals(5, factory(9, Race.HUMAN).getTalentPoints());
    }

    @Test
    public void testHumanTalentPointsAtLevel12() {
        assertEquals(6, factory(12, Race.HUMAN).getTalentPoints());
    }

    private Object[] getCharacter() {
        return Arrays.stream(Race.values())
                .filter(c -> !c.equals(Race.HUMAN))
                .toArray();
    }

    @Test
    public void testCharacterTalentPointsAtLevel1() {
        for (Object o : getCharacter()) {
            assertEquals(1, factory(1, (Race)o).getTalentPoints());
        }
    }

    @Test
    public void testCharacterTalentPointsAtLevel3() {
        for (Object o : getCharacter()) {
            assertEquals(2, factory(3, (Race)o).getTalentPoints());
        }
    }

    @Test
    public void testCharacterTalentPointsAtLevel6() {
        for (Object o : getCharacter()) {
            assertEquals(3, factory(6, (Race)o).getTalentPoints());
        }
    }

    @Test
    public void testCharacterTalentPointsAtLevel9() {
        for (Object o : getCharacter()) {
            assertEquals(4, factory(9, (Race)o).getTalentPoints());
        }
    }

    @Test
    public void testCharacterTalentPointsAtLevel12() {
        for (Object o : getCharacter()) {
            assertEquals(5, factory(12, (Race)o).getTalentPoints());
        }
    }
}
