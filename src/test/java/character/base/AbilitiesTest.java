/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.InvalidParameterException;

import org.junit.jupiter.api.Test;

import character.components.Ability;
import character.components.Clazz;
import character.components.Race;
import exceptions.InvalidInputException;

/**
 * <p>
 * The test class for {@link Abilities}
 *
 * @see Abilities
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 4.2.2
 */
public class AbilitiesTest {
	private final Abilities abilitiesUnderTest = new Abilities(Race.HUMAN, Clazz.BARBARIAN, 1);

	/**
	 * <p>
	 * The <code>setDefaultAbilityValues()</code> method sets all ability values to
	 * 10. This method misses the {@literal @BeforeEach} because not every method
	 * needs the abilities to be set like that.
	 *
	 * @throws InvalidInputException
	 */
	private void setDefaultAbilityValues() throws InvalidInputException {
		for (Ability a : abilitiesUnderTest.asArray()) {
			a.setValue(10);
		}
	}

	@Test
	public void testAbilitiesHasMapSize() {
		assertEquals(6, abilitiesUnderTest.asHashMap().size());
	}

	@Test
	public void testAbilitiesArrayLength() {
		assertEquals(6, abilitiesUnderTest.asArray().length);
	}

	@Test
	public void testStrengthName() {
		assertEquals("Strength", abilitiesUnderTest.getName("str"));
	}

	@Test
	public void testDexterityName() {
		assertEquals("Dexterity", abilitiesUnderTest.getName("dex"));
	}

	@Test
	public void testConstitutionName() {
		assertEquals("Constitution", abilitiesUnderTest.getName("con"));
	}

	@Test
	public void testIntelligenceName() {
		assertEquals("Intelligence", abilitiesUnderTest.getName("int"));
	}

	@Test
	public void testWisdomName() {
		assertEquals("Wisdom", abilitiesUnderTest.getName("wis"));
	}

	@Test
	public void testCharismaName() {
		assertEquals("Charisma", abilitiesUnderTest.getName("cha"));
	}

	@Test
	public void testSetMethodInputError() {
		int[] falseInput = { 1, 2, 3 };
		try {
			abilitiesUnderTest.set(falseInput);
		} catch (InvalidParameterException ex) {
			assertEquals("The length of the given array(length=" + falseInput.length
					+ ") does not equal the the length of the abilities array(" + abilitiesUnderTest.asArray().length
					+ ")", ex.getMessage());
		}
	}

	/**
	 * This is not the best practice to test the ability priorities, but I think it
	 * is the most convenient in this situation. Maybe I'll rework it in the future.
	 */

	@Test
	public void testBarbarianPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.BARBARIAN, 1);
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("str"));
		assertTrue(abilitiesUnderTest.getValue("str") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testBardPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.BARD, 1);
		assertTrue(abilitiesUnderTest.getValue("cha") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("dex"));
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("str"));
	}

	@Test
	public void testClericPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.CLERIC, 1);
		assertTrue(abilitiesUnderTest.getValue(4) >= abilitiesUnderTest.getValue(2));
		assertTrue(abilitiesUnderTest.getValue(2) >= abilitiesUnderTest.getValue(0));
		assertTrue(abilitiesUnderTest.getValue(0) >= abilitiesUnderTest.getValue(1));
		assertTrue(abilitiesUnderTest.getValue(1) >= abilitiesUnderTest.getValue(3));
		assertTrue(abilitiesUnderTest.getValue(3) >= abilitiesUnderTest.getValue(5));
	}

	@Test
	public void testDruidPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.DRUID, 1);
		assertTrue(abilitiesUnderTest.getValue(4) >= abilitiesUnderTest.getValue(1));
		assertTrue(abilitiesUnderTest.getValue(1) >= abilitiesUnderTest.getValue(2));
		assertTrue(abilitiesUnderTest.getValue(2) >= abilitiesUnderTest.getValue(0));
		assertTrue(abilitiesUnderTest.getValue(0) >= abilitiesUnderTest.getValue(3));
		assertTrue(abilitiesUnderTest.getValue(3) >= abilitiesUnderTest.getValue(5));
	}

	@Test
	public void testFighterPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.FIGHTER, 1);
		assertTrue(abilitiesUnderTest.getValue("str") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("dex"));
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testMagePriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.MAGE, 1);
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("dex"));
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("cha"));
		assertTrue(abilitiesUnderTest.getValue("cha") >= abilitiesUnderTest.getValue("str"));
	}

	@Test
	public void testMonkPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.MONK, 1);
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("dex"));
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("str"));
		assertTrue(abilitiesUnderTest.getValue("str") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testPaladinPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.PALADIN, 1);
		assertTrue(abilitiesUnderTest.getValue(5) >= abilitiesUnderTest.getValue(4));
		assertTrue(abilitiesUnderTest.getValue(4) >= abilitiesUnderTest.getValue(0));
		assertTrue(abilitiesUnderTest.getValue(0) >= abilitiesUnderTest.getValue(2));
		assertTrue(abilitiesUnderTest.getValue(2) >= abilitiesUnderTest.getValue(1));
		assertTrue(abilitiesUnderTest.getValue(1) >= abilitiesUnderTest.getValue(3));
	}

	@Test
	public void testRangerPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.RANGER, 1);
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("str"));
		assertTrue(abilitiesUnderTest.getValue("str") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testRoguePriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.ROGUE, 1);
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("str"));
		assertTrue(abilitiesUnderTest.getValue("str") >= abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testWarlockPriorities() {
		abilitiesUnderTest.random(Race.HUMAN, Clazz.WARLOCK, 1);
		assertTrue(abilitiesUnderTest.getValue("cha") >= abilitiesUnderTest.getValue("con"));
		assertTrue(abilitiesUnderTest.getValue("con") >= abilitiesUnderTest.getValue("int"));
		assertTrue(abilitiesUnderTest.getValue("int") >= abilitiesUnderTest.getValue("wis"));
		assertTrue(abilitiesUnderTest.getValue("wis") >= abilitiesUnderTest.getValue("dex"));
		assertTrue(abilitiesUnderTest.getValue("dex") >= abilitiesUnderTest.getValue("str"));
	}

	@Test
	public void testBarbarianAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.BARBARIAN;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(11, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(11, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(12, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(12, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(13, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testBardAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.BARD;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(11, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(11, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(12, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(12, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(13, abilitiesUnderTest.getValue("cha")); // <---
	}

	@Test
	public void testClericAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.CLERIC;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(13, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testDruidAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.DRUID;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(13, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testFighterAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.FIGHTER;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(11, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(11, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(12, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(12, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(13, abilitiesUnderTest.getValue("str")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testMageAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.MAGE;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(11, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(11, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(12, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(12, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(13, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testMonkAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.MONK;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(13, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testPaladinAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.PALADIN;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(11, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(11, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(12, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(12, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(13, abilitiesUnderTest.getValue("cha")); // <---
	}

	@Test
	public void testRangerAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.RANGER;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(11, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(13, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(12, abilitiesUnderTest.getValue("wis")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testRogueAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.ROGUE;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(11, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(11, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(11, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(12, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(12, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(13, abilitiesUnderTest.getValue("dex")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(12, abilitiesUnderTest.getValue("int")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(10, abilitiesUnderTest.getValue("cha"));
	}

	@Test
	public void testWarlockAddPoints() throws InvalidInputException {
		setDefaultAbilityValues();
		Clazz clazz = Clazz.WARLOCK;

		abilitiesUnderTest.levelUp(clazz, 1, 4);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(10, abilitiesUnderTest.getValue("con"));
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(11, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 4, 8);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(11, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 8, 12);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(11, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(12, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 12, 16);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(12, abilitiesUnderTest.getValue("cha")); // <---

		abilitiesUnderTest.levelUp(clazz, 16, 20);
		assertEquals(10, abilitiesUnderTest.getValue("str"));
		assertEquals(10, abilitiesUnderTest.getValue("dex"));
		assertEquals(12, abilitiesUnderTest.getValue("con")); // <---
		assertEquals(10, abilitiesUnderTest.getValue("int"));
		assertEquals(10, abilitiesUnderTest.getValue("wis"));
		assertEquals(13, abilitiesUnderTest.getValue("cha")); // <---
	}
}
