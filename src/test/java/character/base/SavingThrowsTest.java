/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import static character.base.SavingThrows.*;
import static character.components.Clazz.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import character.DDCharacter;
import character.components.Clazz;
import character.components.Race;

/**
 * <p>
 * The test class for SavingThrows.
 * <p>
 * <b>Note:</b> This class only test the default values of the base attack
 * calculation. If the character as a dexterity mod, a constitution mod or a
 * wisdom mod lower or higher then 10 this test will fail because those mods are
 * added to the result.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.4.0
 */
public class SavingThrowsTest {
	private DDCharacter c;

	private DDCharacter factory(int level, Clazz clazz) {
		return new DDCharacter(level, Race.HUMAN, clazz, new int[] { 10, 10, 10, 10, 10, 10 }); //.build();
	}

	@Test
	public void testBarbarian() {
		c = factory(1, BARBARIAN);
		assertEquals(1, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(0, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, BARBARIAN);
		assertEquals(5, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(1, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, BARBARIAN);
		assertEquals(10, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(3, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, BARBARIAN);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(5, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, BARBARIAN);
		assertEquals(20, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(6, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testBard() {
		c = factory(1, BARD);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(2, c.getReflex());
		assertEquals(2, c.getWill());
		assertEquals(0, c.getFortitude());

		c = factory(5, BARD);
		assertEquals(3, c.getBaseAttackBonus());
		assertEquals(4, c.getReflex());
		assertEquals(4, c.getWill());
		assertEquals(1, c.getFortitude());

		c = factory(10, BARD);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(7, c.getReflex());
		assertEquals(7, c.getWill());
		assertEquals(3, c.getFortitude());

		c = factory(15, BARD);
		assertEquals(11, c.getBaseAttackBonus());
		assertEquals(9, c.getReflex());
		assertEquals(9, c.getWill());
		assertEquals(5, c.getFortitude());

		c = factory(20, BARD);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(12, c.getReflex());
		assertEquals(12, c.getWill());
		assertEquals(6, c.getFortitude());
	}

	@Test
	public void testDruid() {
		c = factory(1, DRUID);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(2, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, DRUID);
		assertEquals(3, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(4, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, DRUID);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(7, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, DRUID);
		assertEquals(11, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(9, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, DRUID);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(12, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testCleric() {
		c = factory(1, CLERIC);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(2, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, CLERIC);
		assertEquals(3, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(4, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, CLERIC);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(7, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, CLERIC);
		assertEquals(11, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(9, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, CLERIC);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(12, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testFighter() {
		c = factory(1, FIGHTER);
		assertEquals(1, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(0, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, FIGHTER);
		assertEquals(5, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(1, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, FIGHTER);
		assertEquals(10, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(3, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, FIGHTER);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(5, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, FIGHTER);
		assertEquals(20, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(6, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testMage() {
		c = factory(1, MAGE);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(2, c.getWill());
		assertEquals(0, c.getFortitude());

		c = factory(5, MAGE);
		assertEquals(2, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(4, c.getWill());
		assertEquals(1, c.getFortitude());

		c = factory(10, MAGE);
		assertEquals(5, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(7, c.getWill());
		assertEquals(3, c.getFortitude());

		c = factory(15, MAGE);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(9, c.getWill());
		assertEquals(5, c.getFortitude());

		c = factory(20, MAGE);
		assertEquals(10, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(12, c.getWill());
		assertEquals(6, c.getFortitude());
	}

	@Test
	public void testMonk() {
		c = factory(1, MONK);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(2, c.getReflex());
		assertEquals(2, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, MONK);
		assertEquals(3, c.getBaseAttackBonus());
		assertEquals(4, c.getReflex());
		assertEquals(4, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, MONK);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(7, c.getReflex());
		assertEquals(7, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, MONK);
		assertEquals(11, c.getBaseAttackBonus());
		assertEquals(9, c.getReflex());
		assertEquals(9, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, MONK);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(12, c.getReflex());
		assertEquals(12, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testPaladin() {
		c = factory(1, PALADIN);
		assertEquals(1, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(0, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, PALADIN);
		assertEquals(5, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(1, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, PALADIN);
		assertEquals(10, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(3, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, PALADIN);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(5, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, PALADIN);
		assertEquals(20, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(6, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testRanger() {
		c = factory(1, RANGER);
		assertEquals(1, c.getBaseAttackBonus());
		assertEquals(2, c.getReflex());
		assertEquals(0, c.getWill());
		assertEquals(2, c.getFortitude());

		c = factory(5, RANGER);
		assertEquals(5, c.getBaseAttackBonus());
		assertEquals(4, c.getReflex());
		assertEquals(1, c.getWill());
		assertEquals(4, c.getFortitude());

		c = factory(10, RANGER);
		assertEquals(10, c.getBaseAttackBonus());
		assertEquals(7, c.getReflex());
		assertEquals(3, c.getWill());
		assertEquals(7, c.getFortitude());

		c = factory(15, RANGER);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(9, c.getReflex());
		assertEquals(5, c.getWill());
		assertEquals(9, c.getFortitude());

		c = factory(20, RANGER);
		assertEquals(20, c.getBaseAttackBonus());
		assertEquals(12, c.getReflex());
		assertEquals(6, c.getWill());
		assertEquals(12, c.getFortitude());
	}

	@Test
	public void testRogue() {
		c = factory(1, ROGUE);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(2, c.getReflex());
		assertEquals(0, c.getWill());
		assertEquals(0, c.getFortitude());

		c = factory(5, ROGUE);
		assertEquals(3, c.getBaseAttackBonus());
		assertEquals(4, c.getReflex());
		assertEquals(1, c.getWill());
		assertEquals(1, c.getFortitude());

		c = factory(10, ROGUE);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(7, c.getReflex());
		assertEquals(3, c.getWill());
		assertEquals(3, c.getFortitude());

		c = factory(15, ROGUE);
		assertEquals(11, c.getBaseAttackBonus());
		assertEquals(9, c.getReflex());
		assertEquals(5, c.getWill());
		assertEquals(5, c.getFortitude());

		c = factory(20, ROGUE);
		assertEquals(15, c.getBaseAttackBonus());
		assertEquals(12, c.getReflex());
		assertEquals(6, c.getWill());
		assertEquals(6, c.getFortitude());
	}

	@Test
	public void testWarlock() {
		c = factory(1, WARLOCK);
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(0, c.getReflex());
		assertEquals(2, c.getWill());
		assertEquals(0, c.getFortitude());

		c = factory(5, WARLOCK);
		assertEquals(2, c.getBaseAttackBonus());
		assertEquals(1, c.getReflex());
		assertEquals(4, c.getWill());
		assertEquals(1, c.getFortitude());

		c = factory(10, WARLOCK);
		assertEquals(5, c.getBaseAttackBonus());
		assertEquals(3, c.getReflex());
		assertEquals(7, c.getWill());
		assertEquals(3, c.getFortitude());

		c = factory(15, WARLOCK);
		assertEquals(7, c.getBaseAttackBonus());
		assertEquals(5, c.getReflex());
		assertEquals(9, c.getWill());
		assertEquals(5, c.getFortitude());

		c = factory(20, WARLOCK);
		assertEquals(10, c.getBaseAttackBonus());
		assertEquals(6, c.getReflex());
		assertEquals(12, c.getWill());
		assertEquals(6, c.getFortitude());
	}
}
