/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import character.components.Clazz;
import character.components.Race;

/**
 * <p>
 * Test class for {@link BaseAttack}. This test class tests the bad baseAttacks
 * attack for characters with an STR and DEX modifier of 10.
 * 
 * @see BaseAttack
 * @see BadBaseAttackAbilities14Test
 * @see AverageBaseAttackAbilities10Test
 * @see AverageBaseAttackAbilities14Test
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 4.0.0
 */
public class BadBaseAttackAbilities10Test {
	private final Clazz[] clazzes = { Clazz.MAGE, Clazz.WARLOCK };
	private final Abilities abilities = new Abilities(Race.HUMAN, Clazz.BARD, 1);

	@BeforeEach
	public void test() {
		this.abilities.set(Abilities.STR, 10);
		this.abilities.set(Abilities.DEX, 10);
	}

	@Test
	public void testAverageBaseAttackLevel1() {
		int level = 1;
		for (Clazz z : clazzes) {
			assertEquals(0, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(0, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(0, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel2() {
		int level = 2;
		for (Clazz z : clazzes) {
			assertEquals(1, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(1, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(1, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel4() {
		int level = 4;
		for (Clazz z : clazzes) {
			assertEquals(2, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(2, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(2, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel5() {
		int level = 5;
		for (Clazz z : clazzes) {
			assertEquals(2, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(2, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(2, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel6() {
		int level = 6;
		for (Clazz z : clazzes) {
			assertEquals(3, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(3, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(3, BaseAttack.getRangeMod(z, abilities, level));
		}
	}
}
