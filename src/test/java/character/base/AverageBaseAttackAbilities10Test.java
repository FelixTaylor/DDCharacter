package character.base;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import character.components.Clazz;
import character.components.Race;

/**
 * <p>
 * Test class for {@link BaseAttack}. This test class tests the average
 * baseAttacks attack for characters with an STR and DEX modifier of 10.
 * 
 * @see BaseAttack
 * @see BadBaseAttackAbilities10Test
 * @see BadBaseAttackAbilities14Test
 * @see AverageBaseAttackAbilities14Test
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 3.0.0
 */
public class AverageBaseAttackAbilities10Test {
	private final Clazz[] clazzes = { Clazz.BARD, Clazz.CLERIC, Clazz.DRUID, Clazz.MONK, Clazz.ROGUE };
	private final Abilities abilities = new Abilities(Race.HUMAN, Clazz.BARD, 1);

	@BeforeEach
	public void test() {
		this.abilities.set(Abilities.STR, 10);
		this.abilities.set(Abilities.DEX, 10);
	}

	@Test
	public void testAverageBaseAttackLevel1() {
		int level = 1;
		for (Clazz z : clazzes) {
			assertEquals(0, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(0, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(0, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel2() {
		int level = 2;
		for (Clazz z : clazzes) {
			assertEquals(1, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(1, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(1, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel4() {
		int level = 4;
		for (Clazz z : clazzes) {
			assertEquals(3, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(3, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(3, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel5() {
		int level = 5;
		for (Clazz z : clazzes) {
			assertEquals(3, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(3, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(3, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testAverageBaseAttackLevel6() {
		int level = 6;
		for (Clazz z : clazzes) {
//			baseAttack.set(z, this.abilities, 6);
			assertEquals(4, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(4, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(4, BaseAttack.getRangeMod(z, abilities, level));
		}
	}
}
