/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import static character.base.Disposition.*;
import static character.components.Clazz.*;
import static character.components.Deity.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import character.components.Deity;
import org.junit.jupiter.api.Test;

import character.base.Disposition.Alignment;
import character.base.Disposition.Attitude;
import character.components.Clazz;

/**
 * <p>
 * The test class for Disposition
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.3.2
 */
public class DispositionTest {

	@Test
	public void testBarbarian() {
		assertTrue(isValid(BARBARIAN, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(BARBARIAN, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertFalse(isValid(BARBARIAN, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(BARBARIAN, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(BARBARIAN, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testBard() {
		assertTrue(isValid(BARD, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(BARD, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertFalse(isValid(BARD, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(BARD, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(BARD, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testClericBoccob() {
		assertTrue(isValid(CLERIC, BOCCOB, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(CLERIC, BOCCOB, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(CLERIC, BOCCOB, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(CLERIC, BOCCOB, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(CLERIC, BOCCOB, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testClericMoradin() {
		assertTrue(isValid(CLERIC, MORADIN, Alignment.RIGHTEOUS, Attitude.GOOD));
		assertTrue(isValid(CLERIC, MORADIN, Alignment.NEUTRAL, Attitude.GOOD));
		assertTrue(isValid(CLERIC, MORADIN, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(CLERIC, MORADIN, Alignment.NEUTRAL, Attitude.NEUTRAL));

		assertFalse(isValid(CLERIC, MORADIN, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertFalse(isValid(CLERIC, MORADIN, Alignment.CHAOTIC, Attitude.EVIL));
		assertFalse(isValid(CLERIC, MORADIN, Alignment.NEUTRAL, Attitude.EVIL));
	}

	@Test
	public void testClericGrumsh() {
		assertTrue(isValid(CLERIC, GRUMSH, Alignment.CHAOTIC, Attitude.EVIL));
		assertTrue(isValid(CLERIC, GRUMSH, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(CLERIC, GRUMSH, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(CLERIC, GRUMSH, Alignment.NEUTRAL, Attitude.EVIL));

		assertFalse(isValid(CLERIC, GRUMSH, Alignment.NEUTRAL, Attitude.GOOD));
		assertFalse(isValid(CLERIC, GRUMSH, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertFalse(isValid(CLERIC, GRUMSH, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertFalse(isValid(CLERIC, GRUMSH, Alignment.RIGHTEOUS, Attitude.EVIL));
	}

	@Test
	public void testDruid() {
		assertTrue(isValid(DRUID, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(DRUID, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(DRUID, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(DRUID, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(DRUID, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testFighter() {
		assertTrue(isValid(FIGHTER, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(FIGHTER, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(FIGHTER, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(FIGHTER, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(FIGHTER, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testMage() {
		assertTrue(isValid(MAGE, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(MAGE, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(MAGE, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(MAGE, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(MAGE, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testMonk() {
		assertFalse(isValid(MONK, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertFalse(isValid(MONK, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(MONK, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertFalse(isValid(MONK, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertFalse(isValid(MONK, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testPaladin() {
		assertFalse(isValid(PALADIN, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertFalse(isValid(PALADIN, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(PALADIN, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertFalse(isValid(PALADIN, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertFalse(isValid(PALADIN, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testRanger() {
		assertTrue(isValid(RANGER, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(RANGER, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(RANGER, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(RANGER, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(RANGER, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testRogue() {
		assertTrue(isValid(ROGUE, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(ROGUE, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(ROGUE, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(ROGUE, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(ROGUE, null, Alignment.NEUTRAL, Attitude.GOOD));
	}

	@Test
	public void testWarlock() {
		assertTrue(isValid(WARLOCK, null, Alignment.NEUTRAL, Attitude.NEUTRAL));
		assertTrue(isValid(WARLOCK, null, Alignment.CHAOTIC, Attitude.NEUTRAL));
		assertTrue(isValid(WARLOCK, null, Alignment.RIGHTEOUS, Attitude.NEUTRAL));
		assertTrue(isValid(WARLOCK, null, Alignment.NEUTRAL, Attitude.EVIL));
		assertTrue(isValid(WARLOCK, null, Alignment.NEUTRAL, Attitude.GOOD));
	}
}
