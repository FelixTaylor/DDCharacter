package character.base;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import character.components.Clazz;
import character.components.Race;

/**
 * <p>
 * Test class for {@link BaseAttack}. This test class tests the bad
 * baseAttacks attack for characters with an STR and DEX modifier of 14.
 * 
 * @see BaseAttack
 * @see BadBaseAttackAbilities10Test
 * @see AverageBaseAttackAbilities10Test
 * @see AverageBaseAttackAbilities14Test
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.0
 */
public class BadBaseAttackAbilities14Test {
	private final Clazz[] clazzes = { Clazz.MAGE, Clazz.WARLOCK };
	private final Abilities abilities = new Abilities(Race.HUMAN, Clazz.BARD, 1);

	@BeforeEach
	public void test() {
		this.abilities.set(Abilities.STR, 14);
		this.abilities.set(Abilities.DEX, 15);
	}

	@Test
	public void testBadBaseAttackLevel1() {
		int level = 1;
		for (Clazz z : clazzes) {
			assertEquals(0, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(2, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(2, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testBadBaseAttackLevel2() {
		int level = 2;
		for (Clazz z : clazzes) {
			assertEquals(1, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(3, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(3, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testBadBaseAttackLevel4() {
		int level = 4;
		for (Clazz z : clazzes) {
			assertEquals(2, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(4, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(4, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testBadBaseAttackLevel5() {
		int level = 5;
		for (Clazz z : clazzes) {
			assertEquals(2, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(4, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(4, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

	@Test
	public void testBadBaseAttackLevel6() {
		int level = 6;
		for (Clazz z : clazzes) {
			assertEquals(3, BaseAttack.getBaseAttackBonus(z, level));
			assertEquals(5, BaseAttack.getMeleeMod(z, abilities, level));
			assertEquals(5, BaseAttack.getRangeMod(z, abilities, level));
		}
	}

}
