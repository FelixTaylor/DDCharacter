package character.base;

import character.DDCharacter;
import character.components.Clazz;
import character.components.Race;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static character.components.Clazz.*;
import static character.components.Race.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p>
 * Test class for character skill points.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.0.0
 */
public class SkillPointTest {
    private DDCharacter factory(int level, Race race, Clazz clazz) {
        DDCharacter character = new DDCharacter(level, race, clazz, new int[]{ 10, 10, 10, 10, 10, 10, });
        character.initHitPoints();
        character.addSkillsAndDistributePoints();
        return character;
    }

    private void checkIfPointsAre(int result, Clazz[] clazzes, int level) {
        for (Object o : Arrays.stream(Race.values()).filter(r -> !r.equals(HUMAN)).toArray()) {
            Arrays.stream(clazzes).forEach(c -> assertEquals(result, factory(level, ((Race) o), c).getSkillPoints()));
        }
    }

    @Test
    public void testBarbarianDruidMonkAtLevel1() {
        final Clazz[] clazzes = {BARBARIAN, DRUID, MONK};
        final int level = 1;
        Arrays.stream(clazzes).forEach(c -> assertEquals(21, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(16, clazzes, level);
    }

    @Test
    public void testBardRangerAtLevel1() {
        final Clazz[] clazzes = {BARD, RANGER};
        final int level = 1;
        Arrays.stream(clazzes).forEach(c -> assertEquals(29, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(24, clazzes, level);
    }

    @Test
    public void testRangerAtLevel1() {
        final Clazz[] clazzes = {ROGUE};
        final int level = 1;
        Arrays.stream(clazzes).forEach(c -> assertEquals(37, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(32, clazzes, level);
    }

    @Test
    public void testClericFighterMagePaladinWarlockAtLevel1() {
        final Clazz[] clazzes = {CLERIC, FIGHTER, MAGE, PALADIN, WARLOCK};
        final int level = 1;
        Arrays.stream(clazzes).forEach(c -> assertEquals(13, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(8, clazzes, level);
    }




    @Test
    public void testBarbarianDruidMonkAtLevel3() {
        final Clazz[] clazzes = {BARBARIAN, DRUID, MONK};
        final int level = 3;
        Arrays.stream(clazzes).forEach(c -> assertEquals(31, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(24, clazzes, level);
    }

    @Test
    public void testBardRangerAtLevel3() {
        final Clazz[] clazzes = {BARD, RANGER};
        final int level = 3;
        Arrays.stream(clazzes).forEach(c -> assertEquals(43, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(36, clazzes, level);
    }

    @Test
    public void testRangerAtLevel3() {
        final Clazz[] clazzes = {ROGUE};
        final int level = 3;
        Arrays.stream(clazzes).forEach(c -> assertEquals(55, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(48, clazzes, level);
    }

    @Test
    public void testClericFighterMagePaladinWarlockAtLevel3() {
        final Clazz[] clazzes = {CLERIC, FIGHTER, MAGE, PALADIN, WARLOCK};
        final int level = 3;
        Arrays.stream(clazzes).forEach(c -> assertEquals(19, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(12, clazzes, level);
    }




    @Test
    public void testBarbarianDruidMonkAtLevel6() {
        final Clazz[] clazzes = {BARBARIAN, DRUID, MONK};
        final int level = 6;
        Arrays.stream(clazzes).forEach(c -> assertEquals(46, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(36, clazzes, level);
    }

    @Test
    public void testBardRangerAtLevel6() {
        final Clazz[] clazzes = {BARD, RANGER};
        final int level = 6;
        Arrays.stream(clazzes).forEach(c -> assertEquals(64, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(54, clazzes, level);
    }

    @Test
    public void testRangerAtLevel6() {
        final Clazz[] clazzes = {ROGUE};
        final int level = 6;
        Arrays.stream(clazzes).forEach(c -> assertEquals(82, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(72, clazzes, level);
    }

    @Test
    public void testClericFighterMagePaladinWarlockAtLevel6() {
        final Clazz[] clazzes = {CLERIC, FIGHTER, MAGE, PALADIN, WARLOCK};
        final int level = 6;
        Arrays.stream(clazzes).forEach(c -> assertEquals(28, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(18, clazzes, level);
    }




    @Test
    public void testBarbarianDruidMonkAtLevel12() {
        final Clazz[] clazzes = {BARBARIAN, DRUID, MONK};
        final int level = 12;
        Arrays.stream(clazzes).forEach(c -> assertEquals(76, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(60, clazzes, level);
    }

    @Test
    public void testBardRangerAtLevel12() {
        final Clazz[] clazzes = {BARD, RANGER};
        final int level = 12;
        Arrays.stream(clazzes).forEach(c -> assertEquals(106, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(90, clazzes, level);
    }

    @Test
    public void testRangerAtLevel12() {
        final Clazz[] clazzes = {ROGUE};
        final int level = 12;
        Arrays.stream(clazzes).forEach(c -> assertEquals(136, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(120, clazzes, level);
    }

    @Test
    public void testClericFighterMagePaladinWarlockAtLevel12() {
        final Clazz[] clazzes = {CLERIC, FIGHTER, MAGE, PALADIN, WARLOCK};
        final int level = 12;
        Arrays.stream(clazzes).forEach(c -> assertEquals(46, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(30, clazzes, level);
    }




    @Test
    public void testBarbarianDruidMonkAtLevel20() {
        final Clazz[] clazzes = {BARBARIAN, DRUID, MONK};
        final int level = 20;
        Arrays.stream(clazzes).forEach(c -> assertEquals(116, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(92, clazzes, level);
    }

    @Test
    public void testBardRangerAtLevel20() {
        final Clazz[] clazzes = {BARD, RANGER};
        final int level = 20;
        Arrays.stream(clazzes).forEach(c -> assertEquals(162, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(138, clazzes, level);
    }

    @Test
    public void testRangerAtLevel20() {
        final Clazz[] clazzes = {ROGUE};
        final int level = 20;
        Arrays.stream(clazzes).forEach(c -> assertEquals(208, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(184, clazzes, level);
    }

    @Test
    public void testClericFighterMagePaladinWarlockAtLevel20() {
        final Clazz[] clazzes = {CLERIC, FIGHTER, MAGE, PALADIN, WARLOCK};
        final int level = 20;
        Arrays.stream(clazzes).forEach(c -> assertEquals(70, factory(level, HUMAN, c).getSkillPoints()));
        checkIfPointsAre(46, clazzes, level);
    }
}
