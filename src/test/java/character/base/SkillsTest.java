/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import character.components.Clazz;
import character.components.Race;

/**
 * <p>
 * Test class for skills.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.1.2
 */
class SkillsTest {
	private final Skills skills = new Skills();
	private Clazz clazz;
	private Abilities abilities;

	private void initData(Clazz clazz) {
		this.clazz = clazz;
		this.abilities = new Abilities(Race.HUMAN, clazz, 1);
	}

//	@AfterEach
//	public void printSkills() {
//		System.out.println("\n" + this.clazz.getName() + " (" + this.skills.size() + ")");
//
//		for (Map.Entry<String, Skill> entry : this.skills.entrySet()) {
//			System.out.println(entry.getValue().getName() + ": power=" + entry.getValue().getPower() + ",rank="
//					+ entry.getValue().getRank() + ", isDefault=" + entry.getValue().isDefaultSkill());
//		}
//	}

	@Test
	public void testBard() {
		initData(Clazz.BARD);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(39, this.skills.getSkills().size());
	}

	@Test
	public void testBarbarian() {
		initData(Clazz.BARBARIAN);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(25, this.skills.getSkills().size());
	}

	@Test
	public void testCleric() {
		initData(Clazz.CLERIC);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(30, this.skills.getSkills().size());
	}

	@Test
	public void testDruid() {
		initData(Clazz.DRUID);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(28, this.skills.getSkills().size());
	}

	@Test
	public void testFighter() {
		initData(Clazz.FIGHTER);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(25, this.skills.getSkills().size());
	}

	@Test
	public void testMage() {
		initData(Clazz.MAGE);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(37, this.skills.getSkills().size());
	}

	@Test
	public void testMonk() {
		initData(Clazz.MONK);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(29, this.skills.getSkills().size());
	}

	@Test
	public void testPaladin() {
		initData(Clazz.PALADIN);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(28, this.skills.getSkills().size());
	}

	@Test
	public void testRanger() {
		initData(Clazz.RANGER);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(29, this.skills.getSkills().size());
	}

	@Test
	public void testRogue() {
		initData(Clazz.ROGUE);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(33, this.skills.getSkills().size());
	}

	@Test
	public void testWarlock() {
		initData(Clazz.WARLOCK);
		this.skills.setSkills(this.clazz, this.abilities);
		assertEquals(27, this.skills.getSkills().size());
	}
}
