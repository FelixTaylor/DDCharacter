/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import character.base.AbilitiesTest;
import character.base.Disposition.Alignment;
import character.base.Disposition.Attitude;
import character.components.Clazz;
import character.components.Deity;
import character.components.Gender;
import character.components.Race;

/**
 * <p>
 * The test class for DDCharacter
 * <p>
 * <b>Note:</b> The abilities of the characters a tested in the
 * {@link AbilitiesTest AbilitiesTest} class because the ability methods in the
 * DDCharacter object are redirected to the Abilities object.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.7.4
 */
public class DDCharacterTest {
	private final DDCharacter c = new DDCharacter(1);

	/**
	 * <p>
	 * Testing the constructor to validate if all the attributes are set to there
	 * default values.
	 */
	@Test
	public void testConstructor() {
		// In this method I test if the default values of a new created character are
		// set correctly for further use.
		assertEquals("DDCharacter", c.getName());
		assertEquals(1, c.getLevel());
		assertEquals(0, c.getExperience());
		assertNull(c.getRace());
		assertNull(c.getClazz());

		assertEquals(1, c.getAbilityValue("str"));
		assertEquals(1, c.getAbilityValue("dex"));
		assertEquals(1, c.getAbilityValue("con"));
		assertEquals(1, c.getAbilityValue("int"));
		assertEquals(1, c.getAbilityValue("wis"));
		assertEquals(1, c.getAbilityValue("cha"));

		assertEquals(0, c.getHitPoints());
		assertEquals(0, c.getMaxHitPoints());
		assertEquals(0, c.getSkillPoints());
		assertEquals(0, c.getSkills().size());
		assertEquals(true, c.isAlive());
		assertEquals(Gender.FEMALE, c.getGender());
		assertEquals(Alignment.NONE, c.getAlignment());
		assertEquals(Attitude.NONE, c.getAttitude());
		assertEquals(Deity.NONE, c.getDeity());
		assertEquals(0, c.getTalentPoints());

		// Following statements produce a console warning because the race and
		// clazz of the character was not yet set.
		assertEquals(0, c.getBaseAttackBonus());
		assertEquals(0, c.getMeleeMod());
		assertEquals(0, c.getRangeMod());
	}

	@Test
	public void testCanLevelUp() {
		c.set(1);
		c.addExperience(999);
		assertFalse(c.canLevelUp());

		c.addExperience(1);
		assertTrue(c.canLevelUp());
	}

	@Test
	public void testTalentPointsForHuman() {
		c.set(Race.HUMAN);
		c.setLevel(3);
		assertEquals(3, c.getTalentPoints());
	}

	@Test
	public void testTalentPointsForNonHumans() {
		c.set(Race.DWARF);
		c.setLevel(3);
		assertEquals(2, c.getTalentPoints());
	}

	@Test
	public void testExperience() {
		assertEquals(1, c.getLevel());
		c.setLevel(10);
		assertEquals(10, c.getLevel());
	}

	@Test
	public void testToString() {
		c.set("TestName", Race.ELF, Clazz.BARD, 1, new int[] { 10, 10, 10, 10, 10, 10 });
		assertEquals("TestName [ -1, Elf, Bard, 1, 10 10 10 10 10 10 ]", c.toString());
	}

	@Test
	public void testLevelUp() {
		DDCharacter character = new DDCharacter(1, Race.HUMAN, Clazz.BARBARIAN, new int[] { 10, 10, 10, 10, 10, 10 });
		character.initHitPoints();

		assertEquals(1, character.getLevel());

		character.addExperience(1000);
		character.levelUp();
		assertEquals(2, character.getLevel());
	}

	@Test
	public void testUUID() {
		assertNotEquals(new DDCharacter(1).getUUID(), new DDCharacter(1).getUUID());
	}

}
