/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.misc;

import character.DDCharacter;
import character.components.Clazz;
import character.components.Race;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestHitPoints {
	private DDCharacter c;

	private DDCharacter factory(Clazz clazz, int hitPoints) {
		DDCharacter character = new DDCharacter(1, clazz, Race.HUMAN, new int[] { 10, 10, 10, 10, 10, 10 });
		character.calculateMaxHitPoints();
		character.setHitPoints(hitPoints);
		return character;
	}

	@Test
	public void testIfKnockedOutIsFalse() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, 1);
			assertFalse(c.isKnockedOut());
		}
	}

	@Test
	public void testIfKnockedOutIsTrueAtZeroHitPoints() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, 0);
			assertTrue(c.isKnockedOut());
		}
	}

	@Test
	public void testIfKnockedOutIsTrueBelowZeroHitPoints() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, -1);
			assertTrue(c.isKnockedOut());
		}
	}

	@Test
	public void testIfKnockedOutIsTrueAtMinus9() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, -9);
			assertTrue(c.isKnockedOut());
		}
	}

	@Test
	public void testIsAliveAtMinus9EqualsTrue() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, -9);
			assertTrue(c.isAlive());
		}
	}

	@Test
	public void testIsAliveEqualsFalseAtMinus11HitPoints() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, -10);
			assertFalse(c.isAlive());
		}
	}

	@Test
	public void testHitPointsCanNotFallBelowMinus10() {
		for (Clazz clazz : Clazz.values()) {
			c = factory(clazz, -11);
			assertEquals(-10, c.getHitPoints());
		}
	}
}
