/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * <p>
 * The <code>EntityTest</code> class tests the abstract class {@link Entity}.
 * <b>Note:</b> In order to test this class the unit test creates the
 * <code>E</code> object which extends <code>Entity</code> and uses that for
 * testing.
 * 
 * @author FelixTaylor
 * @since 0.1.0
 * @version 2.0.2
 */
class EntityTest {

	private final E entityUnderTest = new E();

	private static class E extends Entity {
		// New object because Entity is abstract.
	}

	@Test
	public void testEntityDefaultName() {
		assertEquals("New Entity", new E().getName());
	}

	@Test
	public void testEntityDefaultId() {
		assertEquals(-1L, new E().getId());
	}

	@Test
	public void testSetName() {
		entityUnderTest.setName("NewAwesomeName");
		assertEquals("NewAwesomeName", entityUnderTest.getName());
	}

	@Test
	public void testSetNameWithMultipleWhitespaces() {
		entityUnderTest.setName("    Name     With      Spaces   ");
		assertEquals("Name With Spaces", entityUnderTest.getName());
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testNullInputForSetName() {
		try {
			entityUnderTest.setName(null);
		} catch (NullPointerException ex) {
			assertEquals("Name may not be null.", ex.getMessage());
		}
	}

	@Test
	public void testEmptyInputForSetName() {
		try {
			entityUnderTest.setName("");
		} catch (IllegalArgumentException ex) {
			assertEquals("Invalid name input. The given parameter is not allowed to be empty.", ex.getMessage());
		}
	}

	@Test
	public void testSetId() {
		entityUnderTest.setId(42);
		assertEquals(42, entityUnderTest.getId());
	}

}
