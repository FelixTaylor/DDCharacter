package character.components;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * <p>
 * The test class for the {@link DDLevel} class. Note that not all levels are
 * tested, mostly because there is no max level. The official Book states only
 * the information up to level 20.
 * <p>
 * Every used level is tested by three methods:
 * <ul>
 * <li>testExpLevel[level]
 * <li>testRequiredExpForLevel[level+1]
 * <li>testMaxSkillRankForLevel[level]
 * </ul>
 * 
 * @author FelixTaylor
 * @since 0.1.0
 * @version 1.0.4
 */
class DDLevelTest {
	private final DDLevel level = new DDLevel(1);

	@Test
	public void testRequiredExpForLevelTwo() {
		assertEquals(1000, level.getRequiredExpForNextLevel());
	}

	@Test
	public void testMaxSkillRankForLevelOne() {
		assertEquals(4, level.getMaxSkillRank());
	}

	@Test
	public void testRequiredExpForLevelFour() {
		level.setLevel(3);
		assertEquals(6000, level.getRequiredExpForNextLevel());
	}

	@Test
	public void testMaxSkillRankForLevelThree() {
		level.setLevel(3);
		assertEquals(6, level.getMaxSkillRank());
	}

	@Test
	public void testRequiredExpForLevelSeven() {
		level.setLevel(6);
		assertEquals(21000, level.getRequiredExpForNextLevel());
	}

	@Test
	public void testMaxSkillRankForLevelSix() {
		level.setLevel(6);
		assertEquals(9, level.getMaxSkillRank());
	}

	@Test
	public void testRequiredExpForLevelSeventeen() {
		level.setLevel(16);
		assertEquals(136000, level.getRequiredExpForNextLevel());
	}

	@Test
	public void testMaxSkillRankForLevelSixteen() {
		level.setLevel(16);
		assertEquals(19, level.getMaxSkillRank());
	}

	@Test
	public void testRequiredExpForLevelTwentyone() {
		level.setLevel(20);
		assertEquals(210000, level.getRequiredExpForNextLevel());
	}

	@Test
	public void testMaxSkillRankForLevelTwenty() {
		level.setLevel(20);
		assertEquals(23, level.getMaxSkillRank());
	}
}
