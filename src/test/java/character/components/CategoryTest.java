/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import equipment.base.Category;

/**
 * <p>
 * Test Class for {@link Size} enum
 *
 * @see Size
 *
 * @author FelixTaylor
 * @since 0.1.0
 * @version 2.0.2
 */
class CategoryTest {

	@Test
	public void testCategoryEnumSize() {
		assertEquals(3, Category.values().length);
	}

	@Test
	public void testIfAllSizesAreTested() {
		// Note: We have to decrease the value of the array length to get the
		// correct value because of this method and the testCategoryEnumSize method.
		assertEquals(Category.values().length, CategoryTest.this.getClass().getDeclaredMethods().length - 2);
	}

	@Test
	public void testSmallSizeName() {
		assertEquals("Small", Category.SMALL.getCategory());
	}

	@Test
	public void testMediumSizeName() {
		assertEquals("Medium", Category.MEDIUM.getCategory());
	}

	@Test
	public void testLargeSizeName() {
		assertEquals("Large", Category.LARGE.getCategory());
	}

}
