/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * <p>
 * Test class for the {@link Gender} enum.
 * 
 * @see Gender
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.1
 */
public class GenderTest {

	@Test
	public void testGenderEnumSize() {
		assertEquals(4, Gender.values().length);
	}

	@Test
	public void testIfAllGendersAreTested() {
		// Note: We have to decrease the value of the array length to get the
		// correct value because of this method and the testGenderEnumSize method.
		assertEquals(Gender.values().length, GenderTest.this.getClass().getDeclaredMethods().length - 2);
	}

	@Test
	public void testFemaleName() {
		assertEquals("Female", Gender.FEMALE.getName());
	}

	@Test
	public void testMaleName() {
		assertEquals("Male", Gender.MALE.getName());
	}

	@Test
	public void testNoneGenderName() {
		assertEquals("None", Gender.NONE.getName());
	}

	@Test
	public void testQueerGenderName() {
		assertEquals("Queer", Gender.QUEER.getName());
	}

}
