/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * <p>
 * Test class for the {@link Race} enum.
 * 
 * @see Race
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.1
 */
public class RaceTest {

	// Every race except for halfelf's and humans has a strong ability and a weak
	// ability. The strong ability gets a bonus of plus two, while the weak ability
	// is decreased by two.

	@Test
	public void testRaceEnumSize() {
		assertEquals(7, Race.values().length);
	}

	@Test
	public void testIfAllRacesAreTested() {
		// Note: We have to decrease the value of the array length to get the
		// correct value because of this method and the testRaceEnumSize method.
		assertEquals(Race.values().length * 4, RaceTest.this.getClass().getDeclaredMethods().length - 2);
	}

	//
	// DWARF
	//

	@Test
	public void testDwarfName() {
		assertEquals("Dwarf", Race.DWARF.getName());
	}

	@Test
	public void testDwarfSize() {
		assertEquals(Size.DWARF, Race.DWARF.getSize());
	}

	@Test
	public void testDwarfPositiveAbility() {
		assertEquals("con", Race.DWARF.getPositiveAbility());
	}

	@Test
	public void testDwarfNegativeAbility() {
		assertEquals("cha", Race.DWARF.getNegativeAbility());
	}

	//
	// ELF
	//

	@Test
	public void testElfName() {
		assertEquals("Elf", Race.ELF.getName());
	}

	@Test
	public void testElfSize() {
		assertEquals(Size.ELF, Race.ELF.getSize());
	}

	@Test
	public void testElfPositiveAbility() {
		assertEquals("dex", Race.ELF.getPositiveAbility());
	}

	@Test
	public void testElfNegativeAbility() {
		assertEquals("con", Race.ELF.getNegativeAbility());
	}

	//
	// GNOME
	//

	@Test
	public void testGnomeName() {
		assertEquals("Gnome", Race.GNOME.getName());
	}

	@Test
	public void testGnomeSize() {
		assertEquals(Size.GNOME, Race.GNOME.getSize());
	}

	@Test
	public void testGnomePositiveAbility() {
		assertEquals("con", Race.GNOME.getPositiveAbility());
	}

	@Test
	public void testGnomeNegativeAbility() {
		assertEquals("str", Race.GNOME.getNegativeAbility());
	}

	//
	// HALFELF
	//

	@Test
	public void testHalfelfName() {
		assertEquals("Halfelf", Race.HALFELF.getName());
	}

	@Test
	public void testHalfelfSize() {
		assertEquals(Size.HALFELF, Race.HALFELF.getSize());
	}

	@Test
	public void testHalfelfPositiveAbility() {
		assertEquals("NONE", Race.HALFELF.getPositiveAbility());
	}

	@Test
	public void testHalfelfNegativeAbility() {
		assertEquals("NONE", Race.HALFELF.getNegativeAbility());
	}

	//
	// ELFLING
	//

	@Test
	public void testHalflingName() {
		assertEquals("Halfling", Race.HALFLING.getName());
	}

	@Test
	public void testHalflingSize() {
		assertEquals(Size.HALFLING, Race.HALFLING.getSize());
	}

	@Test
	public void testHalflingPositiveAbility() {
		assertEquals("dex", Race.HALFLING.getPositiveAbility());
	}

	@Test
	public void testHalflingNegativeAbility() {
		assertEquals("str", Race.HALFLING.getNegativeAbility());
	}

	//
	// ELFORK
	//

	@Test
	public void testHalforkName() {
		assertEquals("Halfork", Race.HALFORK.getName());
	}

	@Test
	public void testHalforkSize() {
		assertEquals(Size.HALFORK, Race.HALFORK.getSize());
	}

	@Test
	public void testHalforkPositiveAbility() {
		assertEquals("str", Race.HALFORK.getPositiveAbility());
	}

	@Test
	public void testHalforkNegativeAbility() {
		assertEquals("cha", Race.HALFORK.getNegativeAbility());
	}

	//
	// HUMAN
	//

	@Test
	public void testHumanName() {
		assertEquals("Human", Race.HUMAN.getName());
	}

	@Test
	public void testHumanSize() {
		assertEquals(Size.HUMAN, Race.HUMAN.getSize());
	}

	@Test
	public void testHumanPositiveAbility() {
		assertEquals("NONE", Race.HUMAN.getPositiveAbility());
	}

	@Test
	public void testHumanNegativeAbility() {
		assertEquals("NONE", Race.HUMAN.getNegativeAbility());
	}

}
