/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import exceptions.InvalidInputException;

/**
 * <p>
 * The <code>AbilityTest</code> class tests a single {@link Ability}.
 * <b>Note:</b> The <code>Ability.setName(String)</code> and
 * <code>Ability.setId(long)</code> methods are not tested in this class because
 * they are already tested in the {@link EntityTest} unit test.
 * 
 * @author FelixTaylor
 * @since 0.1.0
 * @version 2.0.2
 */
public class AbilityTest {
	private final Ability abilityUnderTest = new Ability("ability");

	@Test
	public void testEntityDefaultValue() {
		assertEquals(1, abilityUnderTest.getValue());
	}

	@Test
	public void testEntityDefaultMod() {
		assertEquals(-5, abilityUnderTest.getMod());
	}

	@Test
	public void testGetName() {
		assertEquals("ability", abilityUnderTest.getName());
	}

	@Test
	public void testSetValue() throws InvalidInputException {
		abilityUnderTest.setValue(42);
		assertEquals(42, abilityUnderTest.getValue());
	}

	@Test
	public void testModForValueOf10() throws InvalidInputException {
		abilityUnderTest.setValue(10);
		assertEquals(0, abilityUnderTest.getMod());
	}

	@Test
	public void testModForValueOf15() throws InvalidInputException {
		abilityUnderTest.setValue(15);
		assertEquals(2, abilityUnderTest.getMod());
	}

	@Test
	public void testModForValueOf20() throws InvalidInputException {
		abilityUnderTest.setValue(20);
		assertEquals(5, abilityUnderTest.getMod());
	}

	@Test
	public void testNegativeInputValueForSetValue() {
		try {
			abilityUnderTest.setValue(-5);
		} catch (InvalidInputException ex) {
			assertEquals("Invalid parameter value: '-5'. The value has to be greater then 0 (zero).", ex.getMessage());
		}
	}

}
