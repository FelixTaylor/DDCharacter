/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exceptions.InvalidInputException;

/**
 * <p>
 * Test class for testing the basic skill class
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.0.1
 */
class SkillTest {

	@Test
	public void testSkill() throws InvalidInputException {
		Ability str = new Ability("strength");
		Skill s = new Skill("Fight", 0, str, true);

		str.setValue(10);
		for (int i = 0; i <= 20; i++) {
			s.setRank(i);
			assertEquals(i, s.getPower());
		}

		s.setRank(0);
		str.setValue(10);
		assertEquals(0, s.getPower());

		s.setRank(2);
		str.setValue(14);
		assertEquals(4, s.getPower());

		s.setRank(5);
		str.setValue(20);
		assertEquals(10, s.getPower());
	}

	@Test
	public void testSkillAddRank() {
		Ability str = new Ability("strength");
		Skill s = new Skill("Fight", 0, str, true);
		s.addRank();
		assertEquals(1, s.getRank());
	}
}
