/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * <p>
 * The <code>ClazzTest</code> class tests the {@link Clazz} enum.
 * 
 * @author FelixTaylor
 * @since 0.1.0
 * @version 2.0.1
 */
public class ClazzTest {

	@Test
	public void testIfAllClazzesAreTested() {
		// Note: We have to decrease the value of the array length to get the
		// correct value because of this method and the testClazzEnumSize method.
		assertEquals(Clazz.values().length * 2, ClazzTest.this.getClass().getDeclaredMethods().length - 2);
	}

	@Test
	public void testClazzEnumSize() {
		assertEquals(11, Clazz.values().length);
	}

	@Test
	public void testBarbarianName() {
		assertEquals("Barbarian", Clazz.BARBARIAN.getName());
	}

	@Test
	public void testBarbarianPriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "wis", "int", "con", "str", "dex" }),
				Arrays.toString(Clazz.BARBARIAN.getPriorities()));
	}

	@Test
	public void testBardName() {
		assertEquals("Bard", Clazz.BARD.getName());
	}

	@Test
	public void testBardPriorities() {
		assertEquals(Arrays.toString(new String[] { "str", "wis", "int", "dex", "con", "cha" }),
				Arrays.toString(Clazz.BARD.getPriorities()));
	}

	@Test
	public void testClericName() {
		assertEquals("Cleric", Clazz.CLERIC.getName());
	}

	@Test
	public void testClericPriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "int", "dex", "str", "con", "wis" }),
				Arrays.toString(Clazz.CLERIC.getPriorities()));
	}

	@Test
	public void testDruidName() {
		assertEquals("Druid", Clazz.DRUID.getName());
	}

	@Test
	public void testDruidPriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "int", "str", "con", "dex", "wis" }),
				Arrays.toString(Clazz.DRUID.getPriorities()));
	}

	@Test
	public void testFighterName() {
		assertEquals("Fighter", Clazz.FIGHTER.getName());
	}

	@Test
	public void testFighterPriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "wis", "int", "dex", "con", "str" }),
				Arrays.toString(Clazz.FIGHTER.getPriorities()));
	}

	@Test
	public void testMageName() {
		assertEquals("Mage", Clazz.MAGE.getName());
	}

	@Test
	public void testMagePriorities() {
		assertEquals(Arrays.toString(new String[] { "str", "cha", "dex", "wis", "con", "int" }),
				Arrays.toString(Clazz.MAGE.getPriorities()));
	}

	@Test
	public void testMonkName() {
		assertEquals("Monk", Clazz.MONK.getName());
	}

	@Test
	public void testMonkPriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "con", "int", "str", "dex", "wis" }),
				Arrays.toString(Clazz.MONK.getPriorities()));
	}

	@Test
	public void testPaladinName() {
		assertEquals("Paladin", Clazz.PALADIN.getName());
	}

	@Test
	public void testPaladinPriorities() {
		assertEquals(Arrays.toString(new String[] { "int", "dex", "con", "str", "wis", "cha" }),
				Arrays.toString(Clazz.PALADIN.getPriorities()));
	}

	@Test
	public void testRangerName() {
		assertEquals("Ranger", Clazz.RANGER.getName());
	}

	@Test
	public void testRangerPriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "int", "con", "str", "wis", "dex" }),
				Arrays.toString(Clazz.RANGER.getPriorities()));
	}

	@Test
	public void testRogueName() {
		assertEquals("Rogue", Clazz.ROGUE.getName());
	}

	@Test
	public void testRoguePriorities() {
		assertEquals(Arrays.toString(new String[] { "cha", "str", "wis", "con", "int", "dex" }),
				Arrays.toString(Clazz.ROGUE.getPriorities()));
	}

	@Test
	public void testWarlockName() {
		assertEquals("Warlock", Clazz.WARLOCK.getName());
	}

	@Test
	public void testWarlockPriorities() {
		assertEquals(Arrays.toString(new String[] { "str", "dex", "wis", "int", "con", "cha" }),
				Arrays.toString(Clazz.WARLOCK.getPriorities()));
	}

}
