/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import character.base.Disposition.Alignment;
import character.base.Disposition.Attitude;

/**
 * <p>
 * The <code>DeityTest</code> class tests the {@link Deity} enum.
 * 
 * @author FelixTaylor
 * @since 0.1.0
 * @version 2.1.0
 */
public class DeityTest {

	@Test
	public void testIfAllDeitiesAreTested() {
		// Note: We have to decrease the value of the array length to get the
		// correct value because of this method and the testDeityEnumSize method.
		assertEquals(Deity.values().length * 3, DeityTest.this.getClass().getDeclaredMethods().length - 2);
	}

	@Test
	public void testDeityEnumSize() {
		assertEquals(20, Deity.values().length);
	}

	// Boccob

	@Test
	public void testBoccobName() {
		assertEquals("Boccob", Deity.BOCCOB.getName());
	}

	@Test
	public void testBoccobDescription() {
		assertEquals("God of magic, arcane knowledge, balance and foresight.", Deity.BOCCOB.getDescription());
	}

	@Test
	public void testBoccobDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.BOCCOB.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.BOCCOB.getAttitude());
	}

	// Corellon Larethian

	@Test
	public void testCorellonLarethianName() {
		assertEquals("Corellon Larethian", Deity.CORELLON_LARETHIAN.getName());
	}

	@Test
	public void testCorellonLarethianDescription() {
		assertEquals("God of elves, magic, music, and arts (also a demi-human power).",
				Deity.CORELLON_LARETHIAN.getDescription());
	}

	@Test
	public void testCorellonLarethianDisposition() {
		assertEquals(Alignment.CHAOTIC, Deity.CORELLON_LARETHIAN.getAlignment());
		assertEquals(Attitude.GOOD, Deity.CORELLON_LARETHIAN.getAttitude());
	}

	// Ehlonna

	@Test
	public void testEhlonnaName() {
		assertEquals("Ehlonna", Deity.EHLONNA.getName());
	}

	@Test
	public void testEhlonnaDescription() {
		assertEquals("Goddess of forests, woodlands, flora & fauna, and fertility.", Deity.EHLONNA.getDescription());
	}

	@Test
	public void testEhlonnaDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.EHLONNA.getAlignment());
		assertEquals(Attitude.GOOD, Deity.EHLONNA.getAttitude());
	}

	// Erythnul

	@Test
	public void testErythnulName() {
		assertEquals("Erythnul", Deity.ERYTHNUL.getName());
	}

	@Test
	public void testErythnulDescription() {
		assertEquals("God of hate, envy, malice, panic, ugliness, and slaughter.", Deity.ERYTHNUL.getDescription());
	}

	@Test
	public void testErythnulDisposition() {
		assertEquals(Alignment.CHAOTIC, Deity.ERYTHNUL.getAlignment());
		assertEquals(Attitude.EVIL, Deity.ERYTHNUL.getAttitude());
	}

	// Fharlanghn

	@Test
	public void testFharlanghnName() {
		assertEquals("Fharlanghn", Deity.FHARLANGHN.getName());
	}

	@Test
	public void testFharlanghnDescription() {
		assertEquals("God of horizons, distance, travel, and roads.", Deity.FHARLANGHN.getDescription());
	}

	@Test
	public void testFharlanghnDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.FHARLANGHN.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.FHARLANGHN.getAttitude());
	}

	// Garl Glittergold

	@Test
	public void testGarlGlittergoldName() {
		assertEquals("Garl Glittergold", Deity.GARL_GLITTERGOLD.getName());
	}

	@Test
	public void testGarlGlittergoldDescription() {
		assertEquals("God of gnomes, humor, and gemcutting (also a demi-human power).",
				Deity.GARL_GLITTERGOLD.getDescription());
	}

	@Test
	public void testGarlGlittergoldDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.GARL_GLITTERGOLD.getAlignment());
		assertEquals(Attitude.GOOD, Deity.GARL_GLITTERGOLD.getAttitude());
	}

	// Grummsch

	@Test
	public void testGrummschName() {
		assertEquals("Grummsch", Deity.GRUMSH.getName());
	}

	@Test
	public void testGrummschDescription() {
		assertEquals("God of orcs (also a monster power).", Deity.GRUMSH.getDescription());
	}

	@Test
	public void testGrummschDisposition() {
		assertEquals(Alignment.CHAOTIC, Deity.GRUMSH.getAlignment());
		assertEquals(Attitude.EVIL, Deity.GRUMSH.getAttitude());
	}

	// Heironeous

	@Test
	public void testHeironeousName() {
		assertEquals("Heironeous", Deity.HEIRONEOUS.getName());
	}

	@Test
	public void testHeironeousDescription() {
		assertEquals("God of chivalry, justice, honor, war, daring, and valor.", Deity.HEIRONEOUS.getDescription());
	}

	@Test
	public void testHeironeousDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.HEIRONEOUS.getAlignment());
		assertEquals(Attitude.GOOD, Deity.HEIRONEOUS.getAttitude());
	}

	// Hextor

	@Test
	public void testHextorName() {
		assertEquals("Hextor", Deity.HEXTOR.getName());
	}

	@Test
	public void testHextorDescription() {
		assertEquals("God of war, discord, massacres, conflict, fitness, and tyranny.", Deity.HEXTOR.getDescription());
	}

	@Test
	public void testHextorDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.HEXTOR.getAlignment());
		assertEquals(Attitude.EVIL, Deity.HEXTOR.getAttitude());
	}

	// Kord

	@Test
	public void testKordName() {
		assertEquals("Kord", Deity.KORD.getName());
	}

	@Test
	public void testKordDescription() {
		assertEquals("God of athletics, sports, brawling, strength, and courage.", Deity.KORD.getDescription());
	}

	@Test
	public void testKordDisposition() {
		assertEquals(Alignment.CHAOTIC, Deity.KORD.getAlignment());
		assertEquals(Attitude.GOOD, Deity.KORD.getAttitude());
	}

	// Moradin

	@Test
	public void testMoradinName() {
		assertEquals("Moradin", Deity.MORADIN.getName());
	}

	@Test
	public void testMoradinDescription() {
		assertEquals("God of dwarfs (also a demi-human power).", Deity.MORADIN.getDescription());
	}

	@Test
	public void testMoradinDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.MORADIN.getAlignment());
		assertEquals(Attitude.GOOD, Deity.MORADIN.getAttitude());
	}

	// Nerull

	@Test
	public void testNerullName() {
		assertEquals("Nerull", Deity.NERULL.getName());
	}

	@Test
	public void testNerullDescription() {
		assertEquals("God of death, darkness, murder and the underworld.", Deity.NERULL.getDescription());
	}

	@Test
	public void testNerullDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.NERULL.getAlignment());
		assertEquals(Attitude.EVIL, Deity.NERULL.getAttitude());
	}

	// Obad-Hai

	@Test
	public void testObadHaiName() {
		assertEquals("Obad-Hai", Deity.OBAD_HAI.getName());
	}

	@Test
	public void testObadHaiDescription() {
		assertEquals("God of nature, freedom, hunting, and beasts.", Deity.OBAD_HAI.getDescription());
	}

	@Test
	public void testObadHaiDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.OBAD_HAI.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.OBAD_HAI.getAttitude());
	}

	// Olidammara

	@Test
	public void testOlidammaraName() {
		assertEquals("Olidammara", Deity.OLIDAMMARA.getName());
	}

	@Test
	public void testOlidammaraDescription() {
		assertEquals("God of music, revels, wine, rogues, humor, and tricks.", Deity.OLIDAMMARA.getDescription());
	}

	@Test
	public void testOlidammaraDisposition() {
		assertEquals(Alignment.CHAOTIC, Deity.OLIDAMMARA.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.OLIDAMMARA.getAttitude());
	}

	// Pelor

	@Test
	public void testPelorName() {
		assertEquals("Pelor", Deity.PELOR.getName());
	}

	@Test
	public void testPelorDescription() {
		assertEquals("God of sun, light, strength and healing. More humans worship Pelor than any other deity.",
				Deity.PELOR.getDescription());
	}

	@Test
	public void testPelorDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.PELOR.getAlignment());
		assertEquals(Attitude.GOOD, Deity.PELOR.getAttitude());
	}

	// St. Cuthbert

	@Test
	public void testStCuthbertName() {
		assertEquals("St. Cuthbert", Deity.ST_CUTHBERT.getName());
	}

	@Test
	public void testStCuthbertDescription() {
		assertEquals("God of common sense, wisdom, zeal, honesty, truth, and discipline.",
				Deity.ST_CUTHBERT.getDescription());
	}

	@Test
	public void testStCuthbertDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.ST_CUTHBERT.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.ST_CUTHBERT.getAttitude());
	}

	// Wee Jas

	@Test
	public void testWeeJasName() {
		assertEquals("Wee Jas", Deity.WEE_JAS.getName());
	}

	@Test
	public void testWeeJasDescription() {
		assertEquals("Goddess of magic, death, vanity, and law.", Deity.WEE_JAS.getDescription());
	}

	@Test
	public void testWeeJasDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.WEE_JAS.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.WEE_JAS.getAttitude());
	}

	// Vecna

	@Test
	public void testVecnaName() {
		assertEquals("Vecna", Deity.VECNA.getName());
	}

	@Test
	public void testVecnaDescription() {
		assertEquals("God of destructive and evil secrets.", Deity.VECNA.getDescription());
	}

	@Test
	public void testVecnaDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.VECNA.getAlignment());
		assertEquals(Attitude.GOOD, Deity.VECNA.getAttitude());
	}

	// Yondalla

	@Test
	public void testYondallaName() {
		assertEquals("Yondalla", Deity.YONDALLA.getName());
	}

	@Test
	public void testYondallaDescription() {
		assertEquals("Goddess of halflings (also a demi-human power).", Deity.YONDALLA.getDescription());
	}

	@Test
	public void testYondallaDisposition() {
		assertEquals(Alignment.RIGHTEOUS, Deity.YONDALLA.getAlignment());
		assertEquals(Attitude.GOOD, Deity.YONDALLA.getAttitude());
	}

	// NONE

	@Test
	public void testNoneDeityName() {
		assertEquals("None", Deity.NONE.getName());
	}

	@Test
	public void testNoneDeityDescription() {
		assertEquals("No god selected.", Deity.NONE.getDescription());
	}

	@Test
	public void testNoneDisposition() {
		assertEquals(Alignment.NEUTRAL, Deity.NONE.getAlignment());
		assertEquals(Attitude.NEUTRAL, Deity.NONE.getAttitude());
	}
}
