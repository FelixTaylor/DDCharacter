package character;

import org.junit.jupiter.api.Test;

import static character.components.Clazz.*;
import static character.components.Gender.*;
import static character.components.Race.*;

class DDControllerTest {

	@Test
	void factoryTest() {
		DDController controller = new DDController(1,"Cool Kid", HUMAN, RANGER, MALE).addEquipment();
		controller.characterSheetToConsole();
	}
}
