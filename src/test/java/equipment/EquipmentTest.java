package equipment;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p>The <code>EquipmentTest</code> class is used to test the Equipment class.
 *
 * @see Equipment
 * @see Armour
 * @see Capital
 * @see Weapon
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.1
 */
public class EquipmentTest {
    Equipment e = new Equipment();

    @Test
    public void testEquipmentConstructor() {
		assertEquals(Armour.NONE, e.getArmour());
		assertEquals(Weapon.NONE, e.getWeapon());
		assertEquals("Gold", e.getCapital().getCurrency());
		assertEquals(0.0, e.getCapital().getCapital());
    }
}
