/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import equipment.base.Damage;

/**
 * <p>
 * The test class for Damage
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.1
 */
public class DamageTest {

	@Test
	public void testDamageTypeSize() {
		assertEquals(9, Damage.values().length);
	}

	@Test
	public void testIfAllDamagesAreTested() {
		// Note: We have to decrease the value of the array length to get the
		// correct value because of this method and the testDamageEnumSize method.
		assertEquals(Damage.values().length * 2, DamageTest.this.getClass().getDeclaredMethods().length - 2);
	}

	@Test
	public void testAmountOneD2() {
		assertEquals(1, Damage.ONE_D2.getAmount());
	}

	@Test
	public void testRangeOneD2() {
		assertEquals(2, Damage.ONE_D2.getRange());
	}

	@Test
	public void testAmountOneD3() {
		assertEquals(1, Damage.ONE_D3.getAmount());
	}

	@Test
	public void testRangeOneD3() {
		assertEquals(3, Damage.ONE_D3.getRange());
	}

	@Test
	public void testAmountOneD4() {
		assertEquals(1, Damage.ONE_D4.getAmount());
	}

	@Test
	public void testRangeOneD4() {
		assertEquals(4, Damage.ONE_D4.getRange());
	}

	@Test
	public void testAmountOneD6() {
		assertEquals(1, Damage.ONE_D6.getAmount());
	}

	@Test
	public void testRangeOneD6() {
		assertEquals(6, Damage.ONE_D6.getRange());
	}

	@Test
	public void testAmountOneD8() {
		assertEquals(1, Damage.ONE_D8.getAmount());
	}

	@Test
	public void testRangeOneD8() {
		assertEquals(8, Damage.ONE_D8.getRange());
	}

	@Test
	public void testAmountOneD10() {
		assertEquals(1, Damage.ONE_D10.getAmount());
	}

	@Test
	public void testRangeOneD10() {
		assertEquals(10, Damage.ONE_D10.getRange());
	}

	@Test
	public void testAmountOneD12() {
		assertEquals(1, Damage.ONE_D12.getAmount());
	}

	@Test
	public void testRangeOneD12() {
		assertEquals(12, Damage.ONE_D12.getRange());
	}

	@Test
	public void testAmountTwoD4() {
		assertEquals(2, Damage.TWO_D4.getAmount());
	}

	@Test
	public void testRangeTwoD4() {
		assertEquals(4, Damage.TWO_D4.getRange());
	}

	@Test
	public void testAmountTwoD6() {
		assertEquals(2, Damage.TWO_D6.getAmount());
	}

	@Test
	public void testRangeTwoD6() {
		assertEquals(6, Damage.TWO_D6.getRange());
	}
}
