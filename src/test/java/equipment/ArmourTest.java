package equipment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * <p>
 * {@link Armour} test class.
 * 
 * @see Armour
 * 
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.0.5
 */
public class ArmourTest {

	@Test
	public void testIfAllArmoursAreTested() {
		// Note: We have to decrease the value of the array length to get the correct value
		assertEquals(Armour.values().length, ArmourTest.this.getClass().getDeclaredMethods().length - 1);
	}

	@Test
	public void testNoArmour() {
		Armour armourUnderTest = Armour.NONE;
		assertEquals("NONE", armourUnderTest.getName());
		assertEquals(0, armourUnderTest.getValue());
		assertEquals(99, armourUnderTest.getMaxDexModifier());
		assertEquals(0, armourUnderTest.getArmourCheckPenalty());
		assertEquals("NONE", armourUnderTest.getType());
	}

	@Test
	public void testTunic() {
		Armour armourUnderTest = Armour.TUNIC;
		assertEquals("Tunic", armourUnderTest.getName());
		assertEquals(1, armourUnderTest.getValue());
		assertEquals(8, armourUnderTest.getMaxDexModifier());
		assertEquals(0, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Light Armour", armourUnderTest.getType());
	}

	@Test
	public void testPadded() {
		Armour armourUnderTest = Armour.PADDED;
		assertEquals("Padded", armourUnderTest.getName());
		assertEquals(1, armourUnderTest.getValue());
		assertEquals(8, armourUnderTest.getMaxDexModifier());
		assertEquals(0, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Light Armour", armourUnderTest.getType());
	}

	@Test
	public void testLeather() {
		Armour armourUnderTest = Armour.LEATHER;
		assertEquals("Leather", armourUnderTest.getName());
		assertEquals(2, armourUnderTest.getValue());
		assertEquals(6, armourUnderTest.getMaxDexModifier());
		assertEquals(0, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Light Armour", armourUnderTest.getType());
	}

	@Test
	public void testHardenedLeather() {
		Armour armourUnderTest = Armour.LEATHER_HARDENED;
		assertEquals("Hardened Leather", armourUnderTest.getName());
		assertEquals(3, armourUnderTest.getValue());
		assertEquals(5, armourUnderTest.getMaxDexModifier());
		assertEquals(-1, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Light Armour", armourUnderTest.getType());
	}

	@Test
	public void testChainMail() {
		Armour armourUnderTest = Armour.CHAIN_MAIL;
		assertEquals("Chain Mail", armourUnderTest.getName());
		assertEquals(4, armourUnderTest.getValue());
		assertEquals(4, armourUnderTest.getMaxDexModifier());
		assertEquals(-2, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Medium Armour", armourUnderTest.getType());
	}

	@Test
	public void testScaleMail() {
		Armour armourUnderTest = Armour.SCALE_MAIL;
		assertEquals("Scale Mail", armourUnderTest.getName());
		assertEquals(4, armourUnderTest.getValue());
		assertEquals(3, armourUnderTest.getMaxDexModifier());
		assertEquals(-4, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Medium Armour", armourUnderTest.getType());
	}

	@Test
	public void testPlateArmour() {
		Armour armourUnderTest = Armour.PLATE_ARMOUR;
		assertEquals("Plate Armour", armourUnderTest.getName());
		assertEquals(7, armourUnderTest.getValue());
		assertEquals(0, armourUnderTest.getMaxDexModifier());
		assertEquals(-7, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Heavy Armour", armourUnderTest.getType());
	}

	@Test
	public void testKnightsArmour() {
		Armour armourUnderTest = Armour.KNIGHTS_ARMOUR;
		assertEquals("Knight's Armour", armourUnderTest.getName());
		assertEquals(8, armourUnderTest.getValue());
		assertEquals(1, armourUnderTest.getMaxDexModifier());
		assertEquals(-6, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Heavy Armour", armourUnderTest.getType());
	}

	@Test
	public void testBreastPlate() {
		Armour armourUnderTest = Armour.BREAST_PLATE;
		assertEquals("Breast Plate", armourUnderTest.getName());
		assertEquals(5, armourUnderTest.getValue());
		assertEquals(3, armourUnderTest.getMaxDexModifier());
		assertEquals(-4, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Medium Armour", armourUnderTest.getType());
	}

	@Test
	public void testScaleArmour() {
		Armour armourUnderTest = Armour.SCALE_ARMOUR;
		assertEquals("Scale Armour", armourUnderTest.getName());
		assertEquals(4, armourUnderTest.getValue());
		assertEquals(3, armourUnderTest.getMaxDexModifier());
		assertEquals(-4, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Medium Armour", armourUnderTest.getType());
	}

	@Test
	public void testHideArmour() {
		Armour armourUnderTest = Armour.HIDE_ARMOUR;
		assertEquals("Hide Armour", armourUnderTest.getName());
		assertEquals(3, armourUnderTest.getValue());
		assertEquals(4, armourUnderTest.getMaxDexModifier());
		assertEquals(-3, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Light Armour", armourUnderTest.getType());
	}

	@Test
	public void testSplintMail() {
		Armour armourUnderTest = Armour.SPLINT_MAIL;
		assertEquals("Splint Mail", armourUnderTest.getName());
		assertEquals(6, armourUnderTest.getValue());
		assertEquals(0, armourUnderTest.getMaxDexModifier());
		assertEquals(-7, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Heavy Armour", armourUnderTest.getType());
	}

	@Test
	public void testBandedMail() {
		Armour armourUnderTest = Armour.BANDED_MAIL;
		assertEquals("Banded Mail", armourUnderTest.getName());
		assertEquals(6, armourUnderTest.getValue());
		assertEquals(1, armourUnderTest.getMaxDexModifier());
		assertEquals(-6, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Heavy Armour", armourUnderTest.getType());
	}

	@Test
	public void testHalfPlate() {
		Armour armourUnderTest = Armour.HALF_PLATE;
		assertEquals("Half Plate", armourUnderTest.getName());
		assertEquals(7, armourUnderTest.getValue());
		assertEquals(0, armourUnderTest.getMaxDexModifier());
		assertEquals(-7, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Heavy Armour", armourUnderTest.getType());
	}

	@Test
	public void testFullPlate() {
		Armour armourUnderTest = Armour.FULL_PLATE;
		assertEquals("Full Plate", armourUnderTest.getName());
		assertEquals(8, armourUnderTest.getValue());
		assertEquals(1, armourUnderTest.getMaxDexModifier());
		assertEquals(-6, armourUnderTest.getArmourCheckPenalty());
		assertEquals("Heavy Armour", armourUnderTest.getType());
	}
}
