/**
 * <p>
 * The <code>exception</code> package contains various exceptions customized for
 * the <code>DDCharacter</code> library. The
 * {@link exceptions.InvalidInputException InvalidInputException} is thrown if
 * an given input is considered invalid.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.0.0
 */
package exceptions;
