/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package exceptions;

/**
 * <p>
 * The <code>InvalidInputException</code> can be thrown by methods if the given
 * input(s) are invalid.
 *
 * @author FelixTaylor
 * @since 0.1.0
 * @version 1.0.0
 *
 * @since 0.1.0
 */
public class InvalidInputException extends Exception {
	private static final long serialVersionUID = 9011384142227347574L;

	public InvalidInputException(String message) {
		super(message);
	}
}
