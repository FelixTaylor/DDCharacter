/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package interfaces;

import character.DDCharacter;

/**
 * <p>
 * The <code>Exportable</code> interface provides functionalities to export the
 * {@link DDCharacter} to various different file types like CSV or PDF. The date
 * can then be used to maybe generate printable PDF character sheets or to
 * import the character on another system.
 *
 * <p>
 * CSV file styling:
 *
 * <pre>
 * type,UUID,creationTime,Name,Race,Clazz,Gender,Level,STR,DEX,CON,INT,WIS,CHA,Armour,Weapon,Capital,Currency,Deity,Alignment,Attitude,MaxHitpoints,Lore
 * NPC,c5b677b7-16fd-4d0e-bcf5-586fa87e10fb,1553792973595,Norron Bodiak,HUMAN,MAGE,MALE,1,12,13,16,18,13,12,LEATHER,THROWING_SPEER_S,67.0,Gold,VECNA,RIGHTEOUS,NEUTRAL,7,This character is awesome.
 * </pre>
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.3
 */
public interface Exportable {

	/**
	 * <p>
	 * The <code>toCSV()</code> method returns a CSV styled string representation of
	 * the current handled character.
	 *
	 * @return String : returns a CSV styled string representation of the current
	 *         character
	 *
	 * @since 0.1.0
	 */
	String toCSV();

	/**
	 * <p>The <code>toJSON</code> method returns a JSON styles string representation
	 * of the current character.
	 *
	 * @return String : JSON representation of the current character
	 *
	 * @since 0.1.0
	 */
	String toJSON();
}
