/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character;

import appearance.Appearance;
import character.base.Abilities;
import character.base.Disposition;
import character.base.Disposition.Alignment;
import character.base.Disposition.Attitude;
import character.base.Names;
import character.base.Skills;
import character.components.*;
import equipment.Armour;
import equipment.Capital;
import equipment.Equipment;
import equipment.Weapon;
import equipment.base.Category;
import org.checkerframework.checker.nullness.qual.NonNull;
import utils.Sheet;

import java.util.Objects;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Arrays.stream;

/**
 * The DDController class is able to generate valid {@link DDCharacter}.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.2.1
 */
public class DDController {
	private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final Random random = new Random();


	private final DDCharacter character;
	private final Appearance appearance;
	private final Equipment equipment;

	static {
		logger.setLevel(Level.INFO);
	}

	public DDController(int level, Object... objects) {
		this.character = new DDCharacter(level);
		this.appearance = new Appearance();
		this.equipment = new Equipment();
		factory(objects);

		final Race race = getCharacter().getRace();
		getAppearance().randomSize(race);
		getAppearance().randomWeight(race);
	}

	/**
	 * <p>
	 * The <code>factory(int, Object...)</code> method creates and a new
	 * calculated character with the given parameter as attributes.
	 * Following attributes will always be randomized:
	 * - skills,
	 * - size,
	 * - weight and
	 * - capital
	 *
	 * @param objects can be various character attributes like race, clazz,
	 *                gender or deity
	 * 
	 * @see DDCharacter#set(Object...)
	 * @see DDCharacter
	 * @see Equipment
	 */
	private void factory(Object... objects) {
		setToCharacterOrEquipment(objects);

		if (getCharacter().getRace() == null) {
			randomRace();
		}

		if (getCharacter().getClazz() == null) {
			randomClazz();
		}

		if (shouldRandomizeAbilities()) {
			randomAbilities();
		}

		if (getCharacter().getName().equals("DDCharacter")
				|| getCharacter().getName().isEmpty()) {
			randomGender();
			randomName();
		}

		if (getCharacter().getDeity() != Deity.NONE) {
			randomDisposition();
			
		} else if(getCharacter().getDeity()       == Deity.NONE
				&& (getCharacter().getAlignment() == Alignment.NONE
				|| getCharacter().getAttitude()   == Attitude.NONE)) {
			
			randomDeity();
			randomDisposition();
		}
		
		randomSkills();
		getCharacter().addSkillsAndDistributePoints();
		getCharacter().calculateMaxHitPoints();
		setHitPoints(getCharacter().getMaxHitPoints());

		setRandomAppearance();
	}

	public void setRandomAppearance() {
		getAppearance().setAppearance(getCharacter().getRace());
	}

	public Appearance getAppearance() {
		return this.appearance;
	}

	public void setAppearance(double size, double weight) {
		getAppearance().setAppearance(size, weight);
	}

	/**
	 * <p>The <code>isValid()</code> method returns true if the handled character
	 * is valid. This method originated from {@link DDCharacter#isValid()}.
	 * This method will print a warning if the size of the equipped weapon
	 * differs from the size of the current character.
	 *
	 * @return boolean : True if the character is valid
	 */
	public boolean isValid() {
		final Category weaponCategory = getEquipment().getWeapon().getCategory();
		final Size characterSize = getCharacter().getRace().getSize();
		if (!weaponCategory.compareTo(characterSize)) {
			logger.info("The size category of the equipped weapon("
					+ weaponCategory + ") is different from the size of the character("
					+ characterSize +")");
		}

		return getCharacter().isValid();
	}

	/**
	 * <p>The <code>isKnockedOut()</code> method returns true if the character
	 * is knocked out and therefor is unable to interact, otherwise the returned
	 * value is false.
	 *
	 * @return boolean : True if the character is unable to interact
	 *
	 * @see DDCharacter#isKnockedOut()
	 */
	public boolean isKnockedOut() {
		return getCharacter().isKnockedOut();
	}

	/**
	 * <p>The <code>isAlive()</code> method returns true if the character is
	 * alive, false if the character was killed.
	 *
	 * @return boolean : True if the character is alive
	 *
	 * @see DDCharacter#isAlive()
	 */
	public boolean isAlive() {
		return getCharacter().isAlive();
	}

	/**
	 * <p>The <code>addEquipment()</code> randomizes the equipment of the character.
	 * This method will override any existing armour, weapon and capital settings.
	 *
	 * @return DDController : This controller
	 *
	 * @see Equipment
	 * @see Armour
	 * @see Capital
	 * @see Weapon
	 */
	public DDController addEquipment() {
		randomArmour();
		randomWeapon();
		randomCapital();
		return this;
	}

	/**
	 * <p>The <code>getEquipment()</code> method returns the equipment object
	 * associated with this character.
	 *
	 * @return Equipment : The equipment associated with this character
	 *
	 * @see Equipment
	 * @see DDCharacter
	 */
	public Equipment getEquipment() {
		return this.equipment;
	}

	/**
	 * <p>The <code>setArmour(Armour)</code> method sets a new {@link Armour} for
	 * the {@link DDCharacter}.
	 *
	 * @param armour the new armour for the character
	 *
	 * @see Equipment
	 * @see Armour
	 */
	public void setArmour(@NonNull final Armour armour) {
		getEquipment().set(armour);
	}

	/**
	 * <p>The <code>setWeapon(Weapon)</code> method sets a new {@link Weapon}
	 * for the {@link DDCharacter}.
	 *
	 * @param weapon the new weapon for the character
	 *
	 * @see Equipment
	 * @see Weapon
	 */
	public void setWeapon(@NonNull final Weapon weapon) {
		getEquipment().set(weapon);
	}

	/**
	 * <p>The <code>getName()</code> returns the name of the current character.
	 * @return String : name of the current character
	 */
	public String getName() {
		return getCharacter().getName();
	}

	/**
	 * <p>The <code>getArmour()</code> method returns the {@link Armour} of the
	 * current {@link DDCharacter}.
	 *
	 * @return the current armour of the character
	 *
	 * @see Armour
	 * @see Equipment
	 */
	private Armour getArmour() {
		return getEquipment().getArmour();
	}

	public Weapon getWeapon() {
		return getEquipment().getWeapon();
	}

	public void setCapital(double capital) {
		getEquipment().setCapital(capital);
	}

	public void addExperience(int exp) {
		getCharacter().setExperience(exp);
		getCharacter().levelUp();
	}

	public DDCharacter getCharacter() {
		return this.character;
	}

	/**
	 * <p>The <code>setHitPoints()</code> method sets the current hitpoints of
	 * this character. Note that the new hitpoint value can not be lower then
	 * -10 and also not greater then the {@link DDCharacter#getMaxHitPoints()}
	 * value.
	 * 
	 * @param hitPoints : The new hit points value for this character
	 *                  
	 * @see DDCharacter#setHitPoints(int)
	 */
	public void setHitPoints(int hitPoints) {
		getCharacter().setHitPoints(hitPoints);
	}

	/**
	 * <p>The <code>shouldRandomizeAbilities()</code> method randomizes all
	 * {@link Abilities} if at least two of them are equal or below three.
	 *
	 * @return boolean : true if abilities should be randomized
	 */
	private boolean shouldRandomizeAbilities() {
		return stream(getCharacter().getAbilities().asArray())
				.filter(a -> a.getValue() <= 3)
				.count() > 1;
	}

	private void setToCharacterOrEquipment(Object... objects) {
		stream(objects).filter(Objects::nonNull).forEach(this::set);
	}

	private void set(Object obj) {
		if (obj instanceof Weapon || obj instanceof Armour) {
			getEquipment().set(obj);
		} else {
			getCharacter().set(obj);
		}
	}

	/**
	 * <p>
	 * The <code>randomAbilities()</code> method generates a random set of
	 * abilities. The set of abilities fits the needs of the selected {@link Clazz}.
	 * Also the additional ability points gained at level-up are added.
	 * 
	 * @see Clazz
	 * @see DDLevel
	 */
	private void randomAbilities() {
		final DDCharacter character = getCharacter();
		final Race race = character.getRace();
		final Clazz clazz = character.getClazz();
		final int level = character.getLevel();
		character.getAbilities().random(race, clazz, level);
	}

	/**
	 * <p>
	 * The <code>randomRace()</code> method selects a random {@link Race} for the
	 * character.
	 * 
	 * @see Race
	 * @see DDCharacter#set(Object...)
	 * @see DDCharacter#getRace()
	 */
	private void randomRace() {
		final Race[] races = Race.values();
		getCharacter().set(races[random.nextInt(races.length)]);
	}

	/**
	 * <p>
	 * The <code>randomSkills()</code> method generates a new set of {@link Skills}
	 * and distributes the available {@link DDCharacter#getSkillPoints()}} to a
	 * random selection. This method will only work, if the character is valid.
	 * Therefore the {@link DDCharacter#isValid()} method is called internally.
	 *
	 * @see Skill
	 * @see Skills
	 * @see DDCharacter#getSkills()
	 * @see DDCharacter#getSkillPoints()
	 */
	private void randomSkills() {
		final DDCharacter character = getCharacter();
		final Race race = character.getRace();
		final Clazz clazz = character.getClazz();
		final Abilities abilities = character.getAbilities();
		final int level = character.getLevel();
		final int rank = character.getMaxSkillRank();

		character.getSkillsObject().random(race, clazz, abilities, level, rank);
	}

	/**
	 * <p>
	 * The <code>randomWeapon()</code> method selects a random weapon for the
	 * current character. The weapon selected will fit the {@link Size} of the
	 * character.
	 *
	 * @see Equipment#getWeapon()
	 * @see Weapon
	 * @see Category
	 * @see DDCharacter
	 */
	private void randomWeapon() {
		final Size size = getCharacter().getRace().getSize();
		final Object[] weapons = stream(Weapon.values())
				.filter(w -> w.getCategory().compareTo(size))
				.toArray();

		getEquipment().set(weapons[random.nextInt(weapons.length)]);
	}

	/**
	 * <p>
	 * The <code>randomClazz()</code> method selects a random {@link Clazz} for the
	 * character.
	 * 
	 * @see Clazz
	 * @see DDCharacter#set(Object...)
	 * @see DDCharacter#getClazz()
	 */
	private void randomClazz() {
		final Clazz[] clazzes = Clazz.values();
		getCharacter().set(clazzes[random.nextInt(clazzes.length)]);
	}

	/**
	 * <p>
	 * The <code>randomName()</code> method selects a random name of the
	 * {@link Names} class for the character.
	 * 
	 * @see Names
	 * @see DDCharacter#setName(String)
	 * @see DDCharacter#getName()
	 */
	private void randomName() {
		final Race race = character.getRace();
		final Gender gender = character.getGender();
		getCharacter().setName(Names.random(race, gender));
	}

	/**
	 * <p>
	 * The <code>randomGender()</code> method selects a random {@link Gender} for
	 * the character.
	 * 
	 * @see Gender
	 * @see DDCharacter#set(Object...)
	 * @see DDCharacter#getGender()
	 */
	private void randomGender() {
		final Gender[] genders = Gender.values();
		getCharacter().set(genders[random.nextInt(genders.length)]);
	}

	/**
	 * <p>
	 * The <code>randomDisposition()</code> method selects a random disposition for
	 * the character and checks if this is a valid combination of {@link Alignment}
	 * and {@link Attitude}. See the {@link Disposition} documentation for more
	 * information.
	 * 
	 * @see Deity
	 * @see Disposition
	 * @see DDCharacter#setDisposition(Alignment, Attitude)
	 * @see DDCharacter#getAlignment()
	 * @see DDCharacter#getAttitude()
	 */
	private void randomDisposition() {
		final Clazz clazz = getCharacter().getClazz();
		final Deity deity = getCharacter().getDeity();
		Alignment alignment;
		Attitude attitude;

		do {
			alignment = Disposition.randomAlignment(clazz);
			attitude = Disposition.randomAttitude(clazz);
		} while (!Disposition.isValid(clazz, deity, alignment, attitude));

		getCharacter().setDisposition(alignment, attitude);
	}

	/**
	 * <p>
	 * The <code>randomDeity()</code> method selects a random {@link Deity} for the
	 * character.
	 * 
	 * @see Deity
	 * @see DDCharacter#set(Object...)
	 * @see DDCharacter#getDeity()
	 */
	private void randomDeity() {
		final Deity[] deities = Deity.values();
		getCharacter().set(deities[random.nextInt(deities.length)]);
	}

	/**
	 * <p>
	 * The <code>randomArmour()</code> method selects a random {@link Armour} for the
	 * character.
	 * 
	 * @see Armour
	 * @see DDCharacter#set(Object...)
	 * @see Equipment#getArmour()
	 */
	private void randomArmour() {
		final Armour[] armours = Armour.values();
		getEquipment().set(armours[random.nextInt(armours.length)]);
	}

	/**
	 * <p>
	 * The <code>randomCapital()</code> method generates a random amount for the
	 * characters capital. See the {@link Capital#calculate(Clazz)} method for more
	 * information.
	 * 
	 * @see Capital
	 * @see Equipment#getCapital()
	 * @see Equipment#set(Object...)
	 */
	private void randomCapital() {
		final Clazz clazz = getCharacter().getClazz();
		getEquipment().getCapital().calculate(clazz);
	}

	public int getHitPoints() {
		return getCharacter().getHitPoints();
	}

	public String getCharacterInfo() {
		return getCharacter().toString();
	}

	public void characterSimpleSheetToConsole() {
		System.out.println(Sheet.getSimpleSheet(getCharacter(), getEquipment()));
	}
	public void characterSheetToConsole() {
		System.out.println(Sheet.print(getCharacter(), getAppearance(), getEquipment()));
	}
}
