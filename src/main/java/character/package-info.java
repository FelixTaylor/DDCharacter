/**
 * <p>
 * The character package contains the core object of this library. These classes
 * can be used to generate valid 'Dungeon & Dragons' characters. At the current
 * sate of the library [v.0.1.0] you can use the DDController class.
 * <p>
 * <b>Note #1:</b> Not all mechanics are exactly as the official 'Player's
 * Handbook' states for different reasons. Some I was not yet able to implement,
 * others I wanted to change/adjust. So keep an eye on the documentation
 * provided by all the packages, objects, methods and variables.
 * <p>
 * <b>Note #2:</b> Use the controller object provided by this package to
 * generate characters. Using classes from the sub packages can cause unexpected
 * errors which could be hard to debug.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.1.1
 *
 * @see character.DDCharacter
 * @see character.DDController
 *
 * @see character.base.Abilities
 * @see character.base.Disposition
 * @see character.base.Names
 * @see character.base.SavingThrows
 * @see character.base.Skills
 *
 * @see character.components.Ability
 * @see character.components.Clazz
 * @see character.components.DDLevel
 * @see character.components.Deity
 * @see character.components.Entity
 * @see character.components.Gender
 * @see character.components.Race
 * @see character.components.Size
 * @see character.components.Skill
 * @see character.components.Talent
 * @see character.components.Weight
 */
package character;