/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import character.components.Clazz;
import character.components.Deity;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Random;
import java.util.logging.Logger;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;

/**
 * <p>
 * The <code>Dispositions</code> class is used to select the alignment and
 * attitude of a figure. You can select a disposition as follow:
 *
 * <pre>
 * character.setDisposition(Disposition.ALIGNMENT.RIGHTEOUS, Disposition.Attitude.NEUTRAL);
 * </pre>
 *
 * Possible values are:
 * <ol>
 * <li>Alignment
 * <ul>
 * <li><code>Righteous</code></li>
 * <li><code>Chaotic</code></li>
 * <li><code>Neutral</code></li>
 * </ul>
 * <li>Attitude
 * <ul>
 * <li><code>Good</code></li>
 * <li><code>Evil</code></li>
 * <li><code>Neutral</code></li>
 * </ol>
 * <p>
 * This class does not have a logger, because it has no failing condition which
 * could be logged.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.3.7
 */
public class Disposition {
	private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * <p>
	 * All alignments selectable by a character:
	 * <ul>
	 * <li>Righteous
	 * <li>Neutral
	 * <li>Chaotic
	 * </ul>
	 *
	 * @see #isValid(Clazz, Deity, Alignment, Attitude)
	 *
	 * @since 0.1.0
	 */
	public enum Alignment {
		NONE(-1, "None"), RIGHTEOUS(0, "Righteous"), NEUTRAL(1, "Neutral"), CHAOTIC(2, "Chaotic");

		private final int id;
		private final String name;

		Alignment(final int id, final String name) {
			this.id = id;
			this.name = name;
		}

		public String getName() {
			return this.name;
		}

		public int getId() {
			return this.id;
		}
	}

	/**
	 * <p>
	 * All attitudes selectable by a character:
	 * <ul>
	 * <li>Good
	 * <li>Neutral
	 * <lI>Evil
	 * </ul>
	 *
	 * @see #isValid(Clazz, Deity, Alignment, Attitude)
	 *
	 * @since 0.1.0
	 */
	public enum Attitude {
		NONE(-1, "None"), GOOD(0, "Good"), NEUTRAL(1, "Neutral"), EVIL(2, "Evil");

		private final int id;
		private final String name;

		Attitude(final int id, final String name) {
			this.id = id;
			this.name = name;
		}

		public String getName() {
			return this.name;
		}

		public int getId() {
			return this.id;
		}
	}

	/**
	 * <p>
	 * The <code>isValid(Clazz, Alignment, Attitude)</code> validates the selected
	 * alignment and attitude for the character.
	 * 
	 * @param clazz     The clazz of the character
	 * @param alignment The alignment of the character
	 * @param attitude  The attitude of the character
	 * @return true if a valid combination of alignment and attitude was selected,
	 *         false if not.
	 * 
	 * @see Clazz
	 * @see Alignment
	 * @see Attitude
	 * @see #randomAlignment(Clazz)
	 * @see #randomAttitude(Clazz)
	 *
	 * @since 0.1.0
	 */
	public static boolean isValid(@NonNull final Clazz clazz, @NonNull final Deity deity,
								  @NonNull final Alignment alignment, @NonNull final Attitude attitude) {
		requireNonNull(clazz, "Clazz may not be null");
		requireNonNull(alignment, "Alignment may not be null");
		requireNonNull(attitude, "Attitude may not be null");

		switch (clazz) {
		case BARBARIAN:
		case BARD:
			return !alignment.equals(Alignment.RIGHTEOUS);

		case DRUID:
			return alignment.equals(Alignment.NEUTRAL) || attitude.equals(Attitude.NEUTRAL);

		case MONK:
			return alignment.equals(Alignment.RIGHTEOUS);

		case PALADIN:
			return alignment.equals(Alignment.RIGHTEOUS) && attitude.equals(Attitude.NEUTRAL);

		case CLERIC: {
			return deity.equals(Deity.NONE) || isValidClericDisposition(deity, alignment, attitude);
		}

		default:
			return true;
		}
	}

	private static boolean isValidClericDisposition(@NonNull final Deity deity, @NonNull final Alignment alignment,
													@NonNull final Attitude attitude) {

		requireNonNull(deity, "Deity may not be null");
		requireNonNull(alignment, "Alignment may not be null");
		requireNonNull(attitude, "Attitude may not be null");

		final int deityAlignmentPos = deity.getAlignment().getId();
		final int deityAttitudePos = deity.getAttitude().getId();
		final int alignmentPos = attitude.getId();
		final int attitudePos = alignment.getId();

		final boolean validAlignment =
				   deityAlignmentPos     == 1
			    || deityAlignmentPos     == alignmentPos
				|| deityAlignmentPos - 1 == alignmentPos
				|| deityAlignmentPos + 1 == alignmentPos;

		final boolean validAttitude =
				   deityAlignmentPos    == 1
			    || deityAttitudePos     == attitudePos
				|| deityAttitudePos - 1 == attitudePos
				|| deityAttitudePos + 1 == attitudePos;

		if (!validAlignment) {
			logger.info("The selected character alignment (" + alignment
					+ ") is invalid in combination with the deities alignment "
					+ deity.getAlignment() + ".");
		}

		if (!validAttitude) {
			logger.warning("The selected character attitude (" + attitude
					+ ") is invalid in combination with the deities attitude "
					+ deity.getAttitude() + ".");
		}

		return validAlignment && validAttitude;
	}

	/**
	 * <p>
	 * The <code>randomAlignment(Clazz)</code> method selects a random
	 * <code>Alignment</code> for the given {@link Clazz}.
	 *
	 * @param clazz The clazz of the character
	 * 
	 * @return random alignment
	 * 
	 * @see Clazz
	 * @see Alignment
	 *
	 * @since 0.1.0
	 */
	public static Alignment randomAlignment(@NonNull final Clazz clazz) {
		requireNonNull(clazz, "Clazz may not be null");
		final Random random = new Random();
		switch (clazz) {
		case BARBARIAN:
		case BARD: {
			return (Alignment) stream(Alignment.values())
					.filter(a -> !a.equals(Alignment.RIGHTEOUS))
					.toArray()[random.nextInt(Alignment.values().length)];
		}

		case PALADIN:
		case MONK: {
			return Alignment.RIGHTEOUS;
		}

		default:
			return Alignment.values()[random.nextInt(3)];

		}
	}

	/**
	 * <p>
	 * Selects a new random <code>Attitude</code> for the given character
	 * <code>Clazz</code>.
	 * </p>
	 *
	 * @see Clazz
	 * @see #isValid(Clazz, Deity, Alignment, Attitude)
	 * @see Attitude
	 *
	 * @param clazz The clazz of the character class
	 * 
	 * @return random attitude
	 *
	 * @since 0.1.0
	 */
	public static Attitude randomAttitude(@NonNull final Clazz clazz) {
		requireNonNull(clazz, "Clazz may not be null");
		final int pos = new Random().nextInt(3);
		return clazz.equals(Clazz.PALADIN) ? Attitude.NEUTRAL : Attitude.values()[pos];
	}
}
