/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import character.DDCharacter;
import character.components.Clazz;
import character.components.DDLevel;
import character.components.Race;
import character.components.Skill;
import equipment.Armour;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.*;

/**
 * <p>
 * The <code>Skills</code> class contains all skills usable by the character and
 * keeps track of how many skill points can be used to increase the ranks of
 * skills. This class also provides a public string representation of each skill
 * for further use.
 * 
 * @see Abilities
 * @see Clazz
 * @see DDCharacter
 * @see DDLevel
 * @see Race
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.8
 */
public class Skills {

	/**
	 * <p>
	 * The <code>skillPoints</code> are calculated depending on the {@link Clazz}
	 * and {@link DDLevel} of the character. The skillPoints can be used to level or
	 * upgrade a skill. Note that increasing the rank of a {@link Skill} can cost
	 * one or two points depending on the skill and its <code>isDefaultSkill</code>.
	 * If the skill is flagged as default the skill costs two points to rank up.
	 *
	 * @see #calculateSkillPoints(Race, Clazz, Abilities, int)
	 * @see #getUsedSkillPoints()
	 * @see #getSkillPoints()
	 * @see #getSkills()
	 * @see Skills
	 * @see Skill
	 */
	private int skillPoints;

	/**
	 * <p>
	 * The <code>usedSkillPoints</code> value represents how many points were used
	 * by the character to improve his skills. Note that increasing the rank of a
	 * {@link Skill} can cost one or two points depending on the skill and its
	 * <code>isDefaultSkill</code>. If the skill is flagged as default the skill
	 * costs two points to rank up.
	 * 
	 * @see #getSkillPoints()
	 * @see #getUsedSkillPoints()
	 * @see Skills
	 * @see Skill
	 */
	private int usedSkillPoints;

	/**
	 * <p>
	 * The <code>skills</code> HashMap stores all available skill of the character.
	 * This is a fixed list for each {@link Clazz}, therefore it is not possible to
	 * add or remove skills.
	 * 
	 * @see Clazz
	 * @see Skill
	 * @see #setSkills(Clazz, Abilities)
	 * @see #getSkills()
	 */
	private HashMap<String, Skill> skills;

	// Following is a list of every skill. Every skill is accessible form outside
	// this class.
	public static final String APPRAISE = "Appraise";
	public static final String BALANCE = "Balance";
	public static final String BLUFF = "Bluff";
	public static final String CLIMB = "Climb";
	public static final String CONCENTRATION = "Concentration";
	public static final String CRAFT = "Craft";
	public static final String DECIPHER_SCRIPT = "Decipher Script";
	public static final String DIPLOMACY = "Diplomacy";
	public static final String DISABLE_DEVICE = "Disable Device";
	public static final String DISGUISE = "Disguise";
	public static final String ESCAPE_ARTIST = "Escape Artist";
	public static final String FORGERY = "Forgery";
	public static final String GATHER_INFORMATION = "Gather Information";
	public static final String HANDLE_ANIMAL = "Handle Animal";
	public static final String HEAL = "Heal";
	public static final String HIDE = "Hide";
	public static final String INTIMIDATE = "Intimidate";
	public static final String JUMP = "Jump";
	public static final String KNOWLEDGE_ARCANE = "Knowledge Arcane";
	public static final String KNOWLEDGE_HISTORY = "Knowledge History";
	public static final String KNOWLEDGE_PLAIN = "Knowledge Plain";
	public static final String KNOWLEDGE_RELIGION = "Knowledge Religion";
	public static final String KNOWLEDGE_NATURE = "Knowledge Nature";
	public static final String KNOWLEDGE_NOBILITY_ROYAL = "Knowledge Nobility and Royal Dynasty";
	public static final String KNOWLEDGE_LOCAL = "Knowledge Local";
	public static final String KNOWLEDGE_ARCHITECTURE = "Knowledge Architecture";
	public static final String KNOWLEDGE_DUNGEONS = "Knowledge Dungeons";
	public static final String KNOWLEDGE_GEOGRAPHY = "Knowledge Geography";
	public static final String LISTEN = "Listen";
	public static final String MOVE_SILENTLY = "Move Silently";
	public static final String OPEN_LOCK = "Open Lock";
	public static final String PERFORM = "Perform";
	public static final String PROFESSION = "Profession";
	public static final String RIDE = "Ride";
	public static final String SEARCH = "Search";
	public static final String SENSE_MOTIVE = "Sense Motive";
	public static final String SLEIGHT_OF_HAND = "Sleight of Hand";
	public static final String SPELL_CRAFT = "Spell craft";
	public static final String SPOT = "Spot";
	public static final String SURVIVAL = "Survival";
	public static final String SWIM = "Swim";
	public static final String TUMBLE = "Tumble";
	public static final String USE_MAGIC_DEVICE = "Use Magic Device";
	public static final String USE_ROPE = "Use Rope";

	/**
	 * <p>
	 * The default <code>Skills</code> constructor is used to initialize a new
	 * skills object.
	 * 
	 * @see #skillPoints
	 * @see #usedSkillPoints
	 * @see #skills
	 * @see Skill
	 *
	 * @since 0.1.0
	 */
	public Skills() {
		this.skillPoints = 0;
		this.usedSkillPoints = 0;
		this.skills = new HashMap<>();
	}

	/**
	 * <p>
	 * The <code>setSkills(Clazz, Abilities)</code> method is used to set the usable
	 * skills for the character without using any of the skill points to rank up the
	 * skills. This includes the skills defined in the
	 * {@link #setDefaultSkills(Abilities)} and the corresponding
	 * <code>add[clazz name]Skills()</code> method.
	 * <p>
	 * To set the available skills if some points were already used, call the
	 * {@link #distributeAvailableSkillPoints(int)}} method.
	 * 
	 * @param clazz     The clazz of the character
	 * @param abilities The abilities of the character
	 * 
	 * @see Clazz
	 * @see Abilities
	 * @see #distributeAvailableSkillPoints(int)
	 *
	 * @since 0.1.0
	 */
	public void setSkills(@NonNull final Clazz clazz, @NonNull final Abilities abilities) {
		requireNonNull(clazz, "Clazz may not be null");
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> skills = setDefaultSkills(abilities);

		switch (clazz) {
		case BARBARIAN:
			skills.putAll(addBarbarianSkills(abilities));
			break;
		case BARD:
			skills.putAll(addBardSkills(abilities));
			break;
		case CLERIC:
			skills.putAll(addClericSkills(abilities));
			break;
		case DRUID:
			skills.putAll(addDruidSkills(abilities));
			break;
		case FIGHTER:
			skills.putAll(addFighterSkills(abilities));
			break;
		case MAGE:
			skills.putAll(addMageSkills(abilities));
			break;
		case MONK:
			skills.putAll(addMonkSkills(abilities));
			break;
		case PALADIN:
			skills.putAll(addPaladinSkills(abilities));
			break;
		case RANGER:
			skills.putAll(addRangerSkills(abilities));
			break;
		case ROGUE:
			skills.putAll(addRogueSkills(abilities));
			break;
		case WARLOCK:
			skills.putAll(addWarlockSkills(abilities));
			break;
		default:
			throw new InvalidParameterException("Invalid input: The clazz '" + clazz.getName()
					+ "' or the abilities are invalid, therefore no class specific skills were selected.");
		}

		// Finally sort the list of skills alphabetically.
		this.skills = skills.entrySet().stream().sorted(Comparator.comparing(e -> e.getValue().getName()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}

	/**
	 * <p>
	 * The <code>random(Race, Clazz, Abilities, DDLevel)</code> method is used to
	 * set the available skills and to increase the rank of the skills randomly as
	 * long as the character has {@link #skillPoints} left.
	 * 
	 * @param race      The race of the character
	 * @param clazz     The clazz of the character
	 * @param abilities The abilities of the character
	 * @param level     The level of the character
	 * 
	 * @see Abilities
	 * @see Clazz
	 * @see DDLevel
	 * @see Race
	 * @see Skill
	 * 
	 * @see #setSkills(Clazz, Abilities)
	 * @see #getSkills()
	 * @see #getSkillPoints()
	 * @see #calculateSkillPoints(Race, Clazz, Abilities, int)
	 * @see Skills#distributeAvailableSkillPoints(int)
	 *
	 * @since 0.1.0
	 */
	public void random(@NonNull final Race race, @NonNull final Clazz clazz, @NonNull final Abilities abilities,
					   final int level, final int maxSkillRank) {
		requireNonNull(race, "Race may not be null");
		requireNonNull(clazz, "Clazz may not be null");
		requireNonNull(abilities, "Abilities may not be null");

		setSkills(clazz, abilities);
		calculateSkillPoints(race, clazz, abilities, level);
		distributeAvailableSkillPoints(maxSkillRank);
	}

	/**
	 * <p>
	 * The <code>calculateSkillPoints(Race, Clazz, Abilities, DDLevel)</code> method
	 * calculates the available skill points by this character. Every {@link Clazz}
	 * has a 'skillpointsPerLevel' value which is then added to the intelligence
	 * modifier once every level. If the character is human he gets four additional
	 * skill points on the first level and one extra on every other level.
	 * <p>
	 * Calculation:
	 *
	 * <pre>
	 * {@literal //} for human characters
	 * skillpoints = (intelligenceMod + skillpointsPerLevel) * 4 <b>+ 1</b> {@literal //} +4 at level 1
	 * {@literal //} for every other character
	 * skillpoints = (intelligenceMod + skillpointsPerLevel) * 4;
	 * </pre>
	 * 
	 * @param race      The race of the character
	 * @param clazz     The clazz of the character
	 * @param abilities The abilities of the character
	 * @param level     The level of the character
	 * 
	 * @see Abilities
	 * @see Clazz
	 * @see DDLevel
	 * @see Race
	 * @see Skill
	 * 
	 * @see #setSkills(Clazz, Abilities)
	 * @see #getSkills()
	 * @see #getSkillPoints()
	 * @see #distributeAvailableSkillPoints(int)
	 *
	 * @since 0.1.0
	 */
	public void calculateSkillPoints(@NonNull final Race race, @NonNull final Clazz clazz,
									 @NonNull final Abilities abilities, final int level) {

		requireNonNull(race, "Race may not be null");
		requireNonNull(clazz, "Clazz may not be null");
		requireNonNull(abilities, "Abilities may not be null");

		final int skillPointsPerLevel;
		switch (clazz) {
		case BARBARIAN:
		case DRUID:
		case MONK:
			skillPointsPerLevel = 4;
			break;
		case BARD:
		case RANGER:
			skillPointsPerLevel = 6;
			break;
		case ROGUE:
			skillPointsPerLevel = 8;
			break;

		default:
			// Cleric, Fighter, Mage, Paladin, Warlock
			skillPointsPerLevel = 2;
			break;
		}

		// For each level the character will get the result of the skillPointsPerLevel +
		// the intelligence modifier calculation.
		final int combinedPointsPerLevel = skillPointsPerLevel + abilities.getModifier(Abilities.INT);

		// For the first level all characters are getting increase skill points. If the
		// character is human he/she gets additional four points.
		int points = (combinedPointsPerLevel * 4) + (combinedPointsPerLevel * (level-1));

		if (race.equals(Race.HUMAN)) {
			points += level + 4;
		}

		this.skillPoints = Math.max(0, points);
	}

	/**
	 * <p>
	 * The <code>getSkills()</code> method returns all available skills of this
	 * character as HashMap.
	 *
	 * @return HashMap Containing all available skills
	 * 
	 * @see Skill
	 * @see Skills
	 *
	 * @since 0.1.0
	 */
	public HashMap<String, Skill> getSkills() {
		return this.skills;
	}

	/**
	 * <p>
	 * The <code>getSkill(String)</code> method returns the {@link Skill} associated
	 * with the given name.
	 * 
	 * @param name The name of the returned skill
	 * 
	 * @return The skill with the given name
	 * 
	 * @see Skill
	 * @see Skills
	 *
	 * @since 0.1.0
	 */
	public Skill getSkill(final String name) {
		return getSkills().get(name);
	}

	private List<Skill> getFilteredSkills(final boolean defaultSkills, final int maxSkillRank) {
		return getSkills().values().stream()
				.filter(defaultSkills ? Skill::isDefaultSkill : s -> !s.isDefaultSkill())
				.filter(s -> s.getRank() < maxSkillRank)
				.collect(Collectors.toList());
	}

	/**
	 * <p>
	 * The <code>distributeAvailableSkillPoints(int)</code> method distributes the
	 * remaining skill points of the character to a randomized selection of skills
	 * usable by the character. Note that a rank can not be increased infinitely.
	 * 
	 * @param maxSkillRank The level of the character
	 *
	 * @see DDLevel
	 * @see Skill
	 * @see Skills
	 * @see DDLevel#getMaxSkillRank()
	 * @see #getSkillPoints()
	 * @see #getUsedSkillPoints()
	 * @see #getSkills()
	 *
	 * @since 0.1.0
	 */
	public void distributeAvailableSkillPoints(final int maxSkillRank) {
		final Random random = new Random();
		List<Skill> skillList;
		while (getUsedSkillPoints() + 2 <= getSkillPoints()) {
			boolean isDefaultSkill = random.nextInt(99) >= 69;
			skillList = getFilteredSkills(isDefaultSkill, maxSkillRank);
			skillList.get(random.nextInt(skillList.size())).addRank();
			this.usedSkillPoints += isDefaultSkill ? 2 : 1;
		}

		if (getSkillPoints() - getUsedSkillPoints() == 1) {
			skillList = getFilteredSkills(false, maxSkillRank);
			skillList.get(random.nextInt(skillList.size())).addRank();
			this.usedSkillPoints += 1;
		}
	}

	/**
	 * <p>
	 * The <code>getUsedSkillPoints()</code> method returns the amount of points the
	 * character has used to rank up his/her skills. Keep in mind that a 'default'
	 * character skill cost two skill points while the {@link Clazz} specific skills
	 * cost one to rank up.
	 * 
	 * @return The used skill points
	 * 
	 * @see #skillPoints
	 * @see #usedSkillPoints
	 * @see #calculateSkillPoints(Race, Clazz, Abilities, int)
	 * @see #getUsedSkillPoints()
	 *
	 * @since 0.1.0
	 */
	public int getUsedSkillPoints() {
		return this.usedSkillPoints;
	}

	/**
	 * <p>
	 * The <code>getSkillPoints()</code> method returns the total
	 * {@link #skillPoints} usable by this character.
	 * <p>
	 * <b>Note:</b> This value does not represent the accessible skill points. If
	 * the character {@link DDCharacter#isValid()} these skill points are being
	 * distributed. This value represents the total amount of usable skill point
	 * for a character at this level. To determine how many skill points can be
	 * used by the character it has to be compared to the value of
	 * {@link #getUsedSkillPoints()}.
	 * 
	 * @return The amount of skill points
	 * 
	 * @see Skills
	 * @see #skillPoints
	 * @see #calculateSkillPoints(Race, Clazz, Abilities, int)
	 * @see #getUsedSkillPoints()
	 *
	 * @since 0.1.0
	 */
	public int getSkillPoints() {
		return this.skillPoints;
	}

	/**
	 * <p>The <code>shouldApplyArmourPenalty(String)</code> method returns
	 * if the minus points from a potentially equipped {@link Armour}
	 * should be added to the skill power.
	 *
	 * @param skillName String : the name of the skill which is being checked for
	 *
	 * @see Armour
	 *
	 * @return boolean : true if the minus points of the armour should be added
	 * to the skill power value.
	 *
	 * @since 0.1.0
	 */
	public static boolean shouldApplyArmourPenalty(final String skillName) {
		final String[] skills = {
				BALANCE.toLowerCase(),          ESCAPE_ARTIST.toLowerCase(),
				SLEIGHT_OF_HAND.toLowerCase(),  CLIMB.toLowerCase(),
				MOVE_SILENTLY.toLowerCase(),    JUMP.toLowerCase(),
				TUMBLE.toLowerCase(),           HIDE.toLowerCase()
		};

		return Arrays.asList(skills).contains(skillName.toLowerCase().trim());
	}

	/**
	 * <p>
	 * The <code>setDefaultSkills(Abilities)</code> sets the default skill every
	 * character has access to. Note that these {@link Skill}s cost two points to
	 * rank up while the clazz specific skills only cost one.
	 * 
	 * @param abilities The abilities of the character
	 * 
	 * @return HashMap containing the default skills
	 * 
	 * @see Abilities
	 * @see Skills
	 * @see Skill
	 * 
	 * @see #addBarbarianSkills(Abilities)
	 * @see #addBardSkills(Abilities)
	 * @see #addClericSkills(Abilities)
	 * @see #addDruidSkills(Abilities)
	 * @see #addFighterSkills(Abilities)
	 * @see #addMageSkills(Abilities)
	 * @see #addMonkSkills(Abilities)
	 * @see #addPaladinSkills(Abilities)
	 * @see #addRangerSkills(Abilities)
	 * @see #addRogueSkills(Abilities)
	 * @see #addWarlockSkills(Abilities)
	 *
	 * @since 0.1.0
	 */
	private static HashMap<String, Skill> setDefaultSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> defaultSkills = new HashMap<>();
		defaultSkills.put(APPRAISE, new Skill(APPRAISE, 0, abilities.get(Abilities.INT), true));
		defaultSkills.put(BALANCE, new Skill(BALANCE, 0, abilities.get(Abilities.DEX), true));
		defaultSkills.put(BLUFF, new Skill(BLUFF, 0, abilities.get(Abilities.CHA), true));
		defaultSkills.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), true));
		defaultSkills.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), true));
		defaultSkills.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), true));
		defaultSkills.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), true));
		defaultSkills.put(DISGUISE, new Skill(DISGUISE, 0, abilities.get(Abilities.CHA), true));
		defaultSkills.put(ESCAPE_ARTIST, new Skill(ESCAPE_ARTIST, 0, abilities.get(Abilities.DEX), true));
		defaultSkills.put(FORGERY, new Skill(FORGERY, 0, abilities.get(Abilities.INT), true));
		defaultSkills.put(GATHER_INFORMATION, new Skill(GATHER_INFORMATION, 0, abilities.get(Abilities.CHA), true));
		defaultSkills.put(HEAL, new Skill(HEAL, 0, abilities.get(Abilities.WIS), true));
		defaultSkills.put(HIDE, new Skill(HIDE, 0, abilities.get(Abilities.DEX), true));
		defaultSkills.put(INTIMIDATE, new Skill(INTIMIDATE, 0, abilities.get(Abilities.CHA), true));
		defaultSkills.put(JUMP, new Skill(JUMP, 0, abilities.get(Abilities.STR), true));
		defaultSkills.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), true));
		defaultSkills.put(MOVE_SILENTLY, new Skill(MOVE_SILENTLY, 0, abilities.get(Abilities.DEX), true));
		defaultSkills.put(RIDE, new Skill(RIDE, 0, abilities.get(Abilities.DEX), true));
		defaultSkills.put(SEARCH, new Skill(SEARCH, 0, abilities.get(Abilities.INT), true));
		defaultSkills.put(SENSE_MOTIVE, new Skill(SENSE_MOTIVE, 0, abilities.get(Abilities.WIS), true));
		defaultSkills.put(SPOT, new Skill(SPOT, 0, abilities.get(Abilities.WIS), true));
		defaultSkills.put(SURVIVAL, new Skill(SURVIVAL, 0, abilities.get(Abilities.WIS), true));
		defaultSkills.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), true));
		defaultSkills.put(USE_ROPE, new Skill(USE_ROPE, 0, abilities.get(Abilities.DEX), true));
		return defaultSkills;
	}

	private static HashMap<String, Skill> addBarbarianSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), false));
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(RIDE, new Skill(RIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), false));
		out.put(JUMP, new Skill(JUMP, 0, abilities.get(Abilities.STR), false));
		out.put(SURVIVAL, new Skill(SURVIVAL, 0, abilities.get(Abilities.WIS), false));
		out.put(INTIMIDATE, new Skill(INTIMIDATE, 0, abilities.get(Abilities.CHA), false));
		out.put(HANDLE_ANIMAL, new Skill(HANDLE_ANIMAL, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addBardSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), false));
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(BALANCE, new Skill(BALANCE, 0, abilities.get(Abilities.DEX), false));
		out.put(ESCAPE_ARTIST, new Skill(ESCAPE_ARTIST, 0, abilities.get(Abilities.DEX), false));
		out.put(HIDE, new Skill(HIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(MOVE_SILENTLY, new Skill(MOVE_SILENTLY, 0, abilities.get(Abilities.DEX), false));
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(APPRAISE, new Skill(APPRAISE, 0, abilities.get(Abilities.INT), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(DECIPHER_SCRIPT, new Skill(DECIPHER_SCRIPT, 0, abilities.get(Abilities.INT), false));
		out.put(PERFORM, new Skill(PERFORM, 0, abilities.get(Abilities.CHA), false));
		out.put(KNOWLEDGE_ARCANE, new Skill(KNOWLEDGE_ARCANE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_HISTORY, new Skill(KNOWLEDGE_HISTORY, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_PLAIN, new Skill(KNOWLEDGE_PLAIN, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_RELIGION, new Skill(KNOWLEDGE_RELIGION, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NATURE, new Skill(KNOWLEDGE_NATURE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NOBILITY_ROYAL, new Skill(KNOWLEDGE_NOBILITY_ROYAL, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_LOCAL, new Skill(KNOWLEDGE_LOCAL, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_ARCHITECTURE, new Skill(KNOWLEDGE_ARCHITECTURE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_DUNGEONS, new Skill(KNOWLEDGE_DUNGEONS, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_GEOGRAPHY, new Skill(KNOWLEDGE_GEOGRAPHY, 0, abilities.get(Abilities.INT), false));
		out.put(SPELL_CRAFT, new Skill(SPELL_CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(SENSE_MOTIVE, new Skill(SENSE_MOTIVE, 0, abilities.get(Abilities.WIS), false));
		out.put(BLUFF, new Skill(BLUFF, 0, abilities.get(Abilities.CHA), false));
		out.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), false));
		out.put(DISGUISE, new Skill(DISGUISE, 0, abilities.get(Abilities.CHA), false));
		out.put(GATHER_INFORMATION, new Skill(GATHER_INFORMATION, 0, abilities.get(Abilities.CHA), false));
		out.put(USE_MAGIC_DEVICE, new Skill(USE_MAGIC_DEVICE, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addClericSkills(@NonNull final Abilities abilities) {
		// The Cleric can have one domain, which is NOT considered by this method!
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_ARCANE, new Skill(KNOWLEDGE_ARCANE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_PLAIN, new Skill(KNOWLEDGE_PLAIN, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_HISTORY, new Skill(KNOWLEDGE_HISTORY, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_RELIGION, new Skill(KNOWLEDGE_RELIGION, 0, abilities.get(Abilities.INT), false));
		out.put(SPELL_CRAFT, new Skill(SPELL_CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(HEAL, new Skill(HEAL, 0, abilities.get(Abilities.WIS), false));
		out.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addDruidSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(RIDE, new Skill(RIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NATURE, new Skill(KNOWLEDGE_NATURE, 0, abilities.get(Abilities.INT), false));
		out.put(SPELL_CRAFT, new Skill(SPELL_CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(SPOT, new Skill(SPOT, 0, abilities.get(Abilities.WIS), false));
		out.put(HEAL, new Skill(HEAL, 0, abilities.get(Abilities.WIS), false));
		out.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), false));
		out.put(SURVIVAL, new Skill(SURVIVAL, 0, abilities.get(Abilities.WIS), false));
		out.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), false));
		out.put(HANDLE_ANIMAL, new Skill(HANDLE_ANIMAL, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addFighterSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), false));
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(JUMP, new Skill(JUMP, 0, abilities.get(Abilities.STR), false));
		out.put(RIDE, new Skill(RIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(INTIMIDATE, new Skill(INTIMIDATE, 0, abilities.get(Abilities.CHA), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(HANDLE_ANIMAL, new Skill(HANDLE_ANIMAL, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addMageSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(DECIPHER_SCRIPT, new Skill(DECIPHER_SCRIPT, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_ARCANE, new Skill(KNOWLEDGE_ARCANE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_HISTORY, new Skill(KNOWLEDGE_HISTORY, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_PLAIN, new Skill(KNOWLEDGE_PLAIN, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_RELIGION, new Skill(KNOWLEDGE_RELIGION, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NATURE, new Skill(KNOWLEDGE_NATURE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NOBILITY_ROYAL, new Skill(KNOWLEDGE_NOBILITY_ROYAL, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_LOCAL, new Skill(KNOWLEDGE_LOCAL, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_ARCHITECTURE, new Skill(KNOWLEDGE_ARCHITECTURE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_DUNGEONS, new Skill(KNOWLEDGE_DUNGEONS, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_GEOGRAPHY, new Skill(KNOWLEDGE_GEOGRAPHY, 0, abilities.get(Abilities.INT), false));
		out.put(SPELL_CRAFT, new Skill(SPELL_CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		return out;
	}

	private static HashMap<String, Skill> addMonkSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), false));
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(JUMP, new Skill(JUMP, 0, abilities.get(Abilities.STR), false));
		out.put(BALANCE, new Skill(BALANCE, 0, abilities.get(Abilities.DEX), false));
		out.put(ESCAPE_ARTIST, new Skill(ESCAPE_ARTIST, 0, abilities.get(Abilities.DEX), false));
		out.put(MOVE_SILENTLY, new Skill(MOVE_SILENTLY, 0, abilities.get(Abilities.DEX), false));
		out.put(TUMBLE, new Skill(TUMBLE, 0, abilities.get(Abilities.DEX), false));
		out.put(HIDE, new Skill(HIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_ARCANE, new Skill(KNOWLEDGE_ARCANE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_RELIGION, new Skill(KNOWLEDGE_RELIGION, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(PERFORM, new Skill(PERFORM, 0, abilities.get(Abilities.CHA), false));
		out.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), false));
		out.put(SENSE_MOTIVE, new Skill(SENSE_MOTIVE, 0, abilities.get(Abilities.WIS), false));
		out.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addPaladinSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(RIDE, new Skill(RIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NOBILITY_ROYAL, new Skill(KNOWLEDGE_NOBILITY_ROYAL, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_RELIGION, new Skill(KNOWLEDGE_RELIGION, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(HEAL, new Skill(HEAL, 0, abilities.get(Abilities.WIS), false));
		out.put(SENSE_MOTIVE, new Skill(SENSE_MOTIVE, 0, abilities.get(Abilities.WIS), false));
		out.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), false));
		out.put(HANDLE_ANIMAL, new Skill(HANDLE_ANIMAL, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addRangerSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), false));
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(JUMP, new Skill(JUMP, 0, abilities.get(Abilities.STR), false));
		out.put(MOVE_SILENTLY, new Skill(MOVE_SILENTLY, 0, abilities.get(Abilities.DEX), false));
		out.put(RIDE, new Skill(RIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(USE_ROPE, new Skill(USE_ROPE, 0, abilities.get(Abilities.DEX), false));
		out.put(HIDE, new Skill(HIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(SEARCH, new Skill(SEARCH, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_NATURE, new Skill(KNOWLEDGE_NATURE, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_GEOGRAPHY, new Skill(KNOWLEDGE_GEOGRAPHY, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_DUNGEONS, new Skill(KNOWLEDGE_DUNGEONS, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(SPOT, new Skill(SPOT, 0, abilities.get(Abilities.WIS), false));
		out.put(HEAL, new Skill(HEAL, 0, abilities.get(Abilities.WIS), false));
		out.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), false));
		out.put(SURVIVAL, new Skill(SURVIVAL, 0, abilities.get(Abilities.WIS), false));
		out.put(HANDLE_ANIMAL, new Skill(HANDLE_ANIMAL, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addRogueSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CLIMB, new Skill(CLIMB, 0, abilities.get(Abilities.STR), false));
		out.put(SWIM, new Skill(SWIM, 0, abilities.get(Abilities.STR), false));
		out.put(JUMP, new Skill(JUMP, 0, abilities.get(Abilities.STR), false));
		out.put(BALANCE, new Skill(BALANCE, 0, abilities.get(Abilities.DEX), false));
		out.put(ESCAPE_ARTIST, new Skill(ESCAPE_ARTIST, 0, abilities.get(Abilities.DEX), false));
		out.put(MOVE_SILENTLY, new Skill(MOVE_SILENTLY, 0, abilities.get(Abilities.DEX), false));
		out.put(OPEN_LOCK, new Skill(OPEN_LOCK, 0, abilities.get(Abilities.DEX), false));
		out.put(USE_ROPE, new Skill(USE_ROPE, 0, abilities.get(Abilities.DEX), false));
		out.put(SLEIGHT_OF_HAND, new Skill(SLEIGHT_OF_HAND, 0, abilities.get(Abilities.DEX), false));
		out.put(TUMBLE, new Skill(TUMBLE, 0, abilities.get(Abilities.DEX), false));
		out.put(HIDE, new Skill(HIDE, 0, abilities.get(Abilities.DEX), false));
		out.put(APPRAISE, new Skill(APPRAISE, 0, abilities.get(Abilities.INT), false));
		out.put(FORGERY, new Skill(FORGERY, 0, abilities.get(Abilities.INT), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(DISABLE_DEVICE, new Skill(DISABLE_DEVICE, 0, abilities.get(Abilities.INT), false));
		out.put(DECIPHER_SCRIPT, new Skill(DECIPHER_SCRIPT, 0, abilities.get(Abilities.INT), false));
		out.put(SEARCH, new Skill(SEARCH, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_LOCAL, new Skill(KNOWLEDGE_LOCAL, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(PERFORM, new Skill(PERFORM, 0, abilities.get(Abilities.CHA), false));
		out.put(SPOT, new Skill(SPOT, 0, abilities.get(Abilities.WIS), false));
		out.put(LISTEN, new Skill(LISTEN, 0, abilities.get(Abilities.WIS), false));
		out.put(SENSE_MOTIVE, new Skill(SENSE_MOTIVE, 0, abilities.get(Abilities.WIS), false));
		out.put(BLUFF, new Skill(BLUFF, 0, abilities.get(Abilities.CHA), false));
		out.put(DIPLOMACY, new Skill(DIPLOMACY, 0, abilities.get(Abilities.CHA), false));
		out.put(INTIMIDATE, new Skill(INTIMIDATE, 0, abilities.get(Abilities.CHA), false));
		out.put(GATHER_INFORMATION, new Skill(GATHER_INFORMATION, 0, abilities.get(Abilities.CHA), false));
		out.put(USE_MAGIC_DEVICE, new Skill(USE_MAGIC_DEVICE, 0, abilities.get(Abilities.CHA), false));
		out.put(DISGUISE, new Skill(DISGUISE, 0, abilities.get(Abilities.CHA), false));
		return out;
	}

	private static HashMap<String, Skill> addWarlockSkills(@NonNull final Abilities abilities) {
		requireNonNull(abilities, "Abilities may not be null");
		final HashMap<String, Skill> out = new HashMap<>();
		out.put(CONCENTRATION, new Skill(CONCENTRATION, 0, abilities.get(Abilities.CON), false));
		out.put(CRAFT, new Skill(CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(KNOWLEDGE_ARCANE, new Skill(KNOWLEDGE_ARCANE, 0, abilities.get(Abilities.INT), false));
		out.put(SPELL_CRAFT, new Skill(SPELL_CRAFT, 0, abilities.get(Abilities.INT), false));
		out.put(PROFESSION, new Skill(PROFESSION, 0, abilities.get(Abilities.WIS), false));
		out.put(BLUFF, new Skill(BLUFF, 0, abilities.get(Abilities.CHA), false));
		return out;
	}
}
