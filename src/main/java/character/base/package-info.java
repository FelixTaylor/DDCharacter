/**
 * <p>
 * The base package holds important classes which are used to generate a valid
 * 'Dungeon & Dragons' character. Most of these classes are using classes from
 * the components package. For example the {@link character.base.Abilities} class uses the
 * {@link character.components.Ability} class to provide the needed functionality to the
 * {@link character.DDCharacter} object.
 * <p>
 * <b>Note:</b> You should use the character object located in the
 * <code>character</code> package to generate characters. Using classes from the
 * sub packages can cause unexpected errors which could be hard to debug.
 * <p>
 * You can find the corresponding test files in the <b>test</b>.character.base
 * package.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.1.2
 *
 * @see character.base.Abilities
 * @see character.base.BaseAttack
 * @see character.base.Disposition
 * @see character.base.Names
 * @see character.base.SavingThrows
 * @see character.base.Skills
 */
package character.base;