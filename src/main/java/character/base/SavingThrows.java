/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import character.DDCharacter;
import character.components.Ability;
import character.components.Clazz;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;

import static character.base.Abilities.*;
import static java.util.Objects.*;

/**
 * <p>
 * The <code>SavingThrows</code> class is used to calculate the saving throws of
 * the character by using the characters {@link Clazz}, {@link int}, dexterity,
 * constitution and wisdom {@link Ability}.
 * <p>
 * Every character can have a good or bad saving throw base from which the
 * actual saving throw for <code>REFLEX, WILL</code> and <code>FORTITUDE</code>
 * is being calculated. Check out the 'Player's Hand Book' on page 42.
 * 
 * @see Ability
 * @see Abilities
 * @see Clazz
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.1
 */
public class SavingThrows {
	private SavingThrows() {
		// Private constructor so object is not initializable
	}

	/**
	 * <p>
	 * The <code>badSavingThrowBase(int)</code> method is used by the
	 * {@link SavingThrows} calculation if the value of the SavingThrow is only
	 * increased for every third level of the character.
	 *
	 * @see Clazz
	 * @see #goodSavingThrowBase(int)
	 *
	 * @param level The level of the character
	 *
	 * @since 0.1.0
	 */
	private static int badSavingThrowBase(final int level) {
		return level / 3;
	}

	/**
	 * <p>
	 * The <code>goodSavingThrowBase(int)</code> method is used by the
	 * {@link SavingThrows} calculation if the value of the SavingThrow is increased
	 * for every second level of the character.
	 *
	 * @see Clazz
	 * @see #badSavingThrowBase(int)
	 *
	 * @param level The level of the character
	 *
	 * @since 0.1.0
	 */
	private static int goodSavingThrowBase(final int level) {
		return 2 + (level / 2);
	}

	/**
	 * <p>The <code>getWill()</code> method returns the will value of the
	 * current character.
	 *
	 * @return int : the character will
	 *
	 * @since 0.1.0
	 */
	public static int getWill(@NonNull final Clazz clazz, @NonNull final Abilities abilities, final int level) {
		requireNonNull(clazz, "Clazz may not be null.");
		requireNonNull(abilities, "Abilities may not be null.");
		return abilities.getModifier(WIS) + calculateWill(clazz, level);
	}

	/**
	 * <p>The <code>getReflex()</code> method returns the reflex value of the
	 * current character.
	 *
	 * @return int : the character reflex
	 *
	 * @since 0.1.0
	 */
	public static int getReflex(@NonNull final Clazz clazz, @NonNull final Abilities abilities, final int level) {
		requireNonNull(clazz, "Clazz may not be null.");
		requireNonNull(abilities, "Abilities may not be null.");
		return abilities.getModifier(DEX) + calculateReflex(clazz, level);
	}

	/**
	 * <p>The <code>getFortitude()</code> method returns the fortitude value of
	 * the current character.
	 *
	 * @return int : the characters fortitude
	 *
	 * @since 0.1.0
	 */
	public static int getFortitude(@NonNull final Clazz clazz, @NonNull final Abilities abilities, final int level) {
		requireNonNull(clazz, "Clazz may not be null.");
		requireNonNull(abilities, "Abilities may not be null.");
		return abilities.getModifier(CON) + calculateFortitude(clazz, level);
	}

	/**
	 * <p>
	 * The <code>calculateWill(Clazz, int)</code> method sets the will attribute for
	 * the current character.
	 *
	 * @see Clazz
	 * @see SavingThrows
	 *
	 * @param clazz The clazz of the character
	 * @param level The level of the character
	 *
	 * @since 0.1.0
	 */
	private static int calculateWill( @NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null.");

		switch (clazz) {
		case BARD:
		case CLERIC:
		case DRUID:
		case MAGE:
		case MONK:
		case WARLOCK: {
			return goodSavingThrowBase(level);
		}
		default:
			return badSavingThrowBase(level);
		}
	}

	/**
	 * <p>
	 * The <code>calculateReflex(Clazz, int)</code> method sets the reflex attribute
	 * for the current character.
	 *
	 * @see Clazz
	 * @see SavingThrows
	 *
	 * @param clazz The clazz of the character
	 * @param level The level of the character
	 *
	 * @since 0.1.0
	 */
	private static int calculateReflex(@NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null");
		switch (clazz) {
		case BARD:
		case MONK:
		case RANGER:
		case ROGUE: {
			return goodSavingThrowBase(level);
		}
		default:
			return badSavingThrowBase(level);
		}
	}

	/**
	 * <p>
	 * The <code>calculateFortitude(Clazz, int)</code> method sets the fortitude
	 * attribute for the current character.
	 *
	 * @see Clazz
	 * @see SavingThrows
	 *
	 * @param clazz The clazz of the character
	 * @param level The level of the character
	 *
	 * @since 0.1.0
	 */
	private static int calculateFortitude(@NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null");
		switch (clazz) {
		case BARBARIAN:
		case CLERIC:
		case DRUID:
		case FIGHTER:
		case MONK:
		case PALADIN:
		case RANGER: {
			return goodSavingThrowBase(level);
		}
		default:
			return badSavingThrowBase(level);
		}
	}
}