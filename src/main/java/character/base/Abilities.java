/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import character.DDCharacter;
import character.components.Ability;
import character.components.Clazz;
import character.components.DDLevel;
import character.components.Race;
import exceptions.InvalidInputException;
import org.checkerframework.checker.nullness.qual.NonNull;
import utils.Dice;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.HashMap;

import static java.util.Objects.requireNonNull;

/**
 * <p>
 * The <code>Abilities</code> class contains a set of six single abilities each
 * defined in the {@link Ability} class of the components package. The
 * <code>Abilities</code> available are
 * <code><b>strength</b>, <b>dexterity</b>, <b>constitution</b>, <b>intelligence</b>,
 * <b>wisdom</b></code> and <code><b>charisma</b></code>. Do not create your own
 * abilities or change the name of the existing ones! If you have to use this
 * class do it as follow:
 * 
 * <pre>
 * Abilities abilities = new Abilities();
 * abilities.set(new int[] { 11, 13, 12, 18, 9, 10 });
 * 
 * {@literal //} or to generate the abilities randomly:
 * abilities.random(Clazz.HUMAN);
 * </pre>
 * <p>
 * Note: You can use the {@link #distribute(Clazz)} method to distribute the
 * ability values to fit the characters need. For example a fighter benefits
 * from a high strength, dexterity or constitution value.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.1.1
 * 
 * @see Ability
 * @see Clazz
 * @see Clazz#getPriorities()
 * @see DDCharacter
 * @see DDCharacter#getAbilities()
 */
public class Abilities /* implements Serializable */ {
	private final Dice diceSet = new Dice(4, 6);

	/**
	 * <p>
	 * The <code>STR</code> variable can be used to select the <code>strength</code>
	 * {@link Ability} from the {@link Abilities} class. Following example returns
	 * the strength ability value:
	 * 
	 * <pre>
	 * {@literal //} Abilities a = new Abilities().random(Clazz.BARD);
	 * <b>a.get(Abilities.STR);</b>
	 * </pre>
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #getModifier(Object)
	 * @see #getValue(Object)
	 *
	 * @since 0.1.0
	 */
	public static final String STR = "str";

	/**
	 * <p>
	 * The <code>DEX</code> variable can be used to select the
	 * <code>dexterity</code> {@link Ability} from the {@link Abilities} class.
	 * Following example returns the dexterity ability value:
	 * 
	 * <pre>
	 * {@literal //} Abilities a = new Abilities().random(Clazz.BARD);
	 * <b>a.get(Abilities.DEX);</b>
	 * </pre>
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #getModifier(Object)
	 * @see #getValue(Object)
	 *
	 * @since 0.1.0
	 */
	public static final String DEX = "dex";

	/**
	 * <p>
	 * The <code>CON</code> variable can be used to select the
	 * <code>constitution</code> {@link Ability} from the {@link Abilities} class.
	 * Following example returns the constitution ability value:
	 * 
	 * <pre>
	 * {@literal //} Abilities a = new Abilities().random(Clazz.BARD);
	 * <b>a.get(Abilities.CON);</b>
	 * </pre>
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #getModifier(Object)
	 * @see #getValue(Object)
	 *
	 * @since 0.1.0
	 */
	public static final String CON = "con";

	/**
	 * <p>
	 * The <code>INT</code> variable can be used to select the
	 * <code>intelligence</code> {@link Ability} from the {@link Abilities} class.
	 * Following example returns the intelligence ability value:
	 * 
	 * <pre>
	 * {@literal //} Abilities a = new Abilities().random(Clazz.BARD);
	 * <b>a.get(Abilities.INT);</b>
	 * </pre>
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #getModifier(Object)
	 * @see #getValue(Object)
	 *
	 * @since 0.1.0
	 */
	public static final String INT = "int";

	/**
	 * <p>
	 * The <code>WIS</code> variable can be used to select the <code>wisdom</code>
	 * {@link Ability} from the {@link Abilities} class. Following example returns
	 * the wisdom ability value:
	 * 
	 * <pre>
	 * {@literal //} Abilities a = new Abilities().random(Clazz.BARD);
	 * <b>a.get(Abilities.WIS);</b>
	 * </pre>
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #getModifier(Object)
	 * @see #getValue(Object)
	 *
	 * @since 0.1.0
	 */
	public static final String WIS = "wis";

	/**
	 * <p>
	 * The <code>CHA</code> variable can be used to select the <code>charisma</code>
	 * {@link Ability} from the {@link Abilities} class. Following example returns
	 * the charisma ability value:
	 * 
	 * <pre>
	 * {@literal //} Abilities a = new Abilities().random(Clazz.BARD);
	 * <b>a.get(Abilities.CHA);</b>
	 * </pre>
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #getModifier(Object)
	 * @see #getValue(Object)
	 *
	 * @since 0.1.0
	 */
	public static final String CHA = "cha";

	private final Ability strength;
	private final Ability dexterity;
	private final Ability constitution;
	private final Ability intelligence;
	private final Ability wisdom;
	private final Ability charisma;

	/**
	 * <p>
	 * <code>Abilities()</code> initializes the main <code>abilities</code> of a
	 * character. Each ability is set to one.
	 * <p>
	 * <b>DO NOT</b> use any other ability names then those listed in the
	 * {@link Abilities}. All names are available to use via the public static final
	 * strings of this class. If you do use other ability names as the given ones
	 * the library will fail to generate a valid {@link DDCharacter}.
	 *
	 * @see Ability
	 * @see DDCharacter
	 * @see #distribute(Clazz)
	 *
	 * @since 0.1.0
	 */
	public Abilities() {
		this.strength = new Ability("Strength");
		this.dexterity = new Ability("Dexterity");
		this.constitution = new Ability("Constitution");
		this.intelligence = new Ability("Intelligence");
		this.wisdom = new Ability("Wisdom");
		this.charisma = new Ability("Charisma");
	}

	/**
	 * <p>
	 * The <code>Abilities(Race, Clazz, DDLevel)</code> constructor initializes a
	 * new random set of abilities. In that process the race bonus and level bonus
	 * are added to the abilities. The clazz determines the preferred abilities.
	 * 
	 * @param race  The race of the character
	 * @param clazz The clazz of the character
	 * @param level The level of the character
	 * 
	 * @see Abilities
	 * @see Clazz
	 * @see Race
	 * 
	 * @see #Abilities(Race, Clazz, DDLevel, int[])
	 *
	 * @since 0.1.0
	 */
	public Abilities(@NonNull final Race race, @NonNull final Clazz clazz, final int level) {
		this();
		requireNonNull(race, "Race may not be null");
		requireNonNull(clazz, "Clazz may not be null");
		random(race, clazz, level);
	}

	/**
	 * <p>
	 * The <code>Abilities(Race, Clazz, DDLevel, int[])</code> constructor
	 * initializes a new set of abilities corresponding to the int array parameter.
	 * To generate a random set of abilities, use the
	 * {@link #Abilities(Race, Clazz, int)} constructor.
	 * 
	 * @param race      The race of the character
	 * @param clazz     The clazz of the character
	 * @param level     The level of the character
	 * @param abilities The abilities of the character
	 * 
	 * @see Abilities
	 * @see Clazz
	 * @see Race
	 * 
	 * @see #Abilities(Race, Clazz, int)
	 *
	 * @since 0.1.0
	 */
	public Abilities(@NonNull final Race race, @NonNull final Clazz clazz, @NonNull final DDLevel level,
					 final int[] abilities) {
		this();
		requireNonNull(race, "Race may not be null");
		requireNonNull(clazz, "Clazz may not be null");

		set(abilities);
		addPoints(clazz, level.getLevel());
		applyRaceBonus(race);
	}

	/**
	 * <p>
	 * The <code>levelUp(Clazz, int, int)</code> method increases one of the two
	 * main abilities of the clazz if the character reaches certain levels. This
	 * method is called by the {@link DDCharacter#levelUp()} method and should only
	 * be used with great care!
	 * 
	 * @param clazz    The clazz of the character
	 * @param oldLevel The previous level of the character
	 * @param newLevel The new level of the character
	 *
	 * @since 0.1.0
	 */
	public void levelUp(@NonNull final Clazz clazz, final int oldLevel, final int newLevel) {
		requireNonNull(clazz, "Clazz may not be null");
		removePoints(clazz, oldLevel);
		addPoints(clazz, newLevel);
	}

	/**
	 * <p>
	 * The <code>random(Clazz)</code> method randomly sets all abilities and calls
	 * the {@link #distribute(Clazz)} method which will order the abilities to fit
	 * the given {@link Clazz}.
	 * 
	 * <pre>
	 * Abilities a = new Abilities().random(Clazz.HUMAN);
	 * {@literal //} or: a.random(Clazz.BARD);
	 * </pre>
	 * 
	 * @param clazz The clazz of the character
	 * 
	 * @return This abilities object
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 * @see Clazz
	 * @see Clazz#getPriorities()
	 * @see #distribute(Clazz)
	 *
	 * @since 0.1.0
	 */
	public Abilities random(@NonNull final Race race, @NonNull final Clazz clazz, final int level) {
		requireNonNull(race, "Race may not be null");
		requireNonNull(clazz, "Clazz may not be null");

		for (Ability a : asArray()) {
			try {
				a.setValue(this.diceSet.roll().eraseSmallestDice().sum());
			} catch (InvalidInputException e) {
				e.printStackTrace();
			}
		}

		distribute(clazz);
		addPoints(clazz, level);
		applyRaceBonus(race);

		return this;
	}

	/**
	 * <p>
	 * The <code>distribute(Clazz)</code> method decides by reference to the
	 * {@link Clazz#getPriorities()} how important each ability for the character
	 * is. Example: The preferred abilities of an barbarian are dexterity and
	 * strength. For a paladin they are charisma and wisdom.
	 * 
	 * <pre>
	 * Abilities a = new Abilities();
	 * a.set(new int[] { 12, 15, 19, 9, 10, 11 });
	 * a.distribute(Clazz.Barbarian);
	 * </pre>
	 * 
	 * @param clazz The clazz of the character.
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 * @see Clazz
	 * @see Clazz#getPriorities()
	 *
	 * @since 0.1.0
	 */
	public void distribute(@NonNull final Clazz clazz) {
		requireNonNull(clazz, "Clazz may not be null");

		int[] values = new int[asArray().length];
		for (int i = 0; i < values.length; i++) {
			values[i] = getValue(i);
		}

		Arrays.sort(values);
		for (int i = 5; i != -1; i--) {
			set(clazz.getPriorities()[i], values[i]);
		}
	}

	/**
	 * <p>
	 * For every fourth level the character will get an additional point on one of
	 * its two main {@link Ability} defined in the {@link Clazz#getPriorities()}
	 * array.
	 *
	 * @param clazz The Clazz of the character
	 * @param level The level of the character
	 *
	 * @see Clazz
	 * @see Clazz#getPriorities()
	 * @see DDLevel
	 * @see #removePoints(Clazz, int)
	 *
	 * @since 0.1.0
	 */
	public void addPoints(@NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null");

		boolean isFirst = true;
		for (int i=0; i < level / 4; i++) {
			int pos = isFirst ? 5 : 4;
			int newValue = getValue(clazz.getPriorities()[pos]) + 1;
			set(clazz.getPriorities()[pos], newValue);
			isFirst = !isFirst;
		}
	}

	/**
	 * <p>
	 * The <code>applyRaceBonus(Race)</code> method takes a {@link Race} as parameter
	 * and adds +2 on the positive ability and removes 2 points from the negative
	 * ability. This method is called <b>once</b> if the race of the character was
	 * set or changed.
	 * <p>
	 * The method will check if the decreased ability has a greater value then 0
	 * (zero) if not the ability value will be fixed at 1. Zero or a negative
	 * ability value is not valid!
	 *
	 * @param race The race of the character
	 * 
	 * @see Ability
	 * @see DDCharacter
	 * @see DDCharacter#set(Object...)
	 * @see #removeRaceBonus(Race)
	 *
	 * @since 0.1.0
	 */
	public void applyRaceBonus(@NonNull final Race race) {
		requireNonNull(race, "Race may not be null");

		if (race != Race.HUMAN && race != Race.HALFELF) {
			set(race.getPositiveAbility(), getValue(race.getPositiveAbility()) + 2);

			int oldValue = getValue(race.getNegativeAbility());
			int newValue = Math.max(1, (oldValue - 2));
			set(race.getNegativeAbility(), newValue);
		}
	}

	/**
	 * <p>
	 * The <code>removeRaceBonus(Race)</code> method takes a {@link Race} as
	 * parameter and removes 2 points from the positive ability and adds 2 points to
	 * the negative ability. This method is called <b>once</b> if the race of the
	 * character was set or changed.
	 * <p>
	 * The method will check if the decreased ability has a greater value then 0
	 * (zero) if not the ability value will be fixed at 1. Zero or a negative
	 * ability value is not valid!
	 *
	 * @param race The race of the character
	 * 
	 * @see Ability
	 * @see DDCharacter
	 * @see DDCharacter#set(Object...)
	 * @see #applyRaceBonus(Race)
	 *
	 * @since 0.1.0
	 */
	// Note: This method is only needed if the race is able to change during the
	// existence of an character. If the DDController class is used to generate
	// characters it is not possible to change the race after initializing the
	// character.
	@Deprecated
	public void removeRaceBonus(@NonNull final Race race) {
		requireNonNull(race, "Race may not be null");

		if (race != Race.HUMAN && race != Race.HALFELF) {
			String pos = race.getPositiveAbility();
			String neg = race.getNegativeAbility();

			int points = Math.max(1, getValue(pos) - 2);
			set(pos, points);

			set(neg, getValue(neg) + 2);
		}
	}

	/**
	 * <p>
	 * For every fourth level the character will lose one point of one of its main
	 * <code>Ability</code> defined in the {@link Clazz#getPriorities()} array. This
	 * method is the 'counter part' to the {@link #addPoints(Clazz, int)} method.
	 *
	 * @see Clazz
	 * @see Clazz#getPriorities()
	 * @see DDCharacter
	 * @see DDLevel
	 * @see #addPoints(Clazz, int)
	 *
	 * @param clazz The clazz of the character
	 * @param level The level of the character
	 *
	 *  * @since 0.1.0
	 */
	public void removePoints(@NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null");

		boolean isFirst = true;
		for (int i=0; i < level / 4; i++) {
			int pos = isFirst ? 5 : 4;
			int oldValue = getValue(clazz.getPriorities()[pos]);
			int newValue = Math.max(1, (oldValue - 1));
			set(clazz.getPriorities()[pos], newValue);
			isFirst = !isFirst;
		}

//		boolean isFirst = true;
//		for (int i = 1; i <= level; i++) {
//			if (i % 4 == 0) {
//				if (isFirst) {
//
//					// The '5' is important, because that is the ability with the highest priority!
//					int oldValue = getValue(clazz.getPriorities()[5]);
//					int newValue = Math.max(1, (oldValue - 1));
//					set(clazz.getPriorities()[5], newValue);
//					isFirst = false;
//
//				} else {
//
//					// The '4' is important, because it's the ability with the SECOND highest
//					// priority!
//					int oldValue = getValue(clazz.getPriorities()[4]);
//					int newValue = Math.max(1, (oldValue - 1));
//					set(clazz.getPriorities()[4], newValue);
//					isFirst = true;
//
//				}
//			}
//		}
	}

	/**
	 * <p>
	 * The <code>set(Object,Integer)</code> sets the value of the
	 * <code>Ability</code> with the given name or index to the value of the second
	 * parameter. Valid options for 'o' are:
	 * <ul>
	 * <li>0, 'str'
	 * <li>1, 'dex'
	 * <li>2, 'con'
	 * <li>3, 'int'
	 * <li>4, 'wis'
	 * <li>5, 'cha'
	 * </ul>
	 * <p>
	 * <b>Note:</b> Use the public variables provided by this class to select a
	 * valid ability by name:
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.set(Abilities.STR, 11);
	 * </pre>
	 * <p>
	 * All the following calls of this method will set value of the strength ability
	 * to 11.
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.set(0, 11);
	 * a.set("str", 11);
	 * a.set(Abilities.STR, 11);
	 * </pre>
	 *
	 * @param o     can be a Integer (0-5) or a String representing the name of the
	 *              ability or an <code>Ability</code> object.
	 * @param value Integer the new value of the Ability
	 *
	 * @see Ability
	 * @see Abilities
	 * @see #get(Object)
	 * @see #asArray()
	 * @see #asHashMap()
	 *
	 * @since 0.1.0
	 */
	public void set(@NonNull final Object o, int value) {
		requireNonNull(o, "Given object may not be null.");

		try {
			if (o instanceof Integer) {
				asArray()[(int) o].setValue(value);
			} else if (o instanceof String) {
				asHashMap().get(o).setValue(value);
			} else {
				throw new InvalidParameterException(
						"Invalid parameter, o(" + o.getClass() + ") is invalid. Use String or Integer.");
			}
		} catch (InvalidInputException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * The <code>set(int[])</code> method sets the current abilities to the given
	 * values. If the length of the given array does not match the length of the
	 * abilities array the method will throw a {@link InvalidParameterException}.
	 * The values are <b>NOT</b> sorted in the process, use the
	 * {@link #distribute(Clazz)} method to do so. For example:
	 *
	 * <pre>
	 * Abilities a = new Abilities();
	 * <b>a.set(new int[] { 12, 15, 19, 9, 10, 11 });</b>
	 * {@literal //} a.distribute(Clazz.Barbarian);
	 * </pre>
	 * 
	 * @param abilities the new abilities for the character
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #distribute(Clazz)
	 * @see #set(Object, int)
	 *
	 * @since 0.1.0
	 */
	public void set(final int[] abilities) {
		if (abilities.length != asArray().length) {
			throw new InvalidParameterException("The length of the given array(length=" + abilities.length
					+ ") does not equal the the length of the abilities array(" + asArray().length + ")");
		}
		try {
			for (int i = 0; i < asArray().length; i++) {
				asArray()[i].setValue(abilities[i]);
			}
		} catch (InvalidInputException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * The <code>get(Object)</code> methods returns the ability associated with the
	 * given object, which can be either an Integer or a String. Valid options for
	 * 'o' are:
	 * <ul>
	 * <li>0, 'str'
	 * <li>1, 'dex'
	 * <li>2, 'con'
	 * <li>3, 'int'
	 * <li>4, 'wis'
	 * <li>5, 'cha'
	 * </ul>
	 * <p>
	 * <b>Note:</b> Use the public variables provided by this class to use a valid
	 * ability by name:
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.get(Abilities.STR);
	 * </pre>
	 * <p>
	 * All the following calls of this method will return the value of the strength
	 * ability.
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.get(0);
	 * a.get("str");
	 * a.get(Abilities.STR);
	 * </pre>
	 *
	 * @param o Object representation of the ability
	 * 
	 * @return the {@link Ability} corresponding to the given parameter
	 *
	 * @see Ability
	 * @see Abilities
	 * @see #set(Object, int)
	 * @see #asArray()
	 * @see #asHashMap()
	 *
	 * @since 0.1.0
	 */
	public Ability get(@NonNull final Object o) {
		if (o instanceof Integer) {
			return asArray()[(Integer) o];
		} else if (o instanceof String) {
			return asHashMap().get(o);
		}

		throw new InvalidParameterException(
				"Invalid parameter, o(" + o.getClass() + ") is invalid. Use String or Integer.");
	}

	/**
	 * <p>
	 * The <code>getValue(Object)</code> returns an Integer value which represents
	 * the value of the <code>Ability</code>. All the following calls of this method
	 * will return the value of the strength ability.
	 *
	 * <p>
	 * <b>Note:</b> Use the public variables provided by this class to use a valid
	 * ability by name:
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.get(Abilities.STR);
	 * </pre>
	 * <p>
	 * All the following calls of this method will return the value of the strength
	 * ability.
	 *
	 * <pre>
	 * Abilities a = new Abilities();
	 * a.getValue(0);
	 * a.getValue("str");
	 * a.getValue(Abilities.STR);
	 * </pre>
	 *
	 * @param o can be a Integer (0-5) or a String representing the name of the
	 *          ability. Valid options for 'o' are:
	 *          <ul>
	 *          <li>0, 'str'
	 *          <li>1, 'dex'
	 *          <li>2, 'con'
	 *          <li>3, 'int'
	 *          <li>4, 'wis'
	 *          <li>5, 'cha'
	 *          </ul>
	 *
	 * @return Integer The value of the Ability
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 * @see #set(Object, int)
	 *
	 * @since 0.1.0
	 */
	public int getValue(@NonNull final Object o) {
		return get(o).getValue();
	}

	/**
	 * <p>
	 * The <code>getModifier(Object)</code> method returns the modifier value of the
	 * ability corresponding to the input. The input <b>must</b> be an Integer or a
	 * String. Valid input values are:
	 * <ul>
	 * <li>0, 'str'
	 * <li>1, 'dex'
	 * <li>2, 'con'
	 * <li>3, 'int'
	 * <li>4, 'wis'
	 * <li>5, 'cha'
	 * </ul>
	 *
	 * <p>
	 * <b>Note:</b> Use the public variables provided by this class to use a valid
	 * ability by name:
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.getModifier(Abilities.STR);
	 * </pre>
	 * <p>
	 * All the following calls of this method will return the modifier of the
	 * strength ability.
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.getModifier(0);
	 * a.getModifier("str");
	 * a.getModifier(Abilities.STR);
	 * </pre>
	 *
	 * @param o Integer or String representing the ability of which the modifier
	 *          value is requested.
	 * 
	 * @return Integer : modifier of the ability
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 *
	 * @since 0.1.0
	 */
	public int getModifier(@NonNull final Object o) {
		return get(o).getMod();
	}

	/**
	 * <p>
	 * The <code>getName(Object)</code> method returns the name of the ability
	 * corresponding to the input. The input <b>must</b> be an Integer or a String.
	 * Valid input values are:
	 * <ul>
	 * <li>0, 'str'
	 * <li>1, 'dex'
	 * <li>2, 'con'
	 * <li>3, 'int'
	 * <li>4, 'wis'
	 * <li>5, 'cha'
	 * </ul>
	 *
	 * <p>
	 * <b>Note:</b> Use the public variables provided by this class to use a valid
	 * ability by name:
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.getName(Abilities.STR);
	 * </pre>
	 * <p>
	 * All the following calls of this method will return the name of the strength
	 * ability.
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.getName(0);
	 * a.getName("str");
	 * a.getName(Abilities.STR);
	 * </pre>
	 * <p>
	 * <b>Note #2:</b> You probably noticed that the call
	 * <code>a.getName("str")</code> is kind of funny. This method is mainly used to
	 * get the ability name easily while iterating over the abilities.
	 *
	 * @param o Integer or String representing the ability of which the modifier
	 *          value is requested.
	 * 
	 * @return String The name of the ability
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 *
	 * @since 0.1.0
	 */
	public String getName(@NonNull final Object o) {
		return get(o).getName();
	}

	/**
	 * <p>
	 * The <code>asArray()<code> method returns an array containing all abilities in
	 * the following order:
	 * <ul>
	 * <li>0, 'strength'
	 * <li>1, 'dexterity'
	 * <li>2, 'constitution'
	 * <li>3, 'intelligence'
	 * <li>4, 'wisdom'
	 * <li>5, 'charisma'
	 * </ul>
	 * <p>
	 * This method is mainly used to simplify the {@link #get(Object)} and
	 * {@link #set(Object, int)} methods.
	 * 
	 * @return All abilities in array
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 * @see #asArray()
	 * @see #get(Object)
	 * @see #set(Object, int)
	 *
	 * @since 0.1.0
	 */
	public Ability[] asArray() {
		return new Ability[] { this.strength, this.dexterity, this.constitution, this.intelligence, this.wisdom,
				this.charisma };
	}

	/**
	 * <p>
	 * The <code>asHashMap()</code> method returns all abilities as a HashMap so
	 * they can be selected by there names. This method is mainly used to simplify
	 * the {@link #get(Object)} and {@link #set(Object, int)} methods.
	 *
	 * <pre>
	 * {@literal //} Abilities a = new Abilities();
	 * a.asHashMap().get(Abilities.STR);
	 * 
	 * <pre>
	 *
	 * @return HashMap including all available abilities
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see DDCharacter
	 * @see #asArray()
	 * @see #get(Object)
	 * @see #set(Object, int)
	 *
	 * @since 0.1.0
	 */
	public HashMap<String, Ability> asHashMap() {
		HashMap<String, Ability> map = new HashMap<>();
		map.put(STR, this.strength);
		map.put(DEX, this.dexterity);
		map.put(CON, this.constitution);
		map.put(INT, this.intelligence);
		map.put(WIS, this.wisdom);
		map.put(CHA, this.charisma);
		return map;
	}
}