/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import character.components.Gender;
import character.components.Race;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Random;

import static java.util.Objects.requireNonNull;

/**
 * <p>
 * The <code>Names</code> class provides an easy way to generate random names
 * for a new character.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.1.2
 *
 * @see Gender
 * @see #random(Race, Gender)
 */
public class Names {
	private static final Random R = new Random();

	/**
	 * <p>
	 * The <code>random(Race, Gender)</code> method selects a random name for
	 * different races and genders.
	 * 
	 * @param race   The race of the character
	 * @param gender The gender of the character
	 * 
	 * @return Random name for the character
	 * 
	 * @see Race
	 * @see Gender
	 *
	 * @since 0.1.0
	 */
	public static String random(@NonNull final Race race, @NonNull final Gender gender) {
		requireNonNull(race, "Race may not be null");
		requireNonNull(gender, "Gendre may not be null.");

		switch (race) {
		case DWARF:
			return dwarf(gender);
		case ELF:
			return elf(gender);
		case GNOME:
			return gnome(gender);
		case HALFELF:
			return halfelf(gender);
		case HALFLING:
			return halfling(gender);
		case HALFORK:
			return halfork(gender);
		default:
			return human(gender);
		}
	}

	private static String getName(@NonNull final Gender gender, final String[] female, final String[] male) {
		requireNonNull(gender, "Gender may not be null.");

		switch (gender) {
		case FEMALE:
			return female[R.nextInt(female.length)];

		case MALE:
			return male[R.nextInt(male.length)];

		default:
		case NONE:
		case QUEER:
			if ((R.nextInt(2) == 1)) {
				return male[R.nextInt(male.length)];
			} else {
				return female[R.nextInt(female.length)];
			}
		}
	}

	private static String dwarf(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] female = { "Bimpnotti", "Caramip", "Duvamil", "Ellywick", "Ellyjobell", "Tupfschleife", "Mardnab",
				"Roywyn", "Shamil", "Wegwocket" };
		String[] male = { "Boddynock", "Dimbel", "Fonkin", "Glim", "Gerbo", "Jebeddo", "Namfudel", "Rundar", "Siebo",
				"Zuck" };

		return getName(gender, female, male);
	}

	private static String elf(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] male = { "Amastacia", "Amakiir", "Galanodel", "Holimion", "Liadon", "Meliamne", "Nailo", "Siannodel",
				"Ilphukiir", "Xiloszient" };

		String[] female = { "Anastrianna", "Antinua", "Drusilia", "Felosial", "Ielelnia", "Lia", "Qillathe", "Silaqui",
				"Valathe", "Xanaphia" };

		return getName(gender, female, male);
	}

	private static String gnome(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] female = { "Bimpnotti", "Caramip", "Duvamil", "Ellywick", "Ellyjobell", "Tupfschleife", "Mardnab",
				"Roywyn", "Shamil", "Wegwocket" };
		String[] male = { "Boddynock", "Dimbel", "Fonkin", "Glim", "Gerbo", "Jebeddo", "Namfudel", "Rundar", "Siebo",
				"Zuck" };

		return getName(gender, female, male);
	}

	private static String halfelf(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] female = { "Olvyre", "Iropisys", "Unaseris", "Howaris", "Jilviel", "Loradove", "Ileaerys", "Armythe",
				"Qizira", "Quetheris" };
		String[] male = { "Rilumin", "Cravoril", "Quogretor", "Kevnan", "Orisariph", "Zanhomin", "Zanhomin", "Wilynor",
				"Halcoril", "Davreak" };

		return getName(gender, female, male);
	}

	private static String halfling(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] female = { "Amaryllis", "Charmine", "Cora", "Euphemia", "Jilian", "Lavinia", "Merla", "Portia",
				"Seraphina", "Verna" };
		String[] male = { "Alton", "Beau", "Kayd", "Eldon", "Garret", "Leyl", "Milo", "Osborn", "Rosco", "Wellby" };

		return getName(gender, female, male);
	}

	private static String halfork(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] female = { "Baggi", "Emen", "Engong", "Myev", "Neega", "Ovak", "Onka", "Schautha", "Vola", "Volen" };
		String[] male = { "Dentsch", "Feng", "Gell", "Henk", "Holg", "Imsh", "Keth", "Ront", "Shump", "Thokk" };

		return getName(gender, female, male);
	}

	private static String human(@NonNull final Gender gender) {
		requireNonNull(gender, "Gender may not be null.");
		String[] surname = { "Alfaran", "Darben", "Bierwirth", "Termoil", "Bodiak", "Perensen", "Arsteener", "Zandor" };
		String[] female = { "Allia", "Celien", "Sabthatrida", "Vertia", "Linlia", "Ilwyn", "Lintha" };
		String[] male = { "Erich", "Ron", "Nius", "Xanbert", "Kucas", "Linus", "Alco", "Torbrecht", "Sigman",
				"Norron" };

		return getName(gender, female, male) + " " + surname[R.nextInt(surname.length)];
	}
}