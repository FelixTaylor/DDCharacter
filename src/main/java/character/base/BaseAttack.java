/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.base;

import character.DDCharacter;
import character.components.Clazz;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * <p>
 * <i>It is recommended to <b>NOT</b> use this class directly because they can
 * cause unexpected errors.</i>
 *
 * <p>
 * The <code>BaseAttack</code> class defines the fighting strength and skill of
 * a {@link DDCharacter}.
 *
 * @see DDCharacter
 * 
 * @author Felix Taylor
 * @since 1.0.0
 * @version 2.0.1
 */
public class BaseAttack {
	private BaseAttack() {
		// Private constructor so object is not initializable
	}

	/**
	 * <p>
	 * The <code>calculateAttacks(int, int)</code> method calculates the amount of
	 * attacks the character can perform in every combat round.
	 *
	 * @param level The level of the character
	 * @param interval The interval at which the attacks are increased.
	 * 
	 * @return The amount of attacks the character can use each combat round
	 * 
	 * @see DDCharacter
	 * @see #getAttacks(Clazz, int)
	 *
	 * @since 0.1.0
	 */
	private static int calculateAttacks(final int level, final int interval) {
		int attacks = 1;
		int count = 1;

		// TODO: Refactor
		for (int i = 1; i < level; i++) {
			if (count == interval) {
				attacks++;
				count = 0;
			}
			count++;
		}
		return attacks;
	}

	/**
	 * <p>
	 * The <code>calculateBaseAttackBonus(Clazz, int)</code> method calculates the
	 * base attack bonus of the character. The base attack bonus is further
	 * used to calculate the ranged and melee attack of the character.
	 *
	 * @param clazz The Clazz of the character
	 * @param level The level of the character
	 *
	 * @return integer represents the baseAttackBonus of the character
	 *
	 * @since 0.1.0
	 */
	public static int getBaseAttackBonus(@NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null");
		int bab = 0;
		switch (clazz) {
			case BARBARIAN:
			case FIGHTER:
			case PALADIN:
			case RANGER:
				bab = level;
				break;
			case BARD:
			case CLERIC:
			case DRUID:
			case MONK:
			case ROGUE:
				// TODO: Refactor
				for (int i = 0; i < level; i++) {
					if (i % 4 != 0) {
						bab++;
					}
				}

				break;
			default:
				// TODO: Refactor
				for (int i = 1; i <= level; i++) {
					if (i % 2 == 0) {
						bab++;
					}
				}
				break;
		}
		return bab;
	}

	/**
	 * <p>
	 * The <code>getAttacks()</code> method returns the amount of attacks a
	 * character can perform each combat round.
	 *
	 * @return integer representing the attacks of the character
	 *
	 * @see #calculateAttacks(int, int)
	 *
	 * @since 0.1.0
	 */
	public static int getAttacks(@NonNull final Clazz clazz, final int level) {
		requireNonNull(clazz, "Clazz may not be null.");
		int interval;
		switch (clazz) {
			case BARBARIAN:
			case FIGHTER:
			case PALADIN:
			case RANGER:
				interval = 5;
				break;
			case BARD:
			case CLERIC:
			case DRUID:
			case MONK:
			case ROGUE:
				interval = 7;
				break;
			default:
				interval = 11;
				break;
		}
		return calculateAttacks(level, interval);
	}

	/**
	 * @return int representing the melee modifier which is used to calculate the
	 *         melee damage of the character.
	 *
	 * @since 0.1.0
	 */
	public static int getMeleeMod(@NonNull final Clazz clazz, @NonNull final Abilities abilities, final int level) {
		requireNonNull(clazz, "Clazz may not be null");
		requireNonNull(abilities, "Abilities may not be null.");
		return getBaseAttackBonus(clazz, level) + abilities.getModifier(Abilities.DEX);
	}

	/**
	 * @return int representing the range modifier which is used to calculate the
	 *         ranged damage of the character.
	 *
	 * @since 0.1.0
	 */
	public static int getRangeMod(@NonNull final Clazz clazz,@NonNull final  Abilities abilities, final int level) {
		requireNonNull(clazz, "Clazz may not be null");
		requireNonNull(abilities, "Abilities may not be null.");
		return getBaseAttackBonus(clazz, level) + abilities.getModifier(Abilities.STR);
	}
}
