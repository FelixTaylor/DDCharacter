/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character;

import character.base.*;
import character.base.Disposition.Alignment;
import character.base.Disposition.Attitude;
import character.components.*;
import character.interfaces.*;
import equipment.Armour;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Math.*;
import static java.util.Arrays.stream;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

/**
 * <p>
 * The <code>DDCharacter</code> is the base of a 'Dungeon &amp; Dragons 3.5'
 * character. It is also possible to set some options randomly while others are
 * set manually.
 *
 * @see DDController
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 5.7.1
 */
public class DDCharacter extends Entity
		implements  Meta, Tribe, Leveling, Health, Combat, Skillable, Believe {

	private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	static {
		logger.setLevel(Level.INFO);
	}

	private final UUID uuid;

	/**
	 * <p>
	 * The creationDateInMillis saves the day and time of the character generation,
	 * or at time of saving.
	 *
	 * @see #setCreationDate(long)
	 * @see #getCreationDate()
	 *
	 * @since 0.1.0
	 */
	private long creationDateInMillis;

	/**
	 * The level is used to determine the overall strength of a character. Many
	 * other values depending on the level to be calculated correctly.
	 *
	 * @see #setLevel(int)
	 * @see #getLevel()
	 * @see DDLevel
	 *
	 * @since 0.1.0
	 */
	private final DDLevel level;

	/**
	 * <p>
	 * The hitPoints value defines how much hitPoints the character currently has
	 * and how much hitPoints he has left temporally. If this value falls below zero
	 * the character {@link #isKnockedOut()} and if this value hits minus ten, the
	 * character is dead.
	 *
	 * @see #getMaxHitPoints()
	 * @see #getHitPoints()'
	 * @see #setHitPoints(int)
	 * @see #isAlive()
	 * @see #isKnockedOut()
	 *
	 * @since 0.1.0
	 */
	private int hitPoints;

	/**
	 * <p>
	 * The maxHitPoints value represents the total maxHitPoints the character has. This
	 * value will not decrease, if the character is damaged the {@link #hitPoints}
	 * is lowered.
	 *
	 * @see #getMaxHitPoints()
	 * @see #calculateMaxHitPoints()
	 *
	 * @since 0.1.0
	 */
	private int maxHitPoints;

	/**
	 * <p>
	 * The gender of the character.
	 *
	 * @see Gender
	 * @see #setGender(Gender)
	 * @see #getGender()
	 *
	 * @since 0.1.0
	 */
	private Gender gender;

	/**
	 * <p>
	 * The deity variable is a 'role play' value and not further used by the
	 * character. It may specify how the character interacts with others. Note that
	 * also the value NONE can be selected via the Deity enum.
	 *
	 * @see Deity
	 * @see #setDeity(Deity)
	 * @see #getDeity()
	 *
	 * @since 0.1.0
	 */
	private Deity deity;

	/**
	 * <p>
	 * The alignment of the character. This value is only used in combination with
	 * attitude. This is a 'role play' value and is not further used by the
	 * character.
	 *
	 * @see Disposition
	 * @see #attitude
	 * @see #setDisposition(Alignment, Attitude)
	 * @see Disposition#isValid(Clazz, Deity, Alignment, Attitude)
	 *
	 * @since 0.1.0
	 */
	private Alignment alignment;

	/**
	 * <p>
	 * The attitude of the character. This value is only used in combination with
	 * alignment. This is a 'role play' value and is not further used by the
	 * character.
	 *
	 * @see Disposition
	 * @see #alignment
	 * @see #setDisposition(Alignment, Attitude)
	 * @see Disposition#isValid(Clazz, Deity, Alignment, Attitude)
	 *
	 * @since 0.1.0
	 */
	private Attitude attitude;

	/**
	 * <p>
	 * The <code>skills</code> objects holds the available skills for the character.
	 * Also a value for the max usable skill points and a value representing the
	 * used skill points are stored there.
	 * 
	 * @see Skills
	 * @see #getSkillPoints()
	 *
	 * @since 0.1.0
	 */
	private final Skills skills;

	/**
	 * <p>
	 * The clazz of the character. It is important for the correct calculation of
	 * many other classes.
	 *
	 * @see Clazz
	 * @see #setClazz(Clazz)
	 * @see #getClazz()
	 *
	 * @since 0.1.0
	 */
	private Clazz clazz;

	/**
	 * <p>
	 * The race of the character. It is important for the correct calculation of
	 * many other classes
	 *
	 * @see Race
	 * @see #setRace(Race)
	 * @see #getRace()
	 *
	 * @since 0.1.0
	 */
	private Race race;

	/**
	 * <p>
	 * The abilities of the character are very important to generate many other
	 * values. For example they define the strength or dexterity of a character.
	 *
	 * @see Abilities
	 * @see #getAbilityValue(Object)
	 * @see #getAbilityMod(Object)
	 *
	 * @since 0.1.0
	 */
	private Abilities abilities;

	/**
	 * <p>
	 * Main constructor to initialize a 'Dungeon &amp; Dragons 3.5' character. See
	 * the documentation of the {@link DDCharacter} class to read more about the
	 * character generation.
	 *
	 * @see DDCharacter
	 *
	 * @since 0.1.0
	 */
	public DDCharacter(final int level) {
		this();
		setLevel(level);
	}

	public DDCharacter() {
		setName("DDCharacter");
		this.uuid = UUID.randomUUID();
		this.level = new DDLevel(1);
		this.creationDateInMillis = System.currentTimeMillis();
		this.race = null;
		this.clazz = null;
		this.abilities = new Abilities();
		this.skills = new Skills();
		this.gender = Gender.FEMALE;
		this.alignment = Alignment.NONE;
		this.attitude = Attitude.NONE;
		this.deity = Deity.NONE;
		this.hitPoints = 0;
		this.maxHitPoints = 0;
	}

	public DDCharacter(final int level, final Object... obj) {
		this(level);
		set(obj);
	}

	/**
	 * <p>
	 * The <code>amIARealBoy()</code> method returns true if the character is a
	 * really real boy, false if not.
	 *
	 * @return Boolean : true if the character is a real boy, false if not
	 * @see DDCharacter
	 *
	 * @since 0.1.0
	 */
	public boolean amIARealBoy() {
		return false;
	}

	/**
	 * <p>The <code>addSkillsAndDistributePoints()</code> method sets the skills
	 * available for this character and distributes all skill points to the
	 * accessible skills. This method will fail if the character is not valid.</p>
	 *
	 * @see #isValid()
	 * @see #getSkills()
	 * @see #calculateSkillPoints()
	 *
	 * @since 0.1.0
	 */
	public void addSkillsAndDistributePoints() {
		if (isValid()) {
			calculateSkillPoints();
			if (getSkills().isEmpty()) {
				setSkills();
			}
		}
	}

	/** {@inheritDoc} **/
	@Override
	public void calculateSkillPoints() {
		this.skills.calculateSkillPoints(getRace(), getClazz(), getAbilities(), getLevel());
	}

	/**
	 * <p>
	 * The <code>isValid()</code> method checks if the given values of this
	 * {@link DDCharacter} object are valid to further calculate various attributes
	 * of the character. For example to calculate the {@link Skills} need a valid
	 * {@link Race}, {@link Clazz} and {@link Abilities} in order to be generated.
	 * 
	 * @see DDCharacter
	 * @return True if the character is valid, false if not
	 *
	 * @since 0.1.0
	 */
	public boolean isValid() {
		String out = "";
		boolean isValid = true;

		if (getRace() == null) {
			out += "No race selected!\n";
			isValid = false;
		}

		if (getClazz() == null) {
			out += "No clazz selected!\n";
			isValid = false;
		}

		if (getAbilities() == null) {
			out += "No abilities selected!\n";
			isValid = false;
		}

		if (getLevelObject() == null) {
			out += "No level selected!\n";
			isValid = false;
		}

		if (!isValid) {
			logger.warning(out);
		}

		return isValid;
	}

	/** {@inheritDoc} **/
	@Override
	public void levelUp() {
		if (isValid() && isAlive()) {
			if (isKnockedOut()) {
				logger.warning("Can not level DDCharacter(" + getName() + ") because character is 'knocked out'.");
				return;
			}

			while (canLevelUp()) {
				DDLevel level = getLevelObject();
				int currentExp = level.getExperience();

				setLevel(getLevel() + 1);
				getAbilities().levelUp(getClazz(), level.getLevel() - 1, level.getLevel());
				getLevelObject().setCurrentExperience(currentExp);

				// TODO: I have to remove the distributeAvailableSkillPoints() method call if
				// the user should be able to skill the character individually. A variable could
				// be used to determine if the skill points should be distributed automatically.
				getSkillsObject().distributeAvailableSkillPoints(getLevel());
				logger.log(Level.INFO, getName() + " leveled Up!");
			}
		}
	}

	/**
	 * <p>
	 * The <code>set(Object...)</code> method calls the needed method based on the
	 * given parameters. So you can call
	 * <code>character.set(Clazz.BARBARIAN);</code> to set the clazz of the
	 * character to barbarian or <code>character.set(Race.ELF, Clazz.BARD);</code>
	 * to set the race of the character to elf and the clazz to bard. You can do
	 * that with:
	 * <ul>
	 * <li>{@link Clazz},
	 * <li>{@link Race},
	 * <li>{@link Deity},
	 * <li>{@link Alignment},
	 * <li>{@link Attitude},
	 * <li>{@link Gender} and
	 * <li>{@link Abilities}
	 * </ul>
	 * <p>
	 * Note: In order to set the abilities with this method a race, clazz and a
	 * level have to be selected.
	 *
	 * @param obj Object... : one of the options listed above.
	 * 
	 * @return This DDCharacter object
	 * 
	 * @see #setClazz(Clazz)
	 * @see #setRace(Race)
	 * @see #setAbilities(int[])
	 * @see #setAlignment(Alignment)
	 * @see #setAttitude(Attitude)
	 * @see #setDeity(Deity)
	 * @see #setLevel(int)
	 * @see #setGender(Gender)
	 * @see #setName(String)
	 *
	 * @since 0.1.0
	 */
	public DDCharacter set(final Object... obj) {
		stream(obj)
				.filter(Objects::nonNull)
				.forEach(this::set);
		return this;
	}

	private void set(final Object obj) {
		if (obj instanceof Clazz) {
			setClazz((Clazz) obj);
		} else if (obj instanceof Race) {
			setRace((Race) obj);
		} else if (obj instanceof Deity) {
			setDeity((Deity) obj);
		} else if (obj instanceof Gender) {
			setGender((Gender) obj);
		} else if (obj instanceof int[]) {
			setAbilities((int[]) obj);
		} else if (obj instanceof Alignment) {
			setAlignment((Alignment) obj);
		} else if (obj instanceof Attitude) {
			setAttitude((Attitude) obj);
		} else if (obj instanceof String) {
			setName((String) obj);
		} else {
			logger.warning("Could not set value of type " + obj.getClass().getSimpleName());
		}
	}

	/** {@inheritDoc} **/
	@Override
	public void setAlignment(@NonNull final Alignment alignment) {
		this.alignment = requireNonNull(alignment, "Alignment may not be null.");
	}

	/** {@inheritDoc} **/
	@Override
	public void setAttitude(@NonNull final Attitude attitude) {
		this.attitude = requireNonNull(attitude, "Attitude may not be null.");
	}

	/**
	 * <p>
	 * The <code>setAbilities(int[])</code> method is used to set all abilities of
	 * the character at once. It is important that the input array has a length of 6
	 * otherwise the method will throw an InvalidParameterException.
	 *
	 * <pre>
	 * character.setAbilities(new int[] { 11, 14, 13, 9, 11, 9 });
	 * </pre>
	 *
	 * <b>Note:</b> Since version 1.8.0 this method is private. You can access it by
	 * calling the <code>.set(new int[])</code> method provided by this class.
	 *
	 * @param abilities int[] : the new abilities for the character
	 * @see Abilities
	 * @see #abilities
	 * @see Ability
	 * @see #set(Object...)
	 *
	 * @since 0.1.0
	 */
	private void setAbilities(final int[] abilities) {
		if (this.abilities == null) {
			this.abilities = new Abilities(getRace(), getClazz(), getLevel());
		}

		if (abilities.length != getAbilities().asArray().length) {
			throw new InvalidParameterException(
					"Invalid array length! The length has to be: " + getAbilities().asArray().length);
		}

		this.abilities.set(abilities);
	}

	/** {@inheritDoc} **/
	@Override
	public void setClazz(@NonNull final Clazz clazz) {
		this.clazz = requireNonNull(clazz, "Clazz may not be null.");
	}

	/** {@inheritDoc} **/
	@Override
	public void setRace(@NonNull final Race race) {
		this.race = requireNonNull(race, "Race may not be null.");
	}

	/** {@inheritDoc} **/
	@Override
	public void setSkills() {
		this.skills.setSkills(getClazz(), this.abilities);
	}

	/** {@inheritDoc} **/
	@Override
	public HashMap<String, Skill> getSkills() {
		return this.skills.getSkills();
	}

	/** {@inheritDoc} **/
	@Override
	public Skills getSkillsObject() {
		return this.skills;
	}

	/**
	 * <p>The <code>canBeModified()</code> method returns true if the character
	 * is alive an NOT knocked out and therefor can be modified.
	 *
	 * @return boolean : True if the character can be modified
	 *
	 * @see #isAlive()
	 * @see #isKnockedOut()
	 *
	 * @since 0.1.0
	 */
	public boolean canBeModified() {
		if (!isAlive()) {
			logger.info("The character can NOT be modified because he is dead.");
		} else if (isKnockedOut()) {
			logger.info("The character can NOT be modified because he is knocked out.");
		}

		return isValid() && !isKnockedOut() && isAlive();
	}

	/** {@inheritDoc} **/
	@Override
	public void setDeity(@NonNull final Deity deity) {
		this.deity = requireNonNull(deity, "Deity may not be null.");
	}

	/** {@inheritDoc} **/
	@Override
	public Deity getDeity() {
		return this.deity;
	}

	/**
	 * <p>
	 * The <code>getMaxSkillRank()</code> method returns the maximum rank a
	 * {@link Skill} can reach.
	 * 
	 * @return int max rank of skill
	 *
	 * @since 0.1.0
	 */
	public int getMaxSkillRank() {
		return getLevelObject().getMaxSkillRank();
	}

	/** {@inheritDoc} **/
	@Override
	public Race getRace() {
		return this.race;
	}

	/** {@inheritDoc} **/
	@Override
	public Clazz getClazz() {
		return this.clazz;
	}

	/**
	 * <p>
	 * The <code>getAbilityValue(Object)</code> method returns the value of the
	 * ability corresponding the given parameter.
	 *
	 * <pre>
	 * {@literal //} DDCharacter character = new DDCharacter();
	 * Ability strength = character.getAbility(Abilities.STR); // or "strength" or 0
	 * </pre>
	 *
	 * @see Abilities
	 * @see Abilities#getValue(Object)
	 * @param o : can be an Integer (0-5) or a String representing the name of the
	 *          ability. Valid options are:
	 *          <ul>
	 *          <li>0, 'str'
	 *          <li>1, 'dex'
	 *          <li>2, 'con'
	 *          <li>3, 'int'
	 *          <li>4, 'wis'
	 *          <li>5, 'cha'
	 *          </ul>
	 * @return Integer : the value of the selected ability
	 *
	 * @since 0.1.0
	 */
	public int getAbilityValue(@NonNull final Object o) {
		requireNonNull(o, "Given object may not be null.");
		return this.abilities.getValue(o);
	}

	/**
	 * <p>
	 * The <code>getAbilityMod(Object)</code> method returns the modifier of the
	 * ability corresponding the given parameter. The method will also check the
	 * <code>maxDexterityModifier</code> defined in the characters {@link Armour} if
	 * the modifier of dexterity is requested. Valid options are:
	 * <ul>
	 * <li>0, 'str'
	 * <li>1, 'dex'
	 * <li>2, 'con'
	 * <li>3, 'int'
	 * <li>4, 'wis'
	 * <li>5, 'cha'
	 * </ul>
	 *
	 * @see Abilities
	 * @see Armour
	 * @see Abilities#getModifier(Object)
	 * @see Armour#getMaxDexModifier()
	 *
	 * @return Integer: the modifier of the selected ability
	 * @param o Object: Can be an Integer (0-5) or a String representing the name of
	 *          the ability. Use the static strings provided by the
	 *          {@link Abilities} class.
	 *
	 * @since 0.1.0
	 */
	public int getAbilityMod(@NonNull final Object o) {
		requireNonNull(o, "Given object may not be null.");
		return this.abilities.getModifier(o);
	}

	/**
	 * <p>
	 * The <code>getAbilities()</code> method returns the abilities of the
	 * character.
	 *
	 * @see Abilities
	 * @see Ability
	 * @see #abilities
	 * @return The abilities of the character
	 *
	 * @since 0.1.0
	 */
	public Abilities getAbilities() {
		return this.abilities;
	}

	/**
	 * <p>
	 * The <code>getTalentPoints()</code> method returns the amount of points usable
	 * by the character to select talents.
	 *
	 * @return int : the amount of selectable talents
	 *
	 * @since 0.1.0
	 */
	public int getTalentPoints() {
		if (isNull(getRace())) {
			return 0;
		}

		return (getRace().equals(Race.HUMAN) ? 2 : 1) + getLevel() / 3;
	}

	/** {@inheritDoc} **/
	@Override
	public int getMaxHitPoints() {
		return this.maxHitPoints;
	}

	/** {@inheritDoc} **/
	@Override
	public void calculateMaxHitPoints() {
		final int conMod = getAbilityMod(Abilities.CON);
		int modifier;
		int hp = 0;

		switch (getClazz()) {
			case MAGE:
			case WARLOCK:
				modifier = 4;
				break;
			case BARD:
			case ROGUE:
				modifier = 6;
				break;
			case FIGHTER:
			case PALADIN:
				modifier = 10;
				break;
			case BARBARIAN:
				modifier = 12;
				break;
			default:
				modifier = 8;
				break;
		}

		hp += modifier + conMod;
		final int level = getLevel();
		if (level > 1) {

			for (int i = 2; i <= level; i++) {
				hp += new Random().nextInt(modifier) + 1;
				hp += conMod;
			}
		}

		this.maxHitPoints = hp;
	}

	/** {@inheritDoc} **/
	@Override
	public void initHitPoints() {
		calculateMaxHitPoints();
		setHitPoints(getMaxHitPoints());
	}

	/** {@inheritDoc} **/
	@Override
	public int getHitPoints() {
		return this.hitPoints;
	}

	/** {@inheritDoc} **/
	@Override
	public void setHitPoints(final int hp) {
		this.hitPoints = max(-10, min(getMaxHitPoints(), hp));
	}

	/** {@inheritDoc} **/
	@Override
	public void addHitPoints(final int hp) {
		setHitPoints(getHitPoints() + abs(hp));
	}

	/** {@inheritDoc} **/
	@Override
	public void reduceHitPoints(final int hp) {
		setHitPoints(getHitPoints() - abs(hp));
	}

	/** {@inheritDoc} **/
	@Override
	public boolean isAlive() {
		return getHitPoints() > -10;
	}

	/** {@inheritDoc} **/
	@Override
	public boolean isKnockedOut() {
		return getHitPoints() <= 0;
	}

	/** {@inheritDoc} **/
	@Override
	public long getCreationDate() {
		return this.creationDateInMillis;
	}

	/** {@inheritDoc} **/
	@Override
	public void setCreationDate(long timeInMillis) {
		this.creationDateInMillis = timeInMillis;
	}

	/** {@inheritDoc} **/
	@Override
	public UUID getUUID() {
		return this.uuid;
	}

	/** {@inheritDoc} **/
	@Override
	public void setLevel(final int level) {
		this.level.setLevel(level);
	}

	/** {@inheritDoc} **/
	@Override
	public void setExperience(final int exp) {
		this.level.setCurrentExperience(exp);
	}

	/** {@inheritDoc} **/
	@Override
	public void addExperience(final int exp) {
		this.level.addExperience(exp);
	}

	/** {@inheritDoc} **/
	@Override
	public boolean canLevelUp() {
		return getExperience() >= this.level.getRequiredExpForNextLevel();
	}

	/** {@inheritDoc} **/
	@Override
	public int getExperience() {
		return this.level.getExperience();
	}

	/** {@inheritDoc} **/
	@Override
	public int getLevel() {
		return this.level.getLevel();
	}

	/** {@inheritDoc} **/
	@Override
	public DDLevel getLevelObject() {
		return this.level;
	}

	/** {@inheritDoc} **/
	@Override
	public void setDisposition(@NonNull final Alignment alignment, @NonNull final Attitude attitude) {
		requireNonNull(alignment, "Alignment may not be null.");
		requireNonNull(attitude, "Attitude may not be null");

		if (Disposition.isValid(this.clazz, getDeity(), alignment, attitude)) {
			this.alignment = alignment;
			this.attitude = attitude;
		} else {
			logger.warning("DDCharacter.setDisposition(" + alignment + "," + attitude
					+ "): The combination is not valid. The disposition remains at:" + getAlignment() + ", "
					+ getAttitude());
		}
	}

	/** {@inheritDoc} **/
	@Override
	public Alignment getAlignment() {
		return this.alignment;
	}

	/** {@inheritDoc} **/
	@Override
	public Attitude getAttitude() {
		return this.attitude;
	}

	/**
	 * <p>
	 * The <code>setGender()</code> method sets the new gender for this character.
	 * <p>
	 * Since version 1.5.0 this method is private. You can access it by calling
	 * <code>character.set(Gender.FEMALE);</code>
	 *
	 * @see Gender
	 * @see #set(Object...)
	 * @see #getGender()
	 * @param gender: the gender of the character
	 *
	 * @since 0.1.0
	 */
	private void setGender(@NonNull final Gender gender) {
		this.gender = requireNonNull(gender, "Gender may not be null.");
	}

	/**
	 * <p>
	 * The <code>getGender()</code> method returns the gender of the current
	 * {@link DDCharacter}.
	 *
	 * @return Gender: the gender of the character
	 * @see #gender
	 * @see #set(Object...)
	 * @see Gender
	 *
	 * @since 0.1.0
	 */
	public Gender getGender() {
		return this.gender;
	}

	/** {@inheritDoc} **/
	@Override
	public int getBaseAttackBonus() {
		return isValid() ? BaseAttack.getBaseAttackBonus(getClazz(), getLevel()) : 0;
	}

	/** {@inheritDoc} **/
	@Override
	public int getMeleeMod() {
		return isValid() ? BaseAttack.getMeleeMod(getClazz(), getAbilities(), getLevel()) : 0;
	}

	/** {@inheritDoc} **/
	@Override
	public int getRangeMod() {
		return isValid() ? BaseAttack.getRangeMod(getClazz(), getAbilities(), getLevel()) : 0;
	}

	/** {@inheritDoc} **/
	@Override
	public int getAttacks() {
		return BaseAttack.getAttacks(getClazz(), getLevel());
	}

	/** {@inheritDoc} **/
	@Override
	public int getFortitude() {
		return SavingThrows.getFortitude(getClazz(), getAbilities(), getLevel());
	}

	/** {@inheritDoc} **/
	@Override
	public int getWill() {
		return SavingThrows.getWill(getClazz(), getAbilities(), getLevel());
	}

	/** {@inheritDoc} **/
	@Override
	public int getReflex() {
		return SavingThrows.getReflex(getClazz(), getAbilities(), getLevel());
	}

	/** {@inheritDoc} **/
	@Override
	public int getSkillPoints() {
		return this.skills.getSkillPoints();
	}

	@Override
	public String toString() {
		StringBuilder out = new StringBuilder(getName());
		String delimiter = ", ";

		out.append(" [ ");
		out.append(getId()).append(delimiter);
		out.append(getRace().getName()).append(delimiter);
		out.append(getClazz().getName()).append(delimiter);
		out.append(getLevel()).append(delimiter);

		for (int i = 0; i < getAbilities().asArray().length; i++) {
			out.append(getAbilityValue(i)).append(" ");
		}
		return out.append("]").toString();
	}
}
