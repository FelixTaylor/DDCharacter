/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */

package character.components;

import java.util.Random;

/**
 * <p>
 * The <code>Weight</code> enum holds all available weights for each race.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.3
 */
public enum Weight {
	DWARF(90, 15), ELF(60, 15),
	GNOME(30, 10), HALFELF(70, 15),
	HALFLING(20, 10), HALFORK(100, 15),
	HUMAN(80, 10);

	/**
	 * <p>
	 * The <code>mean</code> defines the average weight of the race. This value is
	 * used by the {@link #randomWeight()} method to calculate a new random weight
	 * for the character.
	 * 
	 * @see #randomWeight()
	 *
	 * @since 0.1.0
	 */
	private final double mean;

	/**
	 * <p>
	 * The <code>deviation</code> of the average race weight. This value is used by
	 * the {@link #randomWeight()} method to calculate a new random weight for the
	 * character.
	 * 
	 * @see #randomWeight()
	 *
	 * @since 0.1.0
	 */
	private final double deviation;

	Weight(final double mean, final double deviation) {
		this.deviation = deviation;
		this.mean = mean;
	}

	/**
	 * <p>
	 * The <code>randomWeight()</code> method generates a random weight for the
	 * current race and returns it.
	 * 
	 * @return Double: The weight for the current race
	 * 
	 * @see Weight
	 *
	 * @since 0.1.0
	 */
	public double randomWeight() {
		return this.mean + this.deviation * new Random().nextGaussian();
	}

}
