/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;
import character.base.Abilities;

/**
 * <p>
 * The class <code>Clazz</code> contains all available classes which can be
 * assigned to an character. The Clazz also holds a list of the clazzes
 * preferred {@link Abilities}. The {@link #getPriorities()} method is used to
 * skill the character more clever.
 * 
 * <p>
 * The full list of available <code>Clazz</code>:
 * <ul>
 * <li>BARBARIAN
 * <li>BARD
 * <li>CLERIC
 * <li>DRUID
 * <li>FIGHTER
 * <li>MAGE
 * <li>MONK
 * <li>PALADIN
 * <li>RANGER
 * <li>ROGUE
 * <li>WARLOCK
 * </ul>
 * 
 * @see DDCharacter#set(Object...)
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.2.5
 */
public enum Clazz {

	BARBARIAN("Barbarian", new String[] { "cha", "wis", "int", "con", "str", "dex" }),
	BARD("Bard", new String[] { "str", "wis", "int", "dex", "con", "cha" }),
	CLERIC("Cleric", new String[] { "cha", "int", "dex", "str", "con", "wis" }),
	DRUID("Druid", new String[] { "cha", "int", "str", "con", "dex", "wis" }),
	FIGHTER("Fighter", new String[] { "cha", "wis", "int", "dex", "con", "str" }),
	MAGE("Mage", new String[] { "str", "cha", "dex", "wis", "con", "int" }),
	MONK("Monk", new String[] { "cha", "con", "int", "str", "dex", "wis" }),
	PALADIN("Paladin", new String[] { "int", "dex", "con", "str", "wis", "cha" }),
	RANGER("Ranger", new String[] { "cha", "int", "con", "str", "wis", "dex" }),
	ROGUE("Rogue", new String[] { "cha", "str", "wis", "con", "int", "dex" }),
	WARLOCK("Warlock", new String[] { "str", "dex", "wis", "int", "con", "cha" });

	/**
	 * <p>
	 * The name of the <code>Clazz</code> like: 'Barbarian', 'Fighter', 'Paladin',
	 * 'Rogue' or 'Warlock' to name a few.
	 *
	 * @see Clazz
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private final String name;

	/**
	 * <p>
	 * The <code>priorities</code> array contains all <code>Abilities</code> ordered
	 * from lowest to highest priority.
	 *
	 * @see Clazz
	 * @see #getPriorities()
	 *
	 * @since 0.1.0
	 */
	private final String[] priorities;

	/**
	 * <p>
	 * The default constructor initializes a new <code>Clazz</code> object and sets
	 * its predefined values. The constructor is private because it can cause
	 * unexpected errors if creating a new clazz.
	 *
	 * @param name     The name of the clazz
	 * @param priority List containing all abilities ordered from lowest to highest.
	 * 
	 * @see Clazz
	 *
	 * @since 0.1.0
	 */
	Clazz(final String name, final String[] priority) {
		this.name = name;
		this.priorities = priority;
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of the clazz.
	 *
	 * @return The name of the clazz
	 * 
	 * @see Clazz
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * <p>
	 * The <code>getPriorities()</code> method returns a list containing all
	 * abilities ordered from lowest to highest priority.
	 *
	 * @return Priority list including all abilities.
	 * 
	 * @see Clazz
	 * @see #priorities
	 *
	 * @since 0.1.0
	 */
	public String[] getPriorities() {
		return this.priorities;
	}
}
