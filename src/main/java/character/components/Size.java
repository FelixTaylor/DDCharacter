/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import java.util.Objects;
import java.util.Random;

import character.DDCharacter;
import equipment.Armour;
import equipment.Weapon;
import equipment.base.Category;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>
 * The <code>Size</code> enum holds all available sizes for each race. This enum
 * is also used to generate a random size by calling the {@link #randomSize()}
 * method. The sizes provided by this enum only apply for {@link Race}s,
 * <b>NOT</b> for {@link Armour}s or {@link Weapon}s. For equipment the Enum
 * {@link Category} is used.
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.7
 *
 * @see Category
 * @see DDCharacter
 */
public enum Size {

	DWARF("Medium", 1.25, 0.15),
	ELF("Medium", 1.40, 0.15),
	GNOME("Small", 1.00, 0.15),
	HALFELF("Medium", 1.65, 0.15),
	HALFLING("Small", 0.90, 0.10),
	HALFORK("Medium", 1.75, 0.15),
	HUMAN("Medium", 1.65, 0.15);

	/**
	 * <p>
	 * The <code>category</code> of the size, like 'Small', 'Medium' or 'Large'
	 * 
	 * @see #getCategory()
	 * @see #compareTo(Category)
	 *
	 * @since 0.1.0
	 */
	private final String category;

	/**
	 * <p>
	 * The <code>mean</code> defines the average size of the race. This value is
	 * used by the {@link #randomSize()} method to calculate a new random size for
	 * the character.
	 * 
	 * @see #randomSize()
	 *
	 * @since 0.1.0
	 */
	private final double mean;

	/**
	 * <p>
	 * The <code>deviation</code> of the average race size. This value is used by
	 * the {@link #randomSize()} method to calculate a new random size for the
	 * character.
	 * 
	 * @see #randomSize()
	 *
	 * @since 0.1.0
	 */
	private final double deviation;

	/**
	 * <p>
	 * The default constructor initializes a new size. The constructor is private
	 * because all valid sizes are available by this enum.
	 * 
	 * @param category a String representation of the size
	 * @param mean the default mean of the size
	 * @param deviation the deviation added to the mean to calculate the actual size
	 *
	 * @since 0.1.0
	 */
	Size(final String category, final double mean, final double deviation) {
		this.category = category;
		this.mean = mean;
		this.deviation = deviation;
	}

	/**
	 * <p>
	 * The <code>getCategory()</code> method returns a string representation of the
	 * size category of this race.
	 * 
	 * @return string representation of the size category
	 * 
	 * @see #category
	 * @see #compareTo(Category)
	 *
	 * @since 0.1.0
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * <p>
	 * The <code>compareTo(Category)</code> method can be used to compare the size
	 * category of the race with the size category of equipment. This is important
	 * because those values have to match if the character wants to equip an
	 * {@link Armour} or use a {@link Weapon}.
	 * 
	 * @param category Category of the weapon or armour
	 * 
	 * @return True if the categories match, false if not
	 * 
	 * @see #category
	 * @see #getCategory()
	 *
	 * @since 0.1.0
	 */
	public boolean compareTo(@NonNull final Category category) {
		Objects.requireNonNull(category, "Category may not be null.");
		return getCategory().equals(category.getCategory());
	}

	/**
	 * <p>
	 * The <code>randomSize()</code> method generates a random size for the current
	 * race and returns it.
	 * 
	 * @return Double: The size for the current race
	 * 
	 * @see Size
	 *
	 * @since 0.1.0
	 */
	public double randomSize() {
		return this.mean + this.deviation * new Random().nextGaussian();
	}
}