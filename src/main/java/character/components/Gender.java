/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;

/**
 * <p>
 * The <code>Gender</code> enum contains all genders a character can have. At
 * the current state there are three different types of gender,
 * <code> FEMALE, MALE, NONE</code> but I want to add more.
 *
 * @see DDCharacter#set(Object...)
 * @see DDCharacter#getGender()
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.2.6
 */
public enum Gender {
	FEMALE("Female"), MALE("Male"), QUEER("Queer"), NONE("None");

	/**
	 * <p>
	 * The name or identifier of the gender.
	 * 
	 * @see Gender
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private final String name;

	Gender(final String name) {
		this.name = name;
	}

	/**
	 * <p>
	 * The <code>getName()</code> returns the name of the gender.
	 * 
	 * @return The name (or identifier) of the gender
	 * 
	 * @see Gender
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}
}
