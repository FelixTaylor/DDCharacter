/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;
import character.base.Disposition;
import character.base.Disposition.Alignment;
import character.base.Disposition.Attitude;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>
 * The <code>Deity</code> enum contains every deity a character can believe in.
 * The selected deity has effect on the random selected {@link Disposition} of
 * the character.
 * 
 * @see DDCharacter#getDeity()
 * @see Disposition
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.3.4
 */
public enum Deity {
	BOCCOB("Boccob", Alignment.NEUTRAL, Attitude.NEUTRAL, "God of magic, arcane knowledge, balance and foresight."),
	CORELLON_LARETHIAN("Corellon Larethian", Alignment.CHAOTIC, Attitude.GOOD, "God of elves, magic, music, and arts (also a demi-human power)."),
	EHLONNA("Ehlonna", Alignment.NEUTRAL, Attitude.GOOD, "Goddess of forests, woodlands, flora & fauna, and fertility."),
	ERYTHNUL("Erythnul", Alignment.CHAOTIC, Attitude.EVIL, "God of hate, envy, malice, panic, ugliness, and slaughter."),
	FHARLANGHN("Fharlanghn", Alignment.NEUTRAL, Attitude.NEUTRAL, "God of horizons, distance, travel, and roads."),
	GARL_GLITTERGOLD("Garl Glittergold", Alignment.NEUTRAL, Attitude.GOOD, "God of gnomes, humor, and gemcutting (also a demi-human power)."),
	GRUMSH("Grummsch", Alignment.CHAOTIC, Attitude.EVIL, "God of orcs (also a monster power)."),
	HEIRONEOUS("Heironeous", Alignment.RIGHTEOUS, Attitude.GOOD, "God of chivalry, justice, honor, war, daring, and valor."),
	HEXTOR("Hextor", Alignment.RIGHTEOUS, Attitude.EVIL, "God of war, discord, massacres, conflict, fitness, and tyranny."),
	KORD("Kord", Alignment.CHAOTIC, Attitude.GOOD, "God of athletics, sports, brawling, strength, and courage."),
	MORADIN("Moradin", Alignment.RIGHTEOUS, Attitude.GOOD, "God of dwarfs (also a demi-human power)."),
	NERULL("Nerull", Alignment.NEUTRAL, Attitude.EVIL, "God of death, darkness, murder and the underworld."),
	OBAD_HAI("Obad-Hai", Alignment.NEUTRAL, Attitude.NEUTRAL, "God of nature, freedom, hunting, and beasts."),
	OLIDAMMARA("Olidammara", Alignment.CHAOTIC, Attitude.NEUTRAL, "God of music, revels, wine, rogues, humor, and tricks."),
	PELOR("Pelor", Alignment.NEUTRAL, Attitude.GOOD, "God of sun, light, strength and healing. More humans worship Pelor than any other deity."),
	ST_CUTHBERT("St. Cuthbert", Alignment.RIGHTEOUS, Attitude.NEUTRAL, "God of common sense, wisdom, zeal, honesty, truth, and discipline."),
	WEE_JAS("Wee Jas", Alignment.RIGHTEOUS, Attitude.NEUTRAL, "Goddess of magic, death, vanity, and law."),
	VECNA("Vecna", Alignment.RIGHTEOUS, Attitude.GOOD, "God of destructive and evil secrets."),
	YONDALLA("Yondalla", Alignment.RIGHTEOUS, Attitude.GOOD, "Goddess of halflings (also a demi-human power)."),
	NONE("None", Alignment.NEUTRAL, Attitude.NEUTRAL, "No god selected.");

	/**
	 * <p>
	 * The name of the {@link Deity}
	 *
	 * @since 0.1.0
	 */
	private final String name;

	/**
	 * <p>
	 * The <code>description</code> variable holds a short info text about the
	 * {@link Deity}.
	 *
	 * @since 0.1.0
	 */
	private final String description;

	/**
	 * <p>
	 * The <code>alignment</code> of the deity.
	 * 
	 * @see Disposition
	 * @see #getAlignment()
	 *
	 * @since 0.1.0
	 */
	private final Alignment alignment;

	/**
	 * <p>
	 * The <code>attitude</code> of the deity.
	 * 
	 * @see Disposition
	 * @see #getAttitude()
	 *
	 * @since 0.1.0
	 */
	private final Attitude attitude;

	/**
	 * <p>
	 * The default {@link Deity} constructor.
	 * 
	 * @param name        The name of the Deity
	 * @param description Short description of the Deity
	 *
	 * @since 0.1.0
	 */
	Deity(@NonNull final String name, @NonNull final Alignment alignment, @NonNull final Attitude attitude,
		  final String description) {
		this.description = description;
		this.alignment = alignment;
		this.attitude = attitude;
		this.name = name;
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of the {@link Deity}.
	 *
	 * @return The name of the deity
	 * 
	 * @see Deity
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * <p>
	 * The <code>getDescription()</code> method returns a short description of the
	 * {@link Deity}.
	 *
	 * @return The description of the deity
	 * 
	 * @see Deity
	 * @see #description
	 *
	 * @since 0.1.0
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * <p>
	 * The <code>getAlignment()</code> method returns the alignment of the current
	 * {@link Deity}.
	 * 
	 * @return Alignment
	 *
	 * @since 0.1.0
	 */
	public Alignment getAlignment() {
		return alignment;
	}

	/**
	 * <p>
	 * The <code>getAttitude()</code> method returns the attidude of the current
	 * {@link Deity}.
	 * 
	 * @return Attitude
	 *
	 * @since 0.1.0
	 */
	public Attitude getAttitude() {
		return attitude;
	}
}
