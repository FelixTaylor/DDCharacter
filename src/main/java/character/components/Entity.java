/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import org.checkerframework.checker.nullness.qual.NonNull;
import utils.Utils;

import java.util.Objects;

import static java.util.Objects.*;

/**
 * <p>
 * A <code>Entity</code> is the base upon other classes can be build. Many other
 * classes in the project containing the values id and name. Those classes can
 * use <code>Entity</code> to get the correct functionality.
 *
 * @author FelixTaylor
 * @since 0.1.0
 * @version 1.1.2
 */
public abstract class Entity {

	/**
	 * <p>
	 * The <code>id</code> of the entity.
	 * 
	 * @see Entity
	 * @see #setId(long)
	 * @see #getId()
	 *
	 * @since 0.1.0
	 */
	private long id;

	/**
	 * <p>
	 * The <code>name</code> of the entity.
	 * 
	 * @see Entity
	 * @see #setName(String)
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private String name;

	public Entity() {
		this.id = -1;
		this.name = "New Entity";
	}

	/**
	 * <p>
	 * The <code>setId(long)</code> method sets a new id for the
	 * <code>Entity</code>.
	 * 
	 * @param id The new id of the <code>Entity</code>
	 * 
	 * @see Entity
	 * @see #getId()
	 * @see #id
	 *
	 * @since 0.1.0
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * <p>
	 * The <code>getId()</code> method returns the id of this <code>Entity</code>.
	 * 
	 * @return The id of the <code>Entity</code>.
	 * 
	 * @see Entity
	 * @see #setId(long)
	 * @see #id
	 *
	 * @since 0.1.0
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * <p>
	 * The <code>setName(String)</code> method sets a new name for the
	 * <code>Entity</code>. The given string will be trimmed and multiple whitespace
	 * characters are reduced to one.
	 *
	 * @param name The new name of the <code>Entity</code>
	 * 
	 * @throws IllegalArgumentException if the given value is <code>null</code> or
	 *                                  empty.
	 *
	 * @see Entity
	 * @see #getName()
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public void setName(@NonNull final String name) {
		requireNonNull(name, "Name may not be null.");
		if (name.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid name input. The given parameter is not allowed to be empty.");
		}

		this.name = Utils.trimWhitespace(name);
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of this
	 * <code>Entity</code>.
	 * 
	 * @return The name of the <code>Entity</code>.
	 * 
	 * @see Entity
	 * @see #setName(String)
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}
}