/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;
import character.base.Abilities;
import equipment.Weapon;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>
 * The <code>Race</code> class contains all available races which can be
 * assigned to a character. It also holds a reference for the positive the
 * negative ability and a {@link Size} value. The first ability is the
 * characters strong ability, the second his weak ability.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.6.3
 *
 * @see Ability
 * @see Abilities
 * @see Clazz
 * @see DDCharacter
 */
public enum Race {
	DWARF("Dwarf", Size.DWARF, Weight.DWARF, Abilities.CON, Abilities.CHA),
	ELF("Elf", Size.ELF, Weight.ELF, Abilities.DEX, Abilities.CON),
	GNOME("Gnome", Size.GNOME, Weight.GNOME, Abilities.CON, Abilities.STR),
	HALFELF("Halfelf", Size.HALFELF, Weight.HALFELF, "NONE", "NONE"),
	HALFLING("Halfling", Size.HALFLING, Weight.HALFLING, Abilities.DEX, Abilities.STR),
	HALFORK("Halfork", Size.HALFORK, Weight.HALFORK, Abilities.STR, Abilities.CHA),
	HUMAN("Human", Size.HUMAN, Weight.HUMAN, "NONE", "NONE");

	/**
	 * <p>
	 * The <code>name</code> of the race like 'Dwarf', 'Halfelf', 'Human', I think
	 * you get it =)
	 *
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private final String name;

	/**
	 * <p>
	 * The <code>positiveAbility</code> variable is a string representation of the
	 * preferred {@link Ability} the the race. For example a <code>DWARF</code> has
	 * very good constitution while the <code>HALFORK</code> preferred ability is
	 * strength.
	 * <p>
	 * <b>Note:</b> <code>HALFELF</code>s and <code>Human</code>s are the only two
	 * races which do not have a preferred ability.
	 *
	 * @see #getPositiveAbility()
	 * @see #negativeAbility
	 *
	 * @since 0.1.0
	 */
	private final String positiveAbility;

	/**
	 * <p>
	 * The <code>negativeAbility</code> variable is a string representation of the
	 * undesired {@link Ability} of the race. For example a <code>DWARF</code> has
	 * very weak charisma while the <code>HALFLING</code>s undesired ability is
	 * strength.
	 * <p>
	 * <b>Note:</b> <code>HALFELF</code>s and <code>Human</code>s are the only two
	 * races which do not have a undesired ability.
	 *
	 * @see #getPositiveAbility()
	 * @see #positiveAbility
	 *
	 * @since 0.1.0
	 */
	private final String negativeAbility;

	/**
	 * <p>
	 * The <code>size</code> variable is a representation of the races {@link Size}.
	 * This value is mainly used to calculate the damage in combination with a
	 * {@link Weapon} or to determine if an armour can be equipped by the character.
	 *
	 * @see Size
	 * @see #getSize()
	 *
	 * @since 0.1.0
	 */
	private final Size size;

	/**
	 * <p>
	 * The <code>weight</code> variable is a representation of the races
	 * {@link Weight}.
	 *
	 * @see Weight
	 * @see #getWeight()
	 *
	 * @since 0.1.0
	 */
	private final Weight weight;

	/**
	 * <p>
	 * The <code>Race(String, Size, String, String)</code> constructor initializes a
	 * new Race object an sets the attributes to the given values. This constructor
	 * is private because there is no need to instantiate a new race, all valid
	 * races are provided by this enum.
	 *
	 * @param race     The name of the race
	 * @param size     The size of the race
	 * @param positive A String representation of the desired ability
	 * @param negative A String representation of the undesired ability
	 *
	 * @see Ability
	 * @see Size
	 *
	 * @since 0.1.0
	 */
	 Race(@NonNull final String race, @NonNull final Size size, @NonNull final Weight weight,
		  final String positive, final String negative) {
		this.name = race;
		this.size = size;
		this.weight = weight;
		this.positiveAbility = positive;
		this.negativeAbility = negative;
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of the current race.
	 * 
	 * @return The name of the race
	 * 
	 * @see Race
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * <p>
	 * The <code>getPositiveAbility()</code> method returns the positive or desired
	 * ability of this race. In the character generation process the ability
	 * corresponding to this string value will be increased by two.
	 * 
	 * @return String representation of the desired ability.
	 * 
	 * @see Ability
	 * @see #positiveAbility
	 * @see #getNegativeAbility()
	 *
	 * @since 0.1.0
	 */
	public String getPositiveAbility() {
		return this.positiveAbility;
	}

	/**
	 * <p>
	 * The <code>getNegativeAbility()</code> method returns the negative or
	 * undesired ability of this race. In the character generation process the
	 * ability corresponding to this string value will be decreased by two.
	 * 
	 * @return String representation of the undesired ability
	 * 
	 * @see Ability
	 * @see #negativeAbility
	 * @see #getPositiveAbility()
	 *
	 * @since 0.1.0
	 */
	public String getNegativeAbility() {
		return this.negativeAbility;
	}

	/**
	 * <p>
	 * The <code>getSize()</code> method returns the {@link Size} of the current
	 * race.
	 *
	 * @see Size
	 * @see #size
	 * @return The Size of the race
	 *
	 * @since 0.1.0
	 */
	public Size getSize() {
		return this.size;
	}

	/**
	 * <p>
	 * The <code>getWeight()</code> method returns the {@link Weight} of the current
	 * race.
	 * 
	 * @see Weight
	 * @see #weight
	 * @return The weight of the race
	 *
	 * @since 0.1.0
	 */
	public Weight getWeight() {
		return this.weight;
	}
}
