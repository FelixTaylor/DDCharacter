/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;
import character.base.Skills;

/**
 * <p>
 * The <code>DDLevel</code> class represents a {@link DDCharacter}s level. The
 * level is defined by an integer (the actual level), the minimum required
 * {@link #neededExperience} needed for that level and the
 * {@link #getMaxSkillRank()} which defines how many skill points can be spend
 * on the same {@link Skill}.
 * <p>
 * The name <code>DDLevel</code> was chosen because there are a few other java
 * (native) classes called <code>Level</code>, for example
 * {@link java.util.logging.Level} which is also used by this library.
 * 
 * @see DDCharacter
 * @see Skill
 * 
 * @author FelixTaylor
 * @since 0.1.0
 * @version 1.1.3
 */
public class DDLevel {

	/**
	 * <p>
	 * The <code>Level</code> of the character.
	 * 
	 * @see DDLevel
	 * @see #setLevel(int)
	 * @see #getLevel()
	 *
	 * @since 0.1.0
	 */
	private int level;

	/**
	 * <p>
	 * The <code>experience</code> needed for this level. Note that there is no
	 * <code>setExperience(int)</code> method because the experience is calculated
	 * internally.
	 * 
	 * @see DDLevel
	 * @see #getExperience()
	 * @see #getNeededExperience()
	 *
	 * @since 0.1.0
	 */
	private int neededExperience;

	/**
	 * <p>
	 * The <code>currentExperience</code> of the character. To determine if the
	 * character can level up the {@link #neededExperience} value of the next level
	 * is used.
	 * 
	 * @see DDLevel
	 * @see #getExperience()
	 * @see #getNeededExperience()
	 *
	 * @since 0.1.0
	 */
	private int currentExperience;

	/**
	 * <p>
	 * The <code>DDLevel(int)</code> constructor initializes a new level object.
	 * <p>
	 * <b>Note:</b> There is no max level defined for a character so you could call
	 * <code>new DDLevel(999)</code> and you still (should) get a valid character.
	 * 
	 * @param level The value of the level
	 * 
	 * @see DDLevel
	 * @see #level
	 * @see #neededExperience
	 *
	 * @since 0.1.0
	 */
	public DDLevel(final int level) {
		setLevel(level);
		this.currentExperience = getNeededExperience();
	}

	/**
	 * <p>
	 * The <code>getLevel()</code> method returns the value of the current level.
	 * 
	 * @return The current level
	 * 
	 * @see #level
	 * @see DDLevel
	 *
	 * @since 0.1.0
	 */
	public int getLevel() {
		return this.level;
	}

	/**
	 * <p>
	 * The <code>setLevel(int)</code> method sets the current level to the value of
	 * the given parameter. A value below one (1) is invalid an the method will
	 * throw a {@link IllegalArgumentException}.
	 * <p>
	 * The <code>setExperience()</code> and <code>setMaxSkillRank()</code> methods
	 * are called in that process.
	 * 
	 * @param level int : the new level for the character
	 * 
	 * @throws IllegalArgumentException if the input value is below 1
	 * 
	 * @see DDLevel
	 *
	 * @since 0.1.0
	 */
	public void setLevel(final int level) {
		if (level < 1) {
			throw new IllegalArgumentException(
					"Invalid parameter (" + level + "): The minimum level for a character is 1.");
		}

		this.level = level;
		setNeededExperience();
	}

	/**
	 * <p>
	 * The <code>getNeededExperience()</code> method returns the required
	 * {@link #neededExperience} for the current level. This value <b>does not</b>
	 * represent the experience of the character!
	 * 
	 * @return int : the experience needed for the current level
	 *
	 * @since 0.1.0
	 */
	public int getNeededExperience() {
		return this.neededExperience;
	}

	/**
	 * <p>
	 * The <code>getExperience()</code> method returns the current
	 * {@link #currentExperience} for the character.
	 * 
	 * @return The experience of the character
	 *
	 * @since 0.1.0
	 */
	public int getExperience() {
		return this.currentExperience;
	}

	/**
	 * <p>
	 * The <code>setExperience()</code> method sets the, for this level required
	 * (minimum) {@link #neededExperience}. The method is declared private because
	 * the method is called whenever the {@link #setLevel(int)} method is called.
	 * 
	 * @see DDLevel
	 * @see #neededExperience
	 * @see #getNeededExperience()
	 *
	 * @since 0.1.0
	 */
	private void setNeededExperience() {
		this.neededExperience = 1000 * ((getLevel()-1) * getLevel() / 2);
	}

	/**
	 * <p>
	 * The <code>setCurrentExperience(int)</code> method sets the current experience
	 * of the {@link DDCharacter}.
	 * 
	 * @param exp The experience of the character
	 * 
	 * @see #currentExperience
	 * @see #getExperience()
	 *
	 * @since 0.1.0
	 */
	public void setCurrentExperience(final int exp) {
		this.currentExperience = exp;
	}

	/**
	 * <p>
	 * The <code>addExperience(int)</code> method adds the given value to experience
	 * of the character. If the given value is negative it will be altered to a
	 * positive value.
	 * 
	 * @param exp The experience for the character
	 *
	 * @see #neededExperience
	 * @see #getNeededExperience()
	 *
	 * @since 0.1.0
	 */
	public void addExperience(final int exp) {
		this.currentExperience += Math.abs(exp);
	}

	/**
	 * <p>
	 * The <code>getRequiredExpForNextLevel()</code> method returns the required
	 * {@link #neededExperience} needed for the <b>next level</b>. This method is
	 * used by the {@link DDCharacter#canLevelUp()} method to determine if the
	 * character can be leveled up.
	 * 
	 * @return The required experience for the next level
	 * 
	 * @see DDLevel
	 * @see #neededExperience
	 * @see #getNeededExperience()
	 * @see DDCharacter#canLevelUp()
	 *
	 * v
	 */
	public int getRequiredExpForNextLevel() {
		return new DDLevel(getLevel() + 1).getNeededExperience();
	}

	/**
	 * <p>
	 * The <code>getMaxSkillRank()</code> method returns the the maximum amount of
	 * points which can be spend on a single {@link Skill} of the character.
	 * 
	 * @return The maximal rank of a skill
	 * 
	 * @see DDLevel
	 * @see Skill
	 * @see Skills
	 *
	 * @since 0.1.0
	 */
	public int getMaxSkillRank() {
		return this.level + 3;
	}
}
