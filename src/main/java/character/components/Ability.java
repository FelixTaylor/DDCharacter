/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;
import character.base.Abilities;
import equipment.Armour;
import exceptions.InvalidInputException;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>
 * <i>It is recommended to <b>NOT</b> use this class directly, instead use the
 * {@link Abilities} class or {@link DDCharacter} class.</i>
 * <p>
 * An Ability is defined by a name, a value and a modifier. A character has the
 * abilities STRENGTH, DEXTERITY, CONSTITUTION, WISDOM, CHARISMA.
 *
 * @see Abilities
 * @see Clazz
 * @see DDCharacter
 * 
 * @since 0.1.0
 * @version 4.2.5
 * @author Felix Taylor
 */
public class Ability {

	/**
	 * <p>
	 * The <code>name</code> of the ability. Note that there is no
	 * <code>setName(String)</code> method to prevent the ability to be renamed.
	 * 
	 * @see Ability
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private final String name;

	/**
	 * <p>
	 * The <code>value</code> of the ability. This value also determines the value
	 * of the {@link #mod} value. An average character has ability values somewhere
	 * around 10. The minimum value for an ability is 1 (one).
	 * 
	 * @see Ability
	 * @see #setValue(int)
	 * @see #getValue()
	 *
	 * @since 0.1.0
	 */
	private int value;

	/**
	 * <p>
	 * The <code>mod</code> of the ability. This value is used to calculate various
	 * values while generating the character and also in role-play or while using
	 * skills.
	 * <p>
	 * <b>Note:</b> That this value can be suppressed by the returned value of the
	 * {@link Armour#getMaxDexModifier()} method when calling
	 * {@link DDCharacter#getAbilityMod(Object)}.
	 * 
	 * @see Ability
	 * @see #setValue(int)
	 * @see #getValue()
	 *
	 * @since 0.1.0
	 */
	private int mod;

	/**
	 * <p>
	 * <b>Note:</b> Do NOT create your own abilities. The library only works if the
	 * character has six abilities with those specific names listed over at
	 * {@link Ability}. The only place where this constructor should be called is
	 * the {@link Abilities} class.
	 * <p>
	 * The default constructor initializes a new <code>Ability</code> object, sets
	 * the default values and sets the name to the given value.
	 *
	 * @param name The name of the ability
	 *
	 * @since 0.1.0
	 */
	public Ability(@NonNull final String name) {
		super();
		this.name = name;
		this.value = 1;
		this.mod = -5;
	}

	/**
	 * <p>
	 * The <code>setValue(int)</code> method sets this <code>Ability</code> to the
	 * given value if that value is higher or equal to one and is different to the
	 * last selected ability value.
	 *
	 * @param value The value of the ability
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #getValue()
	 *
	 * @since 0.1.0
	 */
	public void setValue(final int value) throws InvalidInputException {
		if (value > 0 && (this.value != value)) {
			this.value = value;
			calculateMod();
		} else if (value <= 0) {
			throw new InvalidInputException(
					"Invalid parameter value: '" + value + "'. The value has to be greater then 0 (zero).");
		}
	}

	/**
	 * <p>
	 * The <code>calculateMod()</code> method calculates the modifier value of this
	 * <code>Ability<code>. It is private because it is always called if the value
	 * of the ability changes.
	 *
	 * @see Ability
	 * @see Abilities
	 * @see #getMod()
	 *
	 * @since 0.1.0
	 */
	private void calculateMod() {
		int startValue = 0;
		int mod = -5;
		if (getValue() >= 10) {
			startValue = 10;
			mod = 0;
		}
		for (int i = startValue; i <= getValue(); i++) {
			mod += i % 2 == 0 ? 1 : 0;
		}
		this.mod = mod - 1;
	}

	/**
	 * <p>
	 * The <code>getValue()</code> method returns the value of the ability.
	 * 
	 * @return value of the <code>Ability</code>.
	 *
	 * @see Ability
	 * @see Abilities
	 * @see #value
	 *
	 * @since 0.1.0
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of this ability.
	 * 
	 * @return String: The name of the <code>Ability</code>.
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * <p>
	 * The <code>getMod()</code> method returns the modifier of the ability.
	 *
	 * @return The modifier of the <code>Ability</code>.
	 * 
	 * @see Ability
	 * @see Abilities
	 * @see #mod
	 *
	 * @since 0.1.0
	 */
	public int getMod() {
		return this.mod;
	}
}