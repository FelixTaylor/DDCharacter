/**
 * <p>
 * The components package holds various object which are used by other objects
 * like {@link character.base.Abilities Abilities} or
 * {@link character.DDCharacter DDCharacter}. It provides a predefined set of
 * options to choose from to generate a character, like the Race or Clazz.
 * <p>
 * <b>Note:</b> You should use the character located in the
 * <code>character</code> package to generate characters. Using classes from the
 * sub packages can cause unexpected errors which could be hard to debug.
 * <p>
 * You can find the corresponding test files in the
 * <b>test</b>.character.components package.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.2.0
 *
 * @see Ability
 * @see Clazz
 * @see DDLevel
 * @see Deity
 * @see Entity
 * @see Gender
 * @see Race
 * @see Size
 * @see Skill
 */
package character.components;