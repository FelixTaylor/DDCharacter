/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.components;

import character.DDCharacter;
import character.base.Abilities;
import character.base.Skills;
import equipment.Armour;
import exceptions.InvalidInputException;

import java.util.Objects;

/**
 * <p>
 * The <code>Skill</code> class represents a skill which can potentially be used
 * by the character. A skill is defined by a name and the rank. It also stores a
 * reference to the ability which is used to perform this skill.
 *
 * <p>
 * <b>Note:</b> This class does not provide any set-methods (except setRank)
 * because all skills are initialized internally and should not be modified
 * manually.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.2.7
 */
public class Skill extends Entity {

	/**
	 * <p>
	 * The rank of the skill. The rank is added to the modifier value of the
	 * referenced ability to calculate the overall power of this skill. The skill
	 * rank can potentially increase every time the character levels up.
	 *
	 * @see Skill
	 * @see #getRank()
	 * @see #getPower()
	 *
	 * @since 0.1.0
	 */
	private int rank;

	/**
	 * <p>
	 * The ability reference stores the reference to the ability which modifier
	 * value is used to calculate the overall power of this skill.
	 *
	 * @see Skill
	 * @see #getReference()
	 * @see #getPower()
	 *
	 * @since 0.1.0
	 */
	private final Ability reference;

	/**
	 * <p>
	 * The <code>isADefaultSkill</code> attribute indicates if the skill belongs to
	 * the default set of skills each character can use.
	 * 
	 * @see #isDefaultSkill();
	 *
	 * @since 0.1.0
	 */
	private final boolean isADefaultSkill;

	/**
	 * <p>
	 * The default constructor initializes a new skill object and sets the variables
	 * to the given parameters. Do <b>not</b> initialize your own skills because
	 * this will cause various errors.
	 *
	 * @param name           The name of the skill
	 * @param rank           The rank of the skill
	 * @param reference      The {@link Ability} used for this skill
	 * @param isDefaultSkill Flag if this is a default skill
	 *
	 * @since 0.1.0
	 */
	public Skill(final String name, final int rank, final Ability reference, final boolean isDefaultSkill) {
		super();
		setName(name);
		this.rank = rank;
		this.reference = Objects.requireNonNull(reference, "Ability reference may not be null.");
		this.isADefaultSkill = isDefaultSkill;
	}

	/**
	 * <p>
	 * The <code>getPower()</code> methods returns the overall power of this skill.
	 * To calculate that value the rank of the skill is added to the ability
	 * modifier value of the referenced ability.
	 * 
	 * @return The overall power of this skill
	 *
	 * @see Skill
	 * @see #getRank()
	 * @see #rank
	 * @see #getReference()
	 * @see #reference
	 *
	 * @since 0.1.0
	 */
	public int getPower() {
		return getRank() + getReference().getMod();
	}

	/**
	 * <p>
	 * The <code>getRank()</code> method returns the rank of this skill. The minimum
	 * value for a skill is 0 (zero).
	 * 
	 * @return The rank of this sill
	 *
	 * @see Skill
	 * @see #rank
	 * @see #getPower()
	 *
	 * @since 0.1.0
	 */
	public int getRank() {
		return this.rank;
	}

	/**
	 * <p>
	 * The <code>setRank(int)</code> method sets the rank of the skill. The rank is
	 * used to calculate the overall power of this skill.
	 * 
	 * @param rank The rank of the skill
	 * 
	 * @throws InvalidInputException if the given rank value is negative
	 *
	 * @see Skill
	 * @see #rank
	 * @see #getRank()
	 * @see #getPower()
	 *
	 * @since 0.1.0
	 */
	public void setRank(final int rank) throws InvalidInputException {
		if (rank < 0) {
			throw new InvalidInputException("The rank of a skill has to be greater then 0 (zero)");
		}

		this.rank = rank;
	}

	/**
	 * <p>
	 * The <code>addRank()</code> method increases the rank value of this skill by
	 * 1. If you want to set the rank to a specific value use the
	 * <code>setRank(int)</code> method.
	 *
	 * @see Skill
	 * @see #rank
	 * @see #setRank(int)
	 * @see #getRank()
	 * @see #getPower()
	 *
	 * @since 0.1.0
	 */
	public void addRank() {
		this.rank++;
	}

	/**
	 * <p>
	 * The <code>getReference()</code> method returns the ability which is used to
	 * perform this skill.
	 * 
	 * @return The referenced ability which is used to perform this skill
	 *
	 * @see Skill
	 * @see Ability
	 * @see #reference
	 * @see #getPower()
	 *
	 * @since 0.1.0
	 */
	public Ability getReference() {
		return this.reference;
	}

	/**
	 * <p>
	 * The <code>isDefaultSkill()</code> method returns true if the skill is usable
	 * by every character false if not.
	 * 
	 * @return true if the skill a default skill, false if not
	 * 
	 * @see #isADefaultSkill
	 * @see Skill
	 * @see Skills
	 *
	 * @since 0.1.0
	 */
	public boolean isDefaultSkill() {
		return this.isADefaultSkill;
	}

	/**
	 * <p>The <code>usesDexModifier()</code> returns true if this skill uses
	 * the dexterity modifier to calculate its overall power. This is important
	 * because of the {@link Armour#getMaxDexModifier()} method which returns
	 * the highest value possible to add to a skill.
	 * 
	 * @see Armour
	 * @see Armour#getMaxDexModifier()
	 * @see DDCharacter
	 *
	 * @return boolean : true if the skill uses the dexterity modifier for its
	 * calculation.
	 *
	 * @since 0.1.0
	 */
	public boolean usesDexModifier() {
		return getReference().getName().toLowerCase().equals(Abilities.DEX);
	}
}
