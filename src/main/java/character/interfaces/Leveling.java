/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.base.Abilities;
import character.base.Skills;
import character.components.Clazz;
import character.components.DDLevel;
import character.components.Race;

/**
 * <p>The <code>Leveling</code> interface contains all needed method to handle
 * a characters level and experience handling.</p>
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.1
 */
public interface Leveling {

    /**
     * <p>
     * The <code>levelUp</code> method performs all needed actions to level up the
     * character if he/she {@link #canLevelUp()} and if the character
     * {@link character.DDCharacter#isAlive()}. Warning: This action can <b>NOT</b> be undone at the
     * current state of the library.
     * <p>
     * <ul>
     * <li>Remove the ability points which the character gained for his
     * {@link Race}, {@link Clazz} or {@link DDLevel}.
     * <li>Increase the level of the character
     * <li>Re-apply the ability points which the character gains for his race, clazz
     * or level.
     * </ul>
     *
     * @see DDLevel
     * @see Abilities
     * @see Skills
     *
     * @since 0.1.0
     */
    void levelUp();

    /**
     * <p>
     * The <code>setLevel(int)</code> method sets a new level for the current
     * character.
     *
     * @see #getLevel()
     * @see DDLevel
     * @param level int : the level for the character
     *
     * @since 0.1.0
     */
    void setLevel(final int level);

    /**
     * <p>
     * The <code>setExperience(int)</code> method sets the current experience of the
     * character. Most of the time the {@link #addExperience(int)} method is
     * preferred.
     *
     * @param exp The current experience points of the character
     *
     * @see #addExperience(int)
     * @see DDLevel#setCurrentExperience(int)
     *
     * @since 0.1.0
     */
    void setExperience(final int exp);

    /**
     * <p>
     * The <code>getLevel()</code> method returns the current level of the
     * character.
     *
     * @see #setLevel(int)
     * @see DDLevel
     *
     * @return int the level of the character
     *
     * @since 0.1.0
     */
    int getLevel();

    /**
     * <p>
     * The <code>getLevelObject()</code> returns the object representing the
     * characters level. This method is manly used for internal methods.
     *
     * @return int the current level of the character
     *
     * @since 0.1.0
     */
    DDLevel getLevelObject();

    /**
     * <p>
     * The <code>addExperience(int)</code> method adds the given amount of
     * experience points the the characters experience points. Check the
     * {@link DDLevel} enum to see at which point a character levels up.
     *
     * @see #getExperience()
     * @see #setExperience(int)
     * @see #canLevelUp()
     * @see DDLevel
     *
     * @param exp The experience points which are being added
     *
     * @since 0.1.0
     */
    void addExperience(final int exp);

    /**
     * <p>
     * The <code>canLevelUp()</code> method returns <code>true</code> if the
     * character can level up and <code>false</code> if not.
     *
     * @see #getLevel()
     * @see #getExperience()
     * @see #levelUp()
     * @see DDLevel
     * @return boolean : true if the character can level up, false if not
     *
     * @since 0.1.0
     */
    boolean canLevelUp();

    /**
     * <p>
     * The <code>getExperience()</code> method returns the current amount of
     * experience points the character has.
     *
     * @see #setExperience(int)
     * @see #canLevelUp()
     *
     * @return The current amount of experience points
     *
     * @since 0.1.0
     */
    int getExperience();
}
