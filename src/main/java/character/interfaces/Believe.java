/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.DDCharacter;
import character.base.Disposition;
import character.components.Deity;

import static character.base.Disposition.Alignment;
import static character.base.Disposition.Attitude;

/**
 * <p>The <code>Believe</code> interface defines all relevant methods to control
 * the current characters {@link Deity}, {@link Alignment} and
 * {@link Attitude}.</p>
 *
 * @see Deity
 * @see Disposition
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.1
 */
public interface Believe {

    /**
     * <p>
     * The <code>setDisposition(Alignment, Attitude)</code> method validate and if
     * true sets the alignment and attitude of the {@link DDCharacter} to the given
     * values.
     *
     * @param alignment Alignment: the alignment of the character
     * @param attitude  Attitude: the attitude of the character
     * @see Disposition
     *
     * @since 0.1.0
     */
    void setDisposition(Alignment alignment, Attitude attitude);

    /**
     * <p>
     * The <code>getAlignment()</code> method returns the alignment of the current
     * {@link DDCharacter}.
     *
     * @return Alignment: the alignment of the character
     * @see Disposition
     *
     * @since 0.1.0
     */
    Alignment getAlignment();

    /**
     * <p>
     * The <code>setAlignment(Alignment)</code> method sets the new alignment for
     * the character. To conform with the 'Dungeon & Dragons' rules the alignment
     * has to be a valid combination with the {@link Attitude} of the character.
     * These two values are combined to the {@link Disposition} which should
     * correspond with the chosen {@link Deity} of the character.
     * <p>
     * This method is accessible by the {@link DDCharacter#set(Object...)}
     * method of this class.
     *
     * @see Alignment
     * @see Attitude
     * @see Deity
     * @see #getAlignment()
     * @see #getAttitude()
     * @see #getDeity()
     * @param alignment: The new alignment for the character
     *
     * @since 0.1.0
     */
    void setAlignment(Alignment alignment);

    /**
     * <p>
     * The <code>getAttitude()</code> method returns the attitude of the current
     * {@link DDCharacter}.
     *
     * @return Attitude: the attitude of the character
     * @see Disposition
     *
     * @since 0.1.0
     */
    Attitude getAttitude();

    /**
     * <p>
     * The <code>setAttitude(Attitude)</code> method sets the new attitude for the
     * character. To conform with the 'Dungeon & Dragons' rules the attitude has to
     * be a valid combination with the {@link Alignment} of the character. These two
     * values are combined to the {@link Disposition} which should correspond with
     * the chosen {@link Deity} of the character.
     * <p>
     * This method is accessible by the {@link DDCharacter#set(Object...)} method
     * of this class.
     *
     * @see Alignment
     * @see Attitude
     * @see Deity
     * @see #getAlignment()
     * @see #getAttitude()
     * @see #getDeity()
     * @param attitude: The new attitude for the character
     *
     * @since 0.1.0
     */
    void setAttitude(Attitude attitude);

    /**
     * <p>
     * The <code>getDeity()</code> method returns the deity selected by the
     * character.
     *
     * @see #setDeity(Deity)
     * @return Deity : the currently selected deity of the character
     *
     * @since 0.1.0
     */
    Deity getDeity();

    /**
     * <p>
     * The <code>setDeity(Deity)</code> method sets the new deity for this
     * character. Use the <code>Deity</code> to select a new deity.
     *
     * <pre>
     * {@literal //} Set 'obad hai' as new deity
     * character.setDeity(Deity.OBAD_HAI);
     * </pre>
     * <p>
     * Since version 1.5.0 this method is private. You can access it by calling
     * <code>character.set(Object);</code>
     *
     * @see #getDeity()
     * @see DDCharacter#set(Object...)
     * @param deity Deity - the new deity of the character.
     *
     * @since 0.1.0
     */
    void setDeity(final Deity deity);
}
