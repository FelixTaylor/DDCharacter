/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.DDCharacter;
import character.base.BaseAttack;
import character.components.Clazz;

/**
 * <p>The <code>Combat</code> interface defines all relevant methods to control
 * the characters combat related skills like evading or hitting.</p>
 *
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.0
 */
public interface Combat {

    /**
     * <p>
     * The <code>getBaseAttackBonus()</code> method returns the base attack bonus
     * of the current {@link DDCharacter}.
     *
     * @return int : the base attack bonus of the character
     * @see BaseAttack
     *
     * @since 0.1.0
     */
    int getBaseAttackBonus();

    /**
     * <p>
     * The <code>getMeleeMod()</code> method returns the melee modifier which is
     * used to calculate the damage out put of an attack done by the
     * {@link DDCharacter}.
     * <p>
     *
     * @return int : the melee modifier for the characters attack
     *
     * @since 0.1.0
     */
    int getMeleeMod();

    /**
     * <p>
     * The <code>getRangeMod()</code> method returns the range modifier which is
     * used to calculate the damage out put of an attack done by the
     * {@link DDCharacter}.
     *
     * @return int : the range modifier for the characters attack
     *
     * @since 0.1.0
     */
    int getRangeMod();

    /**
     * <p>
     * The <code>getAttacks()</code> method returns an Integer representing the
     * attacks the character has in each turn.
     *
     * @see BaseAttack
     * @see BaseAttack#getAttacks(Clazz, int)
     * @return Integer : the attacks of the character
     *
     * @since 0.1.0
     */
    int getAttacks();

    /**
     * <p>The <code>getFortitude()</code> method returns the fortitude value of
     * the current character.
     *
     * @return int : the characters fortitude
     *
     * @since 0.1.0
     */
    int getFortitude();

    /**
     * <p>The <code>getWill()</code> method returns the will value of the
     * current character.
     *
     * @return int : the character will
     *
     * @since 0.1.0
     */
    int getWill();

    /**
     * <p>The <code>getReflex()</code> method returns the reflex value of the
     * current character.
     *
     * @return int : the character reflex
     *
     * @since 0.1.0
     */
    int getReflex();

    /**
     * <p>
     * The <code>getAttackModifier()</code> method will always return the value -5.
     * This method is only used to calculate the strength of an attack which is not
     * the first attack of this character.
     *
     * For each attack after the first one the character has to reduce his attack
     * strength by the value of this method.
     *
     * @return integer representing the attack modifier of the character.
     *
     * @since 0.1.0
     */
    default int getAttackModifier() {
        return -5;
    }
}
