/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.DDCharacter;
import character.base.Skills;
import character.components.Clazz;
import character.components.Race;
import character.components.Skill;

import java.util.HashMap;

/**
 * <p>The <code>Skillable</code> interface defines all relevant methods to control
 * the {@link Skills} of the current character.</p>
 *
 * @see Skill
 * @see Skill
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.0
 */
public interface Skillable {

    /**
     * <p>
     * The <code>calculateSkillPoints()</code> method calculates the usable skill
     * points for this character.
     *
     * @see Skills
     * @see #getSkills()
     *
     * @since 0.1.0
     */
    void calculateSkillPoints();

    /**
     * <p>
     * The <code>setSkills()</code> method sets the predefined skills for the
     * character which can be viewed in the <code>Skills</code> class.
     *
     * @see Skills
     * @see Skill
     * @see #getSkills()
     *
     * @since 0.1.0
     */
    void setSkills();

    /**
     * <p>
     * The <code>getSkills()</code> method returns all available skills of this
     * character.
     *
     * @see Skill
     * @see Skills
     * @see #setSkills()
     *
     * @return HashMap : containing all available skills
     *
     * @since 0.1.0
     */
    HashMap<String, Skill> getSkills();

    /**
     * <p>The <code>getSkillsObject()</code> method returns the object which
     * contains all of the skill information of this character.</p>
     *
     * @see #getSkills()
     * @see #setSkills()
     *
     * @return The skills object of this character
     *
     * @since 0.1.0
     */
    Skills getSkillsObject();

    /**
     * <p>
     * The <code>getSkillPoints()</code> method returns the total skill points
     * usable by this character.
     * <p>
     * <b>Note:</b> This value does not represent the accessible skill points. If
     * the character {@link character.DDCharacter#isValid()} these skill points
     * are being distributed. This value represents the total amount of usable skill
     * point for a character at this level.
     *
     * @see Skills
     * @see #calculateSkillPoints()
     * @return int: The amount of skill points
     *
     * @since 0.1.0
     */
    int getSkillPoints();
}
