/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.DDCharacter;
import character.base.Skills;
import character.components.Skill;

import java.util.UUID;

/**
 * <p>The <code>Meta</code> interface defines all relevant methods to control
 * various information which is not relevant to the actual character.</p>
 *
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.1
 */
public interface Meta {

    /**
     * <p>
     * The <code>getCreationDate()</code> method returns the time and date of the
     * creation of this character in milliseconds.
     *
     * @see #setCreationDate(long)
     * @return Long : the time and date of creation
     *
     * v
     */
    long getCreationDate();

    /**
     * <p>
     * <code>setCreationDate(long)</code> sets the creation date of the current
     * {@link DDCharacter} to the input value.
     *
     * @param timeInMillis long representing the current time in millis.
     *
     * @since 0.1.0
     */
    void setCreationDate(final long timeInMillis);

    /**
     * <p>
     * The <code>getUUID()</code> method returns the UUID referencing this
     * {@link DDCharacter}. The UUID is used to make the character unique on a
     * global scale.
     *
     * @return UUID: The uuid of the character
     *
     * @since 0.1.0
     */
    UUID getUUID();
}
