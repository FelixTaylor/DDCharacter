/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.DDCharacter;

/**
 * <p>The <code>Health</code> interface defines all relevant methods to control
 * the hit points of the current character.</p>
 *
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.1
 */
public interface Health {

    /**
     * <p>
     * The <code>getCurrentHitPoints()</code> method returns the current hit points
     * of the character. This value is important while in combat.
     *
     * @return int: the current hit points of the character
     *
     * @see #setHitPoints(int)
     * @see DDCharacter
     *
     * @since 0.1.0
     */
    int getHitPoints();

    /**
     * <p>
     * The <code>setCurrentHitPoints(int)</code> method sets the temporal hitPoints
     * for this character. The minimum value for the temp hitPoints is -10. If a
     * character reaches that value he dies and the {@link #isAlive} value is set to
     * <code>false</code>.
     *
     * <pre>
     * {@literal //} DDCharacter character = new DDCharacter();
     * character.setHitPoints(999);  // valid, will be set to the character max hitpoints
     * character.setHitPoints(0);    // valid
     * character.setHitPoints(-10);  // valid
     * character.setHitPoints(-20);  // invalid, will be set to -10
     * </pre>
     *
     * @param hp The temporal hit points of the character.
     *
     * @see #getMaxHitPoints()
     * @see #addHitPoints(int)
     * @see #reduceHitPoints(int)
     * @see #getHitPoints()
     * @see DDCharacter
     *
     * @since 0.1.0
     */
    void setHitPoints(final int hp);

    /**
     * <p>The <code>getMaxHitPoints()</code> method returns the max hit points
     * of the current character.</p>
     *
     * @return The characters max hit points
     *
     * @since 0.1.0
     */
    int getMaxHitPoints();

    /**
     * <pThe <code>calculateMaxHitPoints()</code> method calculates the max
     * hit points of the current character.</p>
     *
     * @see #setHitPoints(int)
     * @see #getMaxHitPoints()
     * @see #initHitPoints()
     *
     * @since 0.1.0
     */
    void calculateMaxHitPoints();

    /**
     * <p>The <code>initHitPoints()</code> method is a 'quality of life' method,
     * meaning it should do every thing to set-up the characters health
     * correctly. That is done by calling the {@link #calculateMaxHitPoints()}
     * and the {@link #setHitPoints(int)} method.</p>
     *
     * @see #setHitPoints(int)
     * @see #getMaxHitPoints()
     * @see #calculateMaxHitPoints()
     *
     * @since 0.1.0
     */
    void initHitPoints();

    /**
     * <p>The <code>addHitPoints(int)</code> method adds the given amount of
     * hit points to the characters current hit point value.</p>
     * 
     * @see #setHitPoints(int)
     * @see #reduceHitPoints(int)
     * @see #getHitPoints()
     *
     * @param hp the hit points to be added to the total hit point amount
     *
     * @since 0.1.0
     */
    void addHitPoints(final int hp);

    /**
     * <p>The <code>reduceHitPoints(int)</code> method substitutes the given
     * amount from the characters current hit points.</p>
     *
     * @see #setHitPoints(int)
     * @see #addHitPoints(int)
     * @see #getHitPoints()
     *
     * @param hp the hit points to be substitutes from the total hit points
     *
     * @since 0.1.0
     */
    void reduceHitPoints(final int hp);

    /**
     * <p>
     * The <code>isAlive()</code> method returns true if the character is alive,
     * false if not. A {@link DDCharacter} is considered dead if the
     * {@link #getHitPoints()} returns -10 or a value below. If the character is
     * dead he can't be leveled or skilled.
     *
     * @return True if character is alive, false if not.
     *
     * @see #isAlive
     * @see DDCharacter
     *
     * @since 0.1.0
     */
    boolean isAlive();

    /**
     * <p>
     * The <code>isKnockedOut()</code> method returns true if the current hitpoints
     * of the character are below zero, false if he has at least one hitpoint.
     *
     * @return true if characters hitpoints are below zero
     *
     * @see #getHitPoints()
     * @see #isAlive()
     * @see DDCharacter
     *
     * @since 0.1.0
     */
    boolean isKnockedOut();
}
