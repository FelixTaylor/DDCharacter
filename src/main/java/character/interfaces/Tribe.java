/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package character.interfaces;

import character.DDCharacter;
import character.components.Clazz;
import character.components.Race;

/**
 * <p>The <code>Tribe</code> interface defines all relevant methods to control
 * the {@link Race} and {@link Clazz} of the current character.</p>
 *
 * @see Race
 * @see Clazz
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.1
 */
public interface Tribe {

    /**
     * <p>
     * The <code>getRace()</code> method returns the race of the character.
     *
     * <pre>
     * {@literal //} DDCharacter character = new DDCharacter();
     * Race race = character.getRace();
     * </pre>
     *
     * @see Race
     * @see #setRace(Race)
     * @return Race : the race of the character
     *
     * @since 0.1.0
     */
    Race getRace();

    /**
     * <p>
     * The <code>getClazz()<code> method returns the clazz of the character.
     *
     * <pre>
     * {@literal //} DDCharacter character = new DDCharacter();
     * Clazz clazz = character.getClazz();
     * </pre>
     *
     * @see Clazz
     * @see #setClazz(Clazz)
     * @return Clazz : the clazz of the character
     *
     * @since 0.1.0
     */
    Clazz getClazz();

    /**
     * <p>
     * <code>setRace(String)</code> sets the race of the character and adds the race
     * bonus to the abilities.
     *
     * @param race : the name of the new {@link character.components.Race Race}
     *
     * @since 0.1.0
     */
    void setRace(final Race race);

    /**
     * <p>
     * <code>setClazz(String)</code> sets the {@link Clazz Clazz} of the current
     * <code>DDCharacter</code> to the input value. Use the values provided by the
     * Clazz enum.
     *
     * <p>
     * <b>Note:</b> If you change the clazz of the character you should always also
     * call the <code>distributeAbilities()</code> method to set the preferred
     * abilities for the selected clazz. Example:
     *
     * <pre>
     * DDCharacter c = new DDCharacter();
     * c.setClazz(Clazz.ELF);
     * c.distributeAbilities();
     * </pre>
     *
     * <p>
     * Since version 1.5.0 this method is private. You can access it by calling
     * <code>character.set(Object);</code>
     * <p>
     *
     * @see Clazz
     * @param clazz: the clazz of the character
     *
     * @since 0.1.0
     */
    void setClazz(final Clazz clazz);
}
