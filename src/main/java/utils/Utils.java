/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * <p>
 * The Utils class contains various method to simplify the character generation.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.5.0
 */
public class Utils {

	/**
	 * <p>
	 * The <code>dateToString()</code> method takes two inputs, first is the display
	 * pattern for the output string and the second parameter is the time which
	 * shall be displayed.
	 *
	 * @param displayPattern the date display style. For example: "yyyy.mm.dd" or
	 *                       "dd.mm.yyyy, hh:MM:ss".
	 * @param millis         The date and time in millis
	 * 
	 * @return Nicely formatted date and time
	 *
	 * @since 0.1.0
	 */
	public static String dateToString(String displayPattern, long millis) {
		return new SimpleDateFormat(displayPattern, Locale.getDefault()).format(millis);
	}

	/**
	 * <p>
	 * The <code>trimWhitespace(String)</code> method takes the input string and
	 * replaces each multiple white space characters to a single one. The method
	 * also removes leading and trailing white spaces.
	 * 
	 * @param input The string to be trimmed
	 * 
	 * @return String The trimmed string
	 *
	 * @since 0.1.0
	 */
	public static String trimWhitespace(String input) {
		return input.trim().replaceAll("\\s+", " ");
	}
}
