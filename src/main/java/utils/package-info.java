/**
 * <p>
 * The utils package contains various class to handle very specific tasks around
 * the library but it also contains the Dice object which is used from the
 * Abilities class to generate random abilities for the character.
 * <p>
 * You can find the corresponding test files in the <b>test</b>.utils package.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.1.1
 *
 * @see utils.Dice
 * @see utils.Utils
 */
package utils;