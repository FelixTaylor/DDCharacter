/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package utils;

import character.DDCharacter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * The <code>Dice</code> class represents a set of dice. It is used to calculate
 * the ability points of a {@link DDCharacter}. The <code>Dice</code> object also
 * stores all {@link #results} of the rolled dices for later use.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.2.4
 */
public class Dice /* implements Serializable */ {
	private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
//	private static final long serialVersionUID = Utils.nextVersionUID();

	static {
		logger.setLevel(Level.WARNING);
	}

	/**
	 * <p>
	 * The ArrayList <code>results</code> contains the outcome from each rolled
	 * dice.
	 * 
	 * @see Dice
	 * @see #roll()
	 * @see #getResults()
	 *
	 * @since 0.1.0
	 */
	private final ArrayList<Integer> results;

	/**
	 * <p>
	 * The <code>amount</code> represents the amount or count of dice which are
	 * being rolled.
	 * 
	 * @see Dice
	 * @see #setAmount(int)
	 * @see #getAmount()
	 * @see #roll()
	 *
	 * @since 0.1.0
	 */
	private int amount;

	/**
	 * <p>
	 * The <code>range</code> represents the range of each {@link Dice} in the set.
	 * It can hold a minimum value of two (2), which would be a
	 * <code>true/false</code> dice.
	 * 
	 * @see Dice
	 * @see #setRange(int)
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	private int range;

	/**
	 * <p>
	 * The Random <code>dice</code> object represents the actual {@link Dice} which
	 * is being rolled.
	 *
	 * @since 0.1.0
	 */
	private final Random dice;

	/**
	 * <p>
	 * The <code>Dice()</code> constructor sets the default value for the amount of
	 * dices to 1 and the range of that <code>Dice</code> to six (1D6).
	 *
	 * @see Dice
	 * @see #Dice(int, int)
	 * @see #getAmount()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	public Dice() {
		this.amount = 1;
		this.range = 6;
		this.results = new ArrayList<>();
		this.dice = new Random();
	}

	/**
	 * <p>
	 * If you want to roll multiple dices use this constructor to set the correct
	 * values for your needs.
	 * 
	 * @param amount Integer representing the amount of dices.
	 * @param range  Integer representing the range of each dice. A six represents a
	 *               <code>Dice</code> with six eyes.
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #getAmount()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	public Dice(final int amount, final int range) {
		this();
		setAmount(amount);
		setRange(range);
	}

	/**
	 * <p>
	 * the <code>setAmount(int)</code> method sets the amount of dices. The value of
	 * {@link #amount} has to be greater then 0 (zero). If it's not an error will be
	 * prompted and amount is set to two!
	 *
	 * @param amount The amount of dices to use.
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #getAmount()
	 *
	 * @since 0.1.0
	 */
	public void setAmount(final int amount) {
		if (amount > 0) {
			this.amount = amount;
			return;
		}
		logger.warning("Dice.setAmount(int): Invalid Input, the amount value remains at " + this.amount);
	}

	/**
	 * <p>
	 * <code>setRange()</code> is used to set the range for each {@link Dice}. The
	 * minimum range of this {@link Dice} has to be 2 (then it would be a
	 * <code>true</code>/<code>false</code> {@link Dice}).
	 *
	 * @param range The range of each <code>Dice</code>.
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	private void setRange(final int range) {
		if (range > 1) {
			this.range = range;
			return;
		}
		logger.warning("Dice.setRange(int): Invalid Input, the range value remains at " + this.range);
	}

	/**
	 * <p>
	 * The <code>roll()</code> method is used to actually roll the dice.
	 *
	 * @return This Dice object
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #getAmount()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	public Dice roll() {
		this.results.clear();
		for (int i = 0; i < getAmount(); i++) {
			this.results.add(this.dice.nextInt(getRange()) + 1);
		}
		return this;
	}

	/**
	 * <p>
	 * <code>sort()</code> is used to sort the set of dices from lowest to highest,
	 * so the value at index 0 is the smallest number.
	 *
	 * @return This Dice object
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #getAmount()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	public Dice sort() {
		Collections.sort(this.results);
		return this;
	}

	/**
	 * <p>
	 * <code>eraseSmallestDice()</code> erases the {@link Dice} with the lowest
	 * result. For example: Rolled dice results = 2, 5, 6, 1. In this case the '1'
	 * will be erased '2, 5, 6' remain.
	 * 
	 * @return This dice object
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #results
	 * @see #getAmount()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	public Dice eraseSmallestDice() {
		sort();
		this.results.remove(0);
		return this;
	}

	/**
	 * <p>
	 * The <code>sum()</code> method adds all values from the rolled dices together
	 * and returns that value.
	 *
	 * @return The sum of all dice
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #results
	 * @see #getAmount()
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	public int sum() {
		return getResults().stream().mapToInt(Integer::intValue).sum();
	}

	/**
	 * <p>
	 * <code>getAmount()</code> returns the amount of {@link Dice} being rolled.
	 *
	 * @return The amount of dice
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #amount
	 *
	 * @since 0.1.0
	 */
	public int getAmount() {
		return this.amount;
	}

	/**
	 * <p>
	 * <code>getRange()</code> returns the range of the dices.
	 * 
	 * @return The range of each dice
	 * 
	 * @see Dice
	 * @see #Dice()
	 * @see #range
	 *
	 * @since 0.1.0
	 */
	public int getRange() {
		return this.range;
	}

	/**
	 * <p>
	 * <code>getResults()</code> returns an ArrayList which contains all results of
	 * each rolled dice.
	 * 
	 * @return All dice results
	 * 
	 * @see Dice
	 * @see #Dice
	 * @see #results
	 *
	 * @since 0.1.0
	 */
	public ArrayList<Integer> getResults() {
		return this.results;
	}

	/**
	 * <p>
	 * <code>clear()</code> clears the values from the result ArrayList.
	 *
	 * @see Dice
	 * @see #Dice
	 * @see #results
	 *
	 * @since 0.1.0
	 */
	public void clear() {
		this.results.clear();
	}
}