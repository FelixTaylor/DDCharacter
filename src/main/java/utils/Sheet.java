package utils;

import character.DDCharacter;
import appearance.Appearance;
import character.components.Ability;
import character.components.DDLevel;
import character.components.Race;
import character.components.Skill;
import equipment.Equipment;

import java.util.Map;

/**
 * <p>The <code>Sheet</code> class prints a character sheet representation to
 * the console.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.2.2
 */
public class Sheet {
    private static final int TABLE_WIDTH = 80;
    private static final String TO_ITEM_ROW_FORMAT = "%n%s%n%s%n";
    private static final String dateTimePattern = "yyyy-MM-dd, hh:m:ss";
    private static final String BR = "\n";
    private static final String delimiter = "--------------------------------------------------------------------------------";


    public static String getSimpleSheet(DDCharacter character, Equipment equipment) {
        StringBuilder out = new StringBuilder(character.getName());
        out.append(BR).append(delimiter).append(BR);
        out.append(String.format(setTableColumns(3), character.getRace(), character.getClazz(), Integer.toString(character.getLevel())));
        return out.toString();
    }

    public static String print(DDCharacter character, Appearance appearance, Equipment equipment) {
        StringBuilder out = new StringBuilder();
//        String delimiter = StringUtils.repeat("-", TABLE_WIDTH);
        String format = setTableColumns(2) + BR;

        out.append(String.format(format, "DDCharacter", character.getName()));

        out.append(delimiter);
        out.append(getMetaInfo(character)).append(delimiter);
        out.append(getRaceInfo(character)).append(delimiter);
        out.append(getClazzInfo(character));
        out.append(getAbilitiesInfo(character)).append(delimiter);
        out.append(getLevelInfo(character)).append(delimiter);
        out.append(getDeityInfo(character)).append(delimiter);
        out.append(getDispositionInfo(character)).append(delimiter);
        out.append(getSizeAndWeightInfo(appearance));

        out.append(String.format(format, "gender", character.getGender()));
        out.append(String.format(format, "hitpoints", character.getHitPoints() + "/" + character.getMaxHitPoints()));
        out.append(String.format(format, "talentpoints", character.getTalentPoints()));
        out.append(String.format(format, "isAlive", character.isAlive()));
        out.append(String.format(format, "isKnockedOut", character.isKnockedOut()));
        out.append(String.format(format, "canBeModified", character.canBeModified()));

        out.append(getBaseAttackInfo(character));
        out.append(getSavingThrowsInfo(character));
        out.append(getEquipmentInfo(equipment));
        out.append(getSkillsInfo(character));

        return out.toString();
    }

    private static String getHeader(String title) {
        final String fill = new String(new char[(TABLE_WIDTH-5) - title.length()]).replace('\0', '=');
        return String.format("%n=== %s %s%n", title.toUpperCase(), fill);
    }

    private static String setTableColumns(int items) {
        final int width = Math.floorDiv(TABLE_WIDTH, items);
        final StringBuilder out = new StringBuilder("%-" + (width + (TABLE_WIDTH % items)) +  "s");
        for (int i=1; i<items; i++) {
            out.append("%").append(width).append("s");
        }
        return out.toString();
    }

    private static String getEquipmentInfo(Equipment equipment) {
        final String armour = String.format(setTableColumns(2), "armour", equipment.getArmour().getName());
        final String weapon = String.format(setTableColumns(2), "weapon", equipment.getWeapon().getName());
        final String gold = String.format(setTableColumns(2), equipment.getCapital().getCurrency(), equipment.getCapital().getCapital());
        return String.format("%n%s%s%n%s%n%s", getHeader("Equipment"), armour, weapon, gold);
    }

    private static String getMetaInfo(DDCharacter character) {
        final String first = String.format(setTableColumns(2), "uuid", character.getUUID().toString());
        final String second = String.format(setTableColumns(2), "timestamp",
                Utils.dateToString(dateTimePattern, character.getCreationDate()));

        return String.format(TO_ITEM_ROW_FORMAT, first, second);
    }

    private static String getRaceInfo(DDCharacter character) {
        final Race race = character.getRace();
        final String title = String.format(setTableColumns(3), "race", "positive", "negative");
        final String content = String.format(setTableColumns(3), race.getName(), race.getPositiveAbility(), race.getNegativeAbility());
        return String.format(TO_ITEM_ROW_FORMAT, title, content);
    }

    private static String getClazzInfo(DDCharacter character) {
        final StringBuilder temp = new StringBuilder();
        for (String s : character.getClazz().getPriorities()) {
            temp.append(" ").append(s);
        }

        final String title = String.format(setTableColumns(2), "class", "priorities");
        final String content = String.format(setTableColumns(2), character.getClazz().getName(), temp.toString());
        return String.format(TO_ITEM_ROW_FORMAT, title, content);
    }

    private static String getLevelInfo(DDCharacter character) {
        final DDLevel level = character.getLevelObject();
        final String title = String.format(setTableColumns(4), "level", "maxSkillRank", "neededExp.", "expForNextLevel");
        final String exp = String.format(setTableColumns(2), "experience", Integer.toString(character.getExperience()));
        final String canLevelUp = String.format(setTableColumns(2), "canLevelUp", Boolean.toString(character.canLevelUp()));
        final String content = String.format(setTableColumns(4),
                Integer.toString(level.getLevel()),
                Integer.toString(level.getMaxSkillRank()),
                Integer.toString(level.getNeededExperience()),
                Integer.toString(level.getRequiredExpForNextLevel())
        );

        return String.format("%n%s%n%s%n%n%s%n%s%n", title, content, exp, canLevelUp);
    }

    private static String getDeityInfo(DDCharacter character) {
        final String title = String.format(setTableColumns(3), "deity", "alignment", "attitude");
        final String content = String.format(setTableColumns(3),
                character.getDeity().getName(),
                character.getDeity().getAlignment(),
                character.getDeity().getAttitude());

        return String.format(TO_ITEM_ROW_FORMAT, title, content);
    }

    private static String getDispositionInfo(DDCharacter character) {
        final String content = String.format(setTableColumns(3), "disposition", character.getAlignment(), character.getAttitude());
        return String.format("%n%s%n", content);
    }

    private static String getBaseAttackInfo(DDCharacter character) {
        final String title = String.format(setTableColumns(4), "base", "attacks", "melee", "range");
        final String content = String.format(setTableColumns(4), character.getBaseAttackBonus(), character.getAttacks(),
                character.getMeleeMod(), character.getRangeMod());
        return String.format("%s%s%n%s%n", getHeader("Base Attack"), title, content);
    }

    private static String getSavingThrowsInfo(DDCharacter character) {
        final String title = String.format(setTableColumns(3), "reflex", "will","fortitude");
        final String content = String.format(setTableColumns(3),  character.getReflex(), character.getWill(), character.getFortitude());
        return String.format("%s%s%n%s", getHeader("Saving Throws"), title, content);
    }

    private static String getAbilitiesInfo(DDCharacter character) {
        final StringBuilder out = new StringBuilder(getHeader("Abilities"));
        out.append(String.format(setTableColumns(3), "name", "value", "mod"));
        out.append("\n");

        for (Ability a : character.getAbilities().asArray()) {
            out.append(String.format(setTableColumns(3), a.getName(), a.getValue(), a.getMod()));
            out.append("\n");
        }

        out.append("\n");
        return out.toString();
    }

    private static String getSizeAndWeightInfo(Appearance appearance) {
        final String size = String.format(setTableColumns(2), "size", appearance.getSize() + "m");
        final String weight = String.format(setTableColumns(2), "weight", appearance.getWeight() + "kg");
        return String.format("%n%s%n%s%n", size, weight);
    }

    private static String getSkillsInfo(DDCharacter character) {
        String usedSkillpoints = "(used) " + character.getSkillPoints() + "/" + character.getSkillsObject().getUsedSkillPoints();
        String skillpoints = String.format(setTableColumns(2), "skillpoints", usedSkillpoints);
        String title = String.format(setTableColumns(4), "name", "power", "rank", "mod");

        StringBuilder skills = new StringBuilder();
        for (Map.Entry<String, Skill> entry : character.getSkillsObject().getSkills().entrySet()) {
            Skill s = entry.getValue();
            String name = s.isDefaultSkill() ? s.getName() + "*" : s.getName();
            skills.append(String.format(setTableColumns(4), name, s.getPower(), s.getRank(), s.getReference().getMod()));
            skills.append("\n");
        }

        return String.format("%n%s%s%n%n%s%n%s",  getHeader("Skills"), skillpoints, title, skills);
    }
}
