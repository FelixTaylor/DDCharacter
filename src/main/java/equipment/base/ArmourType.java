package equipment.base;

import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>The <code>ArmourType</code> defines the type of the usable armours.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.3
 */
public enum ArmourType {
    NONE("NONE"),
    LIGHT("Light Armour"),
    MEDIUM("Medium Armour"),
    HEAVY("Heavy Armour");

    /**
     * <p>The <code>type</code> defines of which type the armour is.
     *
     * @see ArmourType
     * @see #getType()
     *
     * @since 0.1.0
     */
    private final String type;

    ArmourType(@NonNull final String type) {
        this.type = type;
    }

    /**
     * <p>The <code>getType()</code> method returns a string representation of
     * the armour type.
     *
     * @see ArmourType
     *
     * @return String : the type of this armour
     *
     * @since 0.1.0
     */
    public String getType() {
        return this.type;
    }
}
