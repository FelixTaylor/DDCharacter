/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment.base;

import character.DDCharacter;
import equipment.Armour;
import equipment.Weapon;
import utils.Dice;

/**
 * <p>
 * The <code>Damage</code> enum defines how much damage a {@link Weapon} can do.
 * For example the <code>ONE_D4</code> is the damage representation of ONE dice
 * with FOUR sides. The DDCharacter library provides a {@link Dice} object which
 * can further be used to roll the dice for an attack. To calculate the damage
 * of the weapon you can call something like this:
 *
 * <pre>
 * Damage dmg = Damage.ONE_D4;
 * Dice dice = new Dice(dmg.getAmount(), dmg.getRange());
 * int damage = dice.roll().sum();
 * </pre>
 * <p>
 * <b>Note:</b> This damage calculation ignores the fact that the character has
 * the {@link DDCharacter#getMeleeMod()} and {@link DDCharacter#getRangeMod()}
 * method which values should be applied on the above calculation in order to
 * generate a valid damage output.
 *
 * @see Dice
 * @see Weapon
 * @see Armour
 * @see DDCharacter#getMeleeMod()
 * @see DDCharacter#getRangeMod()
 * 
 * @author FelixTaylor
 * @since 1.0.0
 * @version 1.1.3
 */
public enum Damage {
	ONE_D2(1, 2), ONE_D3(1, 3), ONE_D4(1, 4), ONE_D6(1, 6), ONE_D8(1, 8), ONE_D10(1, 10), ONE_D12(1, 12), TWO_D4(2, 4),
	TWO_D6(2, 6);

	/**
	 * <p>
	 * The <code>amount</code> variable represents the amount of dices used to roll
	 * the weapons damage.
	 *
	 * @see Damage
	 * @see Weapon
	 * @see #getAmount()
	 *
	 * @since 0.1.0
	 */
	private final int amount;

	/**
	 * <p>
	 * The <code>range</code> variable represents the range of each dice used to
	 * roll the weapons damage.
	 *
	 * @see Damage
	 * @see Weapon
	 * @see #getRange()
	 *
	 * @since 0.1.0
	 */
	private final int range;

	/**
	 * <p>
	 * The default constructor initializes a new {@link Damage} object and sets the
	 * {@link #amount} and {@link #range} values to the given parameters. This
	 * constructor is private because there is no need to generate new damage types.
	 *
	 * @param amount The amount of dice to use for the damage calculation
	 * @param range iThe range of the dice to use for the damage calculation
	 * 
	 * @see Damage
	 * @see #amount
	 * @see #range
	 *
	 * @since 0.1.0
	 */
	Damage(final int amount, final int range) {
		this.amount = amount;
		this.range = range;
	}

	/**
	 * <p>
	 * The <code>getAmount()</code> method returns the amount value of this damage
	 * type.
	 * 
	 * @return The amount of dices to use for the damage calculation
	 *
	 * @see #amount
	 * @see Damage
	 *
	 * @since 0.1.0
	 */
	public int getAmount() {
		return this.amount;
	}

	/**
	 * <p>
	 * The <code>getRange()</code> method returns the range value of this damage
	 * type.
	 * 
	 * @return The range of the dice to use for the damage calculation
	 *
	 * @see #range
	 * @see Damage
	 *
	 * @since 0.1.0
	 */
	public int getRange() {
		return this.range;
	}
}
