/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment.base;

import character.components.Size;
import equipment.Armour;
import equipment.Weapon;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>
 * The <code>Category</code> enum holds all valid size categories for the
 * equipment like armours and weapons. The categories are:
 * <ul>
 * <li>small
 * <li>medium
 * <li>large
 * </ul>
 * 
 * @see Armour
 * @see Size
 * @see Weapon
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.5
 */
public enum Category {
	SMALL("Small"), MEDIUM("Medium"), LARGE("Large");

	/**
	 * <p>
	 * The size <code>category</code> of the equipment. Valid values are: small,
	 * medium and large.
	 * 
	 * @see Category
	 * @see #getCategory()
	 * @see #compareTo(Size)
	 *
	 * @since 0.1.0
	 */
	private final String category;

	Category(@NonNull final String category) {
		this.category = category;
	}

	/**
	 * <p>
	 * The <code>getCategory()</code> method returns the size {@link #category} of
	 * the equipment.
	 * 
	 * @see Category
	 * @see #category
	 * @see #compareTo(Size)
	 * 
	 * @return String representation of the category
	 *
	 * @since 0.1.0
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * <p>
	 * The <code>compareTo(Size)</code> method can be used to compare the size
	 * category of the equipment with the size category of the race. This is
	 * important because those values have to match if the character wants to equip
	 * an {@link Armour} or use a {@link Weapon}.
	 * 
	 * @param size The size of the race.
	 * 
	 * @return boolean : True if the categories match, false if not
	 * 
	 * @see Size
	 * @see Size#compareTo(Category)
	 * @see #category
	 * @see #getCategory()
	 *
	 * @since 0.1.0
	 */
	public boolean compareTo(@NonNull final Size size) {
		return getCategory().equals(size.getCategory());
	}
}
