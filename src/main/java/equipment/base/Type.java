/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment.base;

import character.DDCharacter;
import equipment.Weapon;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * <p>
 * The Type enum is used to categorize the {@link Weapon}. A weapon can be
 * either of type melee or of type range. The type of the weapon has effect on
 * the damage calculation, if the weapon is of type melee the characters
 * strength modifier will be used, otherwise (if range) the dexterity modifier.
 *
 * @see Damage
 * @see DDCharacter
 * @see Weapon
 * @see #getType()
 * 
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.3
 */
public enum Type {
	MELEE("Melee"), RANGE("Range");

	/**
	 * <p>
	 * The <code>type</code> of the {@link Weapon}.
	 *
	 * @see Type
	 * @see Weapon
	 * @see #getType()
	 *
	 * @since 0.1.0
	 */
	private final String type;

	/**
	 * <p>
	 * The default constructor initializes a new Type object. The constructor is
	 * private because there is no need to generate new types. All available types
	 * are accessible by this enum.
	 *
	 * @param type String representation of the weapon type
	 * 
	 * @see Damage
	 * @see Weapon
	 * @see Type
	 *
	 * @since 0.1.0
	 */
	Type(@NonNull final String type) {
		this.type = type;
	}

	/**
	 * <p>
	 * The <code>getType()</code> method returns a string representation of the
	 * type.
	 *
	 * @return String representation of the weapon type
	 * 
	 * @see #type
	 * @see Type
	 * @see Weapon
	 *
	 * @since 0.1.0
	 */
	public String getType() {
		return this.type;
	}
}
