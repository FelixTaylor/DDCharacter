/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment;

import character.DDCharacter;
import character.components.Clazz;
import utils.Dice;
import utils.Utils;

import static java.util.Objects.requireNonNull;

/**
 * <p>
 * The <code>Capital</code> class contains a string determining the currency
 * used by the {@link DDCharacter} and a double value which holds the actual
 * amount of the defined currency.
 * 
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.1.2
 */
public class Capital {

	/**
	 * <p>
	 * The currency which is used by the character. The default value is 'Gold'.
	 *
	 * @see #setCurrency(String)
	 * @see #getCurrency()
	 *
	 * @since 0.1.0
	 */
	private String currency;

	/**
	 * <p>
	 * The capital owned by the character. The value is calculated by the
	 * {@link #calculate(Clazz)} method provided by this class. Note that the
	 * {@link Clazz} has great impact on the capital calculation.
	 *
	 * @see Clazz
	 * @see #calculate(Clazz)
	 * @see #setCapital(double)
	 * @see #getCapital()
	 *
	 * @since 0.1.0
	 */
	private double capital;

	/**
	 * <p>
	 * The default constructor initializes a new {@link Capital} object with the
	 * currency set to 'Gold' and the capital set to '0.0'.
	 *
	 * @see Capital
	 * @see #capital
	 * @see #Capital(String, double)
	 * @see #calculate(Clazz)
	 *
	 * @since 0.1.0
	 */
	public Capital() {
		this.currency = "Gold";
		this.capital = 0.0;
	}

	/**
	 * <p>
	 * The <code>Capital(String, double)</code> constructor calls the default
	 * constructor and then sets the currency and capital to the given values.
	 *
	 * @param currency String representation of the currency
	 * @param capital  The amount of the currency the character owns.
	 *
	 * @see Capital
	 * @see #capital
	 * @see #Capital()
	 * @see #calculate(Clazz)
	 *
	 * @since 0.1.0
	 */
	public Capital(final String currency, final double capital) {
		this();
		setCurrency(currency);
		setCapital(capital);
	}

	/**
	 * <p>
	 * The <code>calculate(Clazz)</code> method calculates the capital value while
	 * considering the clazz of the character. To calculate the capital a [times]D4
	 * is rolled, meaning if the character is, for example, a mage a 30D4 is rolled.
	 * See the full list below for all values.
	 * <ul>
	 * <li><b>5:</b> MONK
	 * <li><b>20:</b> DRUID
	 * <li><b>30:</b> MAGE, WARLOCK
	 * <li><b>40:</b> BARBARIAN, BARD
	 * <li><b>50:</b> CLERIC, ROGUE
	 * <li><b>60:</b> FIGHTER, PALADIN, RANGER
	 * </ul>
	 * 
	 * @param clazz The clazz of the character
	 *
	 * @see Clazz
	 * @see Capital
	 * @see #Capital()
	 * @see #Capital(String, double)
	 * @see #capital
	 * @see #getCapital()
	 * @see Dice
	 *
	 * @since 0.1.0
	 */
	public void calculate(final Clazz clazz) {
		requireNonNull(clazz, "Clazz may not be null.");
		int times;
		switch (clazz) {
		default: // including monk
			times = 5;
			break;
		case DRUID:
			times = 20;
			break;
		case MAGE:
		case WARLOCK:
			times = 30;
			break;
		case BARBARIAN:
		case BARD:
			times = 40;
			break;
		case CLERIC:
		case ROGUE:
			times = 50;
			break;
		case FIGHTER:
		case PALADIN:
		case RANGER:
			times = 60;
			break;
		}

		setCapital(new Dice(times, 4).roll().sum());
	}

	/**
	 * <p>
	 * The <code>getCurrency()</code> method returns a string representation of the
	 * currency used by the character.
	 *
	 * @return String representation of the currency used by the character
	 *
	 * @see Capital
	 * @see #capital
	 * @see #currency
	 * @see #setCurrency(String)
	 *
	 * @since 0.1.0
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * <p>
	 * The <code>setCurrency(String)</code> method sets the currency of the
	 * character to the given value.
	 * 
	 * @param currency The currency which is used by the character
	 *
	 * @see Capital
	 * @see #currency
	 * @see #getCurrency()
	 *
	 * @since 0.1.0
	 */
	public void setCurrency(final String currency) {
		if (currency != null && !currency.trim().isEmpty()) {
			this.currency = Utils.trimWhitespace(currency);
		}
	}

	/**
	 * <p>
	 * The <code>getCapital()</code> method returns a double representing the amount
	 * owned by the character.
	 * 
	 * @return The amount owned by the character
	 *
	 * @see Capital
	 * @see #Capital()
	 * @see #Capital(String, double)
	 * @see #capital
	 * @see #setCapital(double)
	 * @see #calculate(Clazz)
	 *
	 * @since 0.1.0
	 */
	public double getCapital() {
		return capital;
	}

	/**
	 * <p>
	 * The <code>setCapital(double)</code> method sets the capital owned by the
	 * character to the given value.
	 *
	 * @param capital The amount owned by the character
	 * 
	 * @see Capital
	 * @see #Capital()
	 * @see #Capital(String, double)
	 * @see #calculate(Clazz)
	 * @see #getCapital()
	 *
	 * @since 0.1.0
	 */
	public void setCapital(final double capital) {
		this.capital = capital;
	}
}
