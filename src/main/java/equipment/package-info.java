/**
 * <p>
 * The equipment package provides various classes to select equipment for the
 * character. It provides a predefined set of options to choose from to generate
 * a character, like Armour or Weapon.
 * <p>
 * <b>Note:</b> You should use the controller object located in the
 * <code>character</code> package to generate characters. Using classes from the
 * sub packages can cause unexpected errors which could be hard to debug.
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.0.2
 *
 * @see equipment.Equipment
 * @see equipment.Armour
 * @see equipment.Capital
 * @see equipment/Weapon
 */
package equipment;