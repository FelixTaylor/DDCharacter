/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment;

import character.DDCharacter;
import character.base.Skills;
import character.components.Ability;
import equipment.base.ArmourType;

import static java.util.Objects.requireNonNull;

/**
 * <p>
 * The <code>Armour</code> enum provides all available armours for a
 * {@link DDCharacter} to use. Each armour has a {@link #name}, a {@link #armour}
 * value (internally just called armour) and a {@link #maxDexModifier} which
 * defines how much the armour immobilizes the character.
 *
 * @see DDCharacter
 * @see Weapon
 * 
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.4.1
 */
public enum Armour {
	NONE(            "NONE",             ArmourType.NONE,   0, 99, 0),
	TUNIC(           "Tunic",            ArmourType.LIGHT,  1, 8,  0),
	PADDED(          "Padded",           ArmourType.LIGHT,  1,  8, 0),
	LEATHER(         "Leather",          ArmourType.LIGHT,  2, 6,  0),
	LEATHER_HARDENED("Hardened Leather", ArmourType.LIGHT,  3, 5, -1),
	HIDE_ARMOUR(      "Hide Armour",       ArmourType.LIGHT,  3, 4, -3),
	CHAIN_MAIL(      "Chain Mail",       ArmourType.MEDIUM, 4, 4, -2),
	SCALE_MAIL(      "Scale Mail",       ArmourType.MEDIUM, 4, 3, -4),
	BREAST_PLATE(    "Breast Plate",     ArmourType.MEDIUM, 5, 3, -4),
	SCALE_ARMOUR(     "Scale Armour",      ArmourType.MEDIUM, 4, 3, -4),
	PLATE_ARMOUR(     "Plate Armour",      ArmourType.HEAVY,  7, 0, -7),
	KNIGHTS_ARMOUR(   "Knight's Armour",   ArmourType.HEAVY,  8, 1, -6),
	SPLINT_MAIL(     "Splint Mail",      ArmourType.HEAVY,  6, 0, -7),
	BANDED_MAIL(     "Banded Mail",      ArmourType.HEAVY,  6, 1, -6),
	HALF_PLATE(      "Half Plate",       ArmourType.HEAVY,  7, 0, -7),
	FULL_PLATE(      "Full Plate",       ArmourType.HEAVY,  8, 1, -6);

	/**
	 * <p>
	 * The <code>name</code> of the armour, like 'Leather'.
	 *
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private final String name;

	/**
	 * <p>
	 * The <code>armour</code> variable holds the actual armour value of this armour.
	 * <p>
	 * <b>Note:</b> You have to call <code>getValue()</code> to get this value an
	 * not <code>getArmour()</code> or something.
	 *
	 * @see #getValue()
	 *
	 * @since 0.1.0
	 */
	private final int armour;

	/**
	 * <p>
	 * The <code>maxDexModifier</code> variable defines how much a
	 * {@link DDCharacter} is immobilized while an {@link Armour} is equipped. For
	 * example the character has a dexterity mod of five, but the armour has a
	 * maxDexMod of four. Then the characters final dexterity modifier will be
	 * reduced to four as long as this armour is equipped. This effects all skills,
	 * talents or role-play actions.
	 *
	 * @see Ability
	 * @see Armour
	 * @see #getMaxDexModifier()
	 * @see DDCharacter#getAbilityMod(Object)
	 *
	 * @since 0.1.0
	 */
	private final int maxDexModifier;

	/**
	 * <p>The <code>armourMinusPoints</code> are applied to the power of skills
	 * which use the dexterity modifier to be calculated.
	 *
	 * @see Skills#shouldApplyArmourPenalty(String)
	 * @see #getArmourCheckPenalty()
	 *
	 * @since 0.1.0
	 */
	private final int armourCheckPenalty;

	/**
	 * <p>The <code>type</code> defines of which type the current armour is.
	 *
	 * @see ArmourType
	 * @see #getType()
	 *
	 * @since 0.1.0
	 */
	private final ArmourType type;

	/**
	 * <p>
	 * The default constructor initializes a new {@link Armour} and sets its
	 * attributes to the given values. The constructor is private because this no
	 * need to generate new armours later on. All valid armours are accessibly by this
	 * enum.
	 *
	 * @param name              The name of the armour
	 * @param armour             The armour value of the armour
	 * @param type              The type of the armour
	 * @param maxDexModifier    The max dexterity modifier usable by the character
	 *                          while an armour is equipped
	 * @param armourCheckPenalty The penalty of equipping this armour
	 *
	 * @since 0.1.0
	 */
	Armour(final String name, final ArmourType type, final int armour, final int maxDexModifier,
		   final int armourCheckPenalty) {
		this.name = name;
		this.armour = armour;
		this.type = requireNonNull(type, "ArmourType may not be null.");
		this.maxDexModifier = maxDexModifier;
		this.armourCheckPenalty = armourCheckPenalty;
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of this armour, like
	 * 'Tunic'.
	 *
	 * @see #name
	 * @return String : the name of the armour
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * <p>
	 * The <code>getValue()</code> method returns the armour value of the current
	 * armour.
	 *
	 * @see Armour
	 * @see #armour
	 * 
	 * @return The armour value of the current armour
	 *
	 * @since 0.1.0
	 */
	public int getValue() {
		return this.armour;
	}

	/**
	 * <p>
	 * The <code>getMaxDexModifier()</code> method returns the max dexterity
	 * modifier usable by the character while an armour is equipped. See the
	 * {@link #maxDexModifier} documentation for more information.
	 *
	 * @see Armour
	 * @see Ability
	 * @see #maxDexModifier
	 *
	 * @return The max dexterity modifier usable by the character while this armour
	 *         is equipped
	 *
	 * @since 0.1.0
	 */
	public int getMaxDexModifier() {
		return this.maxDexModifier;
	}

	/**
	 * <p>The <code>getArmourMinusPoints()</code> method returns the value which
	 * should be applied to skills like climbing, tumbling, swimming.
	 *
	 * @return int : the minus points to apply to some skills
	 *
	 * @see Skills#shouldApplyArmourPenalty(String)
	 *
	 * @since 0.1.0
	 */
	public int getArmourCheckPenalty() {
		return this.armourCheckPenalty;
	}

	/**
	 * <p>The <code>getType()</code> method returns a string representation of
	 * the armour type.
	 *
	 * @see ArmourType
	 *
	 * @return String : the type of this armour
	 *
	 * @since 0.1.0
	 */
	public String getType() {
		return this.type.getType();
	}
}