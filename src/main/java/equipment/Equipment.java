/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment;

import character.DDCharacter;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Arrays;
import java.util.Currency;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Objects.*;

/**
 * <p>The <code>Equipment</code> class represents the equipment carried by the
 * character.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.1.1
 */
public class Equipment {
    private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    static {
        logger.setLevel(Level.INFO);
    }

    /**
     * <p>
     * The <code>armour</code> used by the character. The armour effects the max
     * usable dexterity modifier of the character. For further information on how
     * the {@link Armour} effects the dexterity modifier see
     * {@link Armour#getMaxDexModifier()} method.
     *
     * @see Armour
     * @see #getArmour()
     * @see #setArmour(Armour)
     * @see Armour#getMaxDexModifier()
     *
     * @since 0.1.0
     */
    private Armour armour;

    /**
     * <p>
     * The capital of the current character. The <code>Capital</code> class contains
     * the currency (string) and the capital (double) values.
     *
     * @see Capital
     * @see #setCurrency(String)
     * @see Capital#getCurrency()
     *
     * @since 0.1.0
     */
    private Capital capital;

    /**
     * <p>
     * The <code>weapon</code> used by the character.
     *
     * @see #setWeapon(Weapon)
     * @see #getWeapon()
     * @see Weapon
     *
     * @since 0.1.0
     */
    private Weapon weapon;

    public Equipment() {
        this.armour = Armour.NONE;
        this.weapon = Weapon.NONE;
        this.capital = new Capital("Gold", 0.0);
    }

    /**
     * <p>
     * The <code>setWeapon(Weapon)</code> method sets a new weapon for the current
     * character.
     *
     * @param weapon Weapon : the new weapon for the character
     *
     * @since 0.1.0
     */
    private void setWeapon(@NonNull final Weapon weapon) {
        this.weapon = requireNonNull(weapon, "Weapon may not be null.");
    }

    /**
     * <p>
     * The <code>setArmour(Armour)</code> method is used to set a new armour for the
     * character.
     *
     * @see Armour
     * @see #getArmour()
     * @param armour Armour : the armour of the character
     *
     * @since 0.1.0
     */
    private void setArmour(final Armour armour) {
        this.armour = requireNonNull(armour, "Armour may not be null.");
    }

    /**
     * <p>
     * The <code>setCurrency(String)</code> method sets the currency used by the
     * character.
     *
     * @see Capital#setCurrency(String)
     * @param currency The currency used by the character
     *
     * @since 0.1.0
     */
    public void setCurrency(final String currency) {
        this.capital.setCurrency(currency);
    }

    /**
     * <p>
     * The <code>setCapital(Double)</code> method sets the new amount of capital the
     * character owns.
     *
     * @see Capital
     * @see #getCapital()
     * @param capital the new capital for the character
     *
     * @since 0.1.0
     */
    public void setCapital(final double capital) {
        this.capital.setCapital(capital);
    }

    /**
     * <p>
     * The <code>setCapital(Capital)</code> method sets a new capital object for the
     * current character.
     *
     * @see Capital
     * @see #getCapital()
     *
     * @since 0.1.0
     **/
    private void setCapital(@NonNull final Capital capital) {
        this.capital = requireNonNull(capital, "Capital may not be null.");
    }

    /**
     * <p>The <code>set(Object...)</code> method sets all given objects to the
     * characters equipment, if they are not null.</p>
     *
     * @see Armour
     * @see Equipment
     * @see Capital
     * @see Currency
     * @see Weapon
     * @see DDCharacter
     *
     * @param objects can be any enum entry or object which can be associated
     *                with the equipment of the character
     *
     * @since 0.1.0
     */
    public void set(Object... objects) {
        Arrays.stream(objects).filter(Objects::nonNull).forEach(this::setObj);
    }

    private void setObj(Object obj) {
        if (obj instanceof Weapon) {
            setWeapon((Weapon) obj);
        } else if (obj instanceof Armour) {
            setArmour((Armour) obj);
        } else if (obj instanceof Capital) {
            setCapital((Capital) obj);
        } else {
            logger.warning("The unknown object '"
                    + obj.getClass().getSimpleName()
                    + "' could not be set as equipment.");
        }
    }
    /**
     * <p>
     * The <code>getArmour()</code> method returns the armour used by the handled
     * character.
     *
     * @return the armour of the character
     * @see Armour
     *
     * @since 0.1.0
     */
    public Armour getArmour() {
        return this.armour;
    }

    /**
     * <p>
     * The <code>getCapital()</code> method returns the capital of the character,
     * including the currency and the amount owned by the character.
     *
     * @return Capital: the capital of the current character
     * @see Capital
     *
     * @since 0.1.0
     */
    public Capital getCapital() {
        return this.capital;
    }

    /**
     * <p>
     * The <code>getWeapon()</code> method returns the currently equipped Weapon of
     * the character.
     *
     * @return Weapon: the weapon in use by the character
     *
     * @since 0.1.0
     */
    public Weapon getWeapon() {
        return this.weapon;
    }
}
