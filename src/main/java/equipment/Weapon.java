/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package equipment;

import character.DDCharacter;
import character.components.Size;
import equipment.base.Category;
import equipment.base.Damage;
import equipment.base.Type;
import org.checkerframework.checker.nullness.qual.NonNull;
import utils.Dice;

/**
 * <p>
 * The <code>Weapon</code> enum stores every available weapon for the
 * {@link DDCharacter} to use. Each weapon has a {@link #name} and a
 * {@link #damage} attribute and a {@link #type}.
 * <p>
 * - So choose wisely!
 * 
 * @see Armour
 * @see Damage
 * @see DDCharacter
 * @see Type
 *
 * @since 0.1.0
 * @author Felix Taylor
 * @version 1.4.4
 */
public enum Weapon {
	NONE("Fists", Damage.ONE_D3, Type.MELEE, Category.SMALL),

	// Bows
	SHORT_COMPOSITE_BOW_S("Short Composite Bow", Damage.ONE_D4, Type.RANGE, Category.SMALL),
	SHORT_COMPOSITE_BOW_M("Short Composite Bow", Damage.ONE_D6, Type.RANGE, Category.MEDIUM),
	LONG_COMPOSITE_BOW_S("Long Composite Bow", Damage.ONE_D6, Type.RANGE, Category.SMALL),
	LONG_COMPOSITE_BOW_M("Long Composite Bow", Damage.ONE_D8, Type.RANGE, Category.MEDIUM),
	SHORT_BOW_S("Short Bow", Damage.ONE_D4, Type.RANGE, Category.SMALL),
	SHORT_BOW_M("Short Bow", Damage.ONE_D6, Type.RANGE, Category.MEDIUM),
	LONG_BOW_S("Long Bow", Damage.ONE_D6, Type.RANGE, Category.SMALL),
	LONG_BOW_M("Long Bow", Damage.ONE_D8, Type.RANGE, Category.MEDIUM),

	// Daggers
	DAGGER_S("Dagger", Damage.ONE_D3, Type.MELEE, Category.SMALL),
	DAGGER_M("Dagger", Damage.ONE_D4, Type.MELEE, Category.MEDIUM),

	// Hammers
	LIGHT_HAMMER_S("Light Hammer", Damage.ONE_D3, Type.MELEE, Category.SMALL),
	LIGHT_HAMMER_M("Light Hammer", Damage.ONE_D4, Type.MELEE, Category.MEDIUM),

	// Swords
	SHORT_SWORD_S("Short Sword", Damage.ONE_D4, Type.MELEE, Category.SMALL),
	SHORT_SWORD_M("Short Sword", Damage.ONE_D6, Type.MELEE, Category.MEDIUM),
	LONG_SWORD_S("Long Sword", Damage.ONE_D6, Type.MELEE, Category.SMALL),
	LONG_SWORD_M("Long Sword", Damage.ONE_D8, Type.MELEE, Category.MEDIUM),

	// Spears
	LONG_SPEAR_S("Long Speer", Damage.ONE_D6, Type.MELEE, Category.SMALL),
	LONG_SPEAR_M("Long Speer", Damage.ONE_D8, Type.MELEE, Category.MEDIUM),
	SHORT_SPEAR_S("Short Speer", Damage.ONE_D4, Type.MELEE, Category.SMALL),
	SHORT_SPEAR_M("Short Speer", Damage.ONE_D6, Type.MELEE, Category.MEDIUM),
	SPEAR_S("Speer", Damage.ONE_D6, Type.MELEE, Category.SMALL),
	SPEAR_M("Speer", Damage.ONE_D8, Type.MELEE, Category.MEDIUM),
	JAVELIN_S("Javelin", Damage.ONE_D4, Type.MELEE, Category.SMALL),
	JAVELIN_M("Javelin", Damage.ONE_D6, Type.MELEE, Category.MEDIUM);

	/**
	 * <p>
	 * The <code>name</code> of the weapon.
	 *
	 * @see #getName()
	 *
	 * @since 0.1.0
	 */
	private final String name;

	/**
	 * <p>
	 * The <code>damage</code> of the weapon. It is define in the {@link Damage
	 * Damage} class.
	 *
	 * @see Damage
	 * @see #getDamage()
	 *
	 * @since 0.1.0
	 */
	private final Damage damage;

	/**
	 * <p>
	 * The <code>type</code> of the weapon. It can be <code>MELEE</code> or
	 * <code>RANGE</code>.
	 * 
	 * @see Type
	 * @see #getType()
	 *
	 * @since 0.1.0
	 */
	private final Type type;

	/**
	 * <p>
	 * The <code>category</code> of the weapon. A {@link DDCharacter} can handle a
	 * {@link Weapon} most effective if the weapon has the same size as the
	 * character. <b>Note:</b> At the current time a character can NOT equip a
	 * weapon which is not in his size category.
	 *
	 * @since 0.1.0
	 */
	private final Category category;

	/**
	 * <p>
	 * The default constructor initializes a new {@link Weapon} and sets its
	 * attributes to the given values. The constructor is private because there no
	 * need to generate new weapons. All weapons are accessibly by this enum.
	 *
	 * @param name   The name of the weapon
	 * @param damage The damage of the weapon
	 * 
	 * @see Category
	 * @see Damage
	 * @see Weapon
	 * @see Type
	 *
	 * @since 0.1.0
	 */
	Weapon(@NonNull final String name, @NonNull final Damage damage, @NonNull final Type type,
		   @NonNull final Category category) {
		this.name = name;
		this.damage = damage;
		this.type = type;
		this.category = category;
	}

	/**
	 * <p>
	 * The <code>getName()</code> method returns the name of the weapon, like "Short
	 * Sword".
	 * 
	 * @return String representation of the Weapon
	 *
	 * @see #name
	 *
	 * @since 0.1.0
	 */
	public String getName() {
		return name;
	}

	/**
	 * <p>
	 * The <code>getDamage()</code> method returns the damage of the weapon. In that
	 * process a {@link Dice} will be rolled with the amount and range define in the
	 * Damage object of this {@link Weapon}.
	 * <p>
	 * <b>Note:</b> The method ignores the fact that the
	 * {@link DDCharacter#getMeleeMod()} method which return value should further be
	 * added to the <code>getDamage()</code> value. And also that the returned value
	 * is randomly generated. This method should be used as follow:
	 * 
	 * <pre>
	 * {@literal //} DDCharacter character = new DDCharacter().random();
	 * <b>int damage = character.getMeleeMod() + character.getWeapon().getDamage();</b>
	 * 
	 * {@literal //} or if the weapon used is ranged:
	 * int damage = character<b>.getRangeMod()</b> + character.getWeapon().getDamage();
	 * </pre>
	 * <p>
	 * The damage calculated in this example is the damage that the character does
	 * with one attack.
	 *
	 * @return the calculated damage from this weapon
	 * 
	 * @see #damage
	 * @see Dice
	 * @see DDCharacter
	 * @see Weapon
	 *
	 * @since 0.1.0
	 */
	public int getDamage() {
		return new Dice(this.damage.getAmount(), this.damage.getRange()).roll().sum();
	}

	/**
	 * <p>
	 * The <code>getType()</code> method returns the type of this {@link Weapon}. It
	 * can be either <code>MELEE</code> or <code>RANGED</code>.
	 *
	 * @return The type of the character
	 * 
	 * @see Weapon
	 * @see Type
	 * @see #type
	 *
	 * @since 0.1.0
	 */
	public Type getType() {
		return this.type;
	}

	/**
	 * <p>
	 * The <code>getCategory()</code> method returns the category of the weapon.
	 *
	 * @return The category of the weapon
	 *
	 * @see #category
	 * @see Size
	 * @see Weapon
	 *
	 * @since 0.1.0
	 */
	public Category getCategory() {
		return this.category;
	}
}
