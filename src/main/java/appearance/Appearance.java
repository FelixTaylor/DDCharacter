/*
 * This file is part of DDCharacter.
 *
 * DDCharacter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DDCharacter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with DDCharacter. If not, see <http://www.gnu.org/licenses/>.
 */
package appearance;

import character.DDCharacter;
import character.components.Race;
import character.components.Size;
import character.components.Weight;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.lang.Math.*;

/**
 * <p>The <code>Appearance</code> class contains all attributes of the current
 * character which define how the character looks.</p>
 *
 * @see Size
 * @see Weight
 * @see DDCharacter
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 1.0.3
 */
public class Appearance {

    /**
     * <p>
     * The weight of the character.
     *
     * @see #setWeight(double)
     * @see #getWeight()
     *
     * @since 0.1.0
     */
    private double weight;

    /**
     * <p>
     * The size of the character in meters
     *
     * @see Size
     * @see #getSize()
     * @see #setSize(double)
     *
     * @since 0.1.0
     */
    private double size;

    public Appearance() {
        this.size = 0.0;
        this.weight = 0.0;
    }

    public void setAppearance(double size, double weight) {
        setSize(size);
        setWeight(weight);
    }

    public void setAppearance(@NonNull final Race race) {
        randomSize(race);
        randomWeight(race);
    }

    /**
     * <p>
     * The <code>setSize(double)</code> method sets the new size - in meters - of
     * this character. The new value has to be greater then 0 (zero).
     *
     * <pre>
     * {@literal //} DDCharacter character = new DDCharacter();
     * character.setSize(1.23456); // will evaluate to 1.23
     * character.setSize(2.00142); // will evaluate to 2
     * </pre>
     *
     * @see #size
     * @see #getSize()
     * @see Size
     *
     * @param size the new size of the character
     *
     * @since 0.1.0
     */
    public void setSize(final double size) {
        if (size <= 0) {
            throw new IllegalArgumentException(
                    "The size input value " + size + " is invalid. The value has to be greater then 0 (zero).");
        }

        this.size = floor(size * 100) / 100;
    }

    /**
     * <p>
     * The <code>setWeight(double)</code> method sets the weight for the current
     * character.
     *
     * @param weight double: the weight assigned to the character
     *
     * @see DDCharacter
     *
     * @since 0.1.0
     */
    public void setWeight(final double weight) {
        this.weight = floor(weight * 100) / 100;
    }

    /**
     * <p>
     * The <code>getSize()</code> method returns the exact size of the character in
     * meters. This size value is <b>not</b> representing the {@link Race#getSize()
     * size of the race}!
     *
     * @see #size
     * @see #setSize(double)
     * @see Size
     *
     * @see #setSize(double) 
     * @see #randomSize(Race)
     *
     * @return double : The size of the character
     *
     * @since 0.1.0
     */
    public double getSize() {
        return this.size;
    }

    /**
     * <p>
     * The <code>getWeight()</code> method returns the weight (in kg) of the
     * character.
     *
     * @see #setWeight(double)
     * @see #randomWeight(Race) 
     *
     * @return double : The weight of the character
     *
     * @since 0.1.0
     */
    public double getWeight() {
        return this.weight;
    }

    /**
     * <p>
     * The <code>randomSize()</code> method sets a random generated double value as
     * new size for the character.
     *
     * @see Size
     *
     * @since 0.1.0
     */
    public void randomSize(@NonNull final Race race) {
        setSize(race.getSize().randomSize());
    }

    /**
     * <p>
     * The <code>randomWeight()</code> method sets a random generated double value as
     * new weight for the character.
     *
     * @see Weight
     *
     * @since 0.1.0
     */
    public void randomWeight(@NonNull final Race race) {
        setWeight(race.getWeight().randomWeight());
    }
}
